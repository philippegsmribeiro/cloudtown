package cloudtown.storage.cloud.service.storage;

import static org.junit.Assert.assertFalse;
import static org.junit.Assert.assertNotNull;
import static org.mockito.Mockito.mock;
import static org.mockito.Mockito.when;

import cloudtown.storage.UnitTest;
import cloudtown.storage.cloud.provider.ProviderIntegrationException;
import cloudtown.storage.cloud.provider.aws.AwsGlacier;
import cloudtown.storage.cloud.provider.aws.AwsS3;
import cloudtown.storage.model.account.Credential;
import cloudtown.storage.model.bucket.Bucket;
import cloudtown.storage.model.bucket.BucketDetail;
import cloudtown.storage.model.vault.Vault;
import com.amazonaws.regions.Regions;
import com.amazonaws.services.glacier.model.DescribeVaultOutput;
import com.amazonaws.services.s3.model.AccessControlList;
import com.amazonaws.services.s3.model.CanonicalGrantee;
import com.amazonaws.services.s3.model.Grant;
import com.amazonaws.services.s3.model.Owner;
import com.amazonaws.services.s3.model.Permission;
import java.util.ArrayList;
import java.util.List;
import org.junit.Before;
import org.junit.BeforeClass;
import org.junit.Test;

public class AwsStorageServiceTest extends UnitTest {

  private AwsS3 awsS3;
  private AwsGlacier awsGlacier;
  private AwsStorageService awsStorageService;
  private static String bucketName = UnitTest.getProperty("bucket.name");
  private static String bucketRegion = UnitTest.getProperty("bucket.region");
  private static String ownerId = UnitTest.getProperty("bucket.owner.id");
  private static String ownerDisplayName = UnitTest.getProperty("bucket.owner.name");

  private static List<com.amazonaws.services.s3.model.Bucket> awsBuckets;
  private static List<DescribeVaultOutput> awsVaults;

  /**
   * Pre setup.
   */
  @BeforeClass
  public static void preSetUp() {
    awsBuckets = new ArrayList<>();
    for (int i = 0; i < 5; i++) {
      com.amazonaws.services.s3.model.Bucket bucket =
          new com.amazonaws.services.s3.model.Bucket("bucket" + i);
      bucket.setOwner(new Owner(ownerId, ownerDisplayName));
      bucket.setName(bucketName);
      awsBuckets.add(bucket);
    }
    awsVaults = new ArrayList<>();
    for (int i = 0; i < 5; i++) {
      DescribeVaultOutput vault = new DescribeVaultOutput();
      vault.setVaultName("vault" + i);
      awsVaults.add(vault);
    }
  }

  /**
   * Setup the mocks.
   */
  @Before
  public void setUp() {
    awsS3 = mock(AwsS3.class);
    awsGlacier = mock(AwsGlacier.class);
    awsStorageService = new AwsStorageService(awsS3, awsGlacier);
  }

  @Test
  public void listBucketsTest() throws ProviderIntegrationException {
    Credential credential = new Credential();
    String region = Regions.US_EAST_1.getName();
    when(awsS3.listBuckets(credential, region)).thenReturn(awsBuckets);
    List<Bucket> list = awsStorageService.listBuckets(credential, region, false);
    assertNotNull(list);
    assertFalse(list.isEmpty());
  }

  @Test
  public void listBucketsDetailTest() throws ProviderIntegrationException {
    Credential credential = new Credential();
    String region = Regions.US_EAST_1.getName();
    when(awsS3.listBuckets(credential, region)).thenReturn(awsBuckets);
    Bucket bucket = Bucket.builder().name(bucketName).build();
    when(awsS3.getRegionBucket(credential, region, bucket.getName())).thenReturn(region);
    when(awsS3.getAccessControlBucket(credential, region, bucket.getName()))
        .thenReturn(getAccessControlList());
    List<Bucket> list = awsStorageService.listBuckets(credential, region, true);
    assertNotNull(list);
    assertFalse(list.isEmpty());
  }

  @Test
  public void getAccessControlBucketTest() throws ProviderIntegrationException {
    Credential credential = new Credential();
    String region = Regions.US_EAST_1.getName();
    when(awsS3.getAccessControlBucket(credential, region, bucketName))
        .thenReturn(getAccessControlList());
    AccessControlList accessControl =
        awsStorageService.getAccessControlBucket(credential, region, bucketName);
    assertNotNull(accessControl);
  }

  @Test
  public void getRegionBucketTest() throws ProviderIntegrationException {
    Credential credential = new Credential();
    String region = Regions.US_EAST_1.getName();
    when(awsS3.getRegionBucket(credential, region, bucketName)).thenReturn(bucketRegion);
    String regionBucket = awsStorageService.getRegionBucket(credential, region, bucketName);
    assertNotNull(regionBucket);
  }

  @Test
  public void getBucketDetailTest() throws ProviderIntegrationException {
    Credential credential = new Credential();
    String region = Regions.US_EAST_1.getName();
    when(awsS3.getRegionBucket(credential, region, bucketName)).thenReturn(region);
    when(awsS3.getAccessControlBucket(credential, region, bucketName))
        .thenReturn(getAccessControlList());
    BucketDetail bucketComplete = awsStorageService.getBucketDetail(credential, region, bucketName);
    assertNotNull(bucketComplete.getAccessControl());
    assertNotNull(bucketComplete.getRegion());
  }

  /**
   * Method that returns a AccessControlList.
   * 
   * @return AccessControlList;
   */
  private AccessControlList getAccessControlList() {
    AccessControlList accessControl = new AccessControlList();
    Grant grant = mockGrant(accessControl);
    accessControl.getGrantsAsList().add(grant);
    return accessControl;
  }

  /**
   * Method that returns a Grant.
   * 
   * @param accessControl AccessControlList
   * @return Grant
   */
  private Grant mockGrant(AccessControlList accessControl) {
    Owner owner = new Owner();
    owner.setDisplayName(ownerDisplayName);
    owner.setId(ownerId);
    accessControl.setOwner(owner);
    CanonicalGrantee grantee = new CanonicalGrantee("id");
    Grant grant = new Grant(grantee, Permission.FullControl);
    return grant;
  }

  @Test
  public void listVaultsTest() throws ProviderIntegrationException {
    Credential credential = new Credential();
    String region = Regions.US_EAST_1.getName();

    when(awsGlacier.listVaults(credential, region)).thenReturn(awsVaults);

    List<Vault> list = awsStorageService.listVaults(credential, region);
    assertNotNull(list);
    assertFalse(list.isEmpty());
  }
}
