package cloudtown.storage.cloud;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertNotNull;
import static org.junit.Assert.assertNull;

import cloudtown.storage.UnitTest;
import org.junit.Test;

public class ProviderTest extends UnitTest {

  @Test
  public void test() {
    String shortName = Provider.AMAZON_AWS.getShortName();
    assertNotNull(shortName);
    
    Provider providerByName = Provider.getProviderByName(shortName);
    assertNotNull(providerByName);
    
    assertEquals(Provider.AMAZON_AWS, providerByName);
    assertEquals(shortName, providerByName.getShortName());
    
    Provider providerNotExistent = Provider.getProviderByName("blah");
    assertNull(providerNotExistent);
  }
}
