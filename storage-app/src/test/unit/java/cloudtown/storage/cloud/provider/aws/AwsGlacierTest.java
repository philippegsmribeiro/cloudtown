package cloudtown.storage.cloud.provider.aws;

import static org.assertj.core.api.Assertions.fail;
import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertFalse;
import static org.junit.Assert.assertNotNull;
import static org.junit.Assert.assertNull;
import static org.junit.Assert.assertTrue;

import cloudtown.storage.UnitTest;
import cloudtown.storage.cloud.provider.ProviderIntegrationException;
import cloudtown.storage.model.account.Credential;
import com.amazonaws.regions.Regions;
import com.amazonaws.services.glacier.AmazonGlacier;
import com.amazonaws.services.glacier.model.DescribeVaultOutput;
import java.util.List;
import org.junit.Before;
import org.junit.BeforeClass;
import org.junit.Test;

public class AwsGlacierTest extends UnitTest {

  private AwsGlacier awsGlacier;

  private static Credential credential;

  /**
   * Pre setup.
   */
  @BeforeClass
  public static void preSetup() {
    credential = Credential.builder()
                           .awsCredential(TestUtils.getValidAwsCredential())
                           .build();
  }

  @Before
  public void setup() {
    awsGlacier = new AwsGlacier();
  }

  @Test
  public void testCache() throws ProviderIntegrationException {
    String region = Regions.US_EAST_1.getName();
    AmazonGlacier client = awsGlacier.getClient(credential, region);
    assertNotNull(client);
    AmazonGlacier client2 = awsGlacier.getClient(credential, region);
    assertNotNull(client2);
    assertEquals(client, client2);
  }

  @Test
  public void testCacheNull() throws ProviderIntegrationException {
    AmazonGlacier client = awsGlacier.getClient(credential, null);
    assertNotNull(client);
    AmazonGlacier client2 = awsGlacier.getClient(credential, null);
    assertNotNull(client2);
    assertEquals(client, client2);
  }
  
  @Test
  public void testClientCredentialNull() throws ProviderIntegrationException {
    try {
      AmazonGlacier client = awsGlacier.getClient(null, "blah");
      assertNull(client);
      fail("Should've thrown an exception!");
    } catch (ProviderIntegrationException e) {
      System.out.println(e.getMessage());
    }
  }
  
  @Test
  public void testClientWrongData() throws ProviderIntegrationException {
    try {
      AmazonGlacier client = awsGlacier.getClient(Credential.builder().build(), "blah");
      assertNull(client);
      fail("Should've thrown an exception!");
    } catch (ProviderIntegrationException e) {
      System.out.println(e.getMessage());
    }
  }

  @Test
  public void testVaults() throws ProviderIntegrationException {
    List<DescribeVaultOutput> list = awsGlacier.listVaults(credential, null);
    assertTrue(list == null || list.isEmpty());
    
    String region = Regions.US_WEST_1.getName();
    List<DescribeVaultOutput> vaults = awsGlacier.listVaults(credential, region);
    assertNotNull(vaults);
    assertFalse(vaults.isEmpty());
  }
}
