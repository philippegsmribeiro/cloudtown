package cloudtown.storage.cloud.provider.aws;

import static org.assertj.core.api.Assertions.fail;
import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertFalse;
import static org.junit.Assert.assertNotNull;
import static org.junit.Assert.assertNull;
import static org.junit.Assert.assertTrue;

import cloudtown.storage.UnitTest;
import cloudtown.storage.cloud.provider.ProviderIntegrationException;
import cloudtown.storage.model.account.Credential;
import com.amazonaws.regions.Regions;
import com.amazonaws.services.ec2.AmazonEC2;
import com.amazonaws.services.ec2.model.AvailabilityZone;
import com.amazonaws.services.ec2.model.Region;
import java.util.List;
import org.junit.Before;
import org.junit.BeforeClass;
import org.junit.Test;

public class AwsEc2Test extends UnitTest {

  private AwsEc2 awsEc2;

  private static Credential credential;

  /**
   * Pre setup.
   */
  @BeforeClass
  public static void preSetup() {
    credential = Credential.builder()
                           .awsCredential(TestUtils.getValidAwsCredential())
                           .build();
  }

  @Before
  public void setup() {
    awsEc2 = new AwsEc2();
  }

  @Test
  public void testCache() throws ProviderIntegrationException {
    String region = Regions.US_EAST_1.getName();
    AmazonEC2 client = awsEc2.getClient(credential, region);
    assertNotNull(client);
    AmazonEC2 client2 = awsEc2.getClient(credential, region);
    assertNotNull(client2);
    assertEquals(client, client2);
  }

  @Test
  public void testCacheNull() throws ProviderIntegrationException {
    AmazonEC2 client = awsEc2.getClient(credential, null);
    assertNotNull(client);
    AmazonEC2 client2 = awsEc2.getClient(credential, null);
    assertNotNull(client2);
    assertEquals(client, client2);
  }
  
  @Test
  public void testClientCredentialNull() throws ProviderIntegrationException {
    try {
      AmazonEC2 client = awsEc2.getClient(null, "blah");
      assertNull(client);
      fail("Should've thrown an exception!");
    } catch (ProviderIntegrationException e) {
      System.out.println(e.getMessage());
    }
  }
  
  @Test
  public void testClientWrongData() throws ProviderIntegrationException {
    try {
      AmazonEC2 client = awsEc2.getClient(Credential.builder().build(), "blah");
      assertNull(client);
      fail("Should've thrown an exception!");
    } catch (ProviderIntegrationException e) {
      System.out.println(e.getMessage());
    }
  }

  @Test
  public void testRegions() throws ProviderIntegrationException {
    List<Region> list = awsEc2.listRegions(credential);
    assertNotNull(list);
    assertFalse(list.isEmpty());
  }

  @Test
  public void testZones() throws ProviderIntegrationException {
    String region = Regions.US_EAST_1.getName();
    List<AvailabilityZone> list = awsEc2.listAvailabilityZones(credential, region);
    assertNotNull(list);
    assertFalse(list.isEmpty());
    list.forEach(t -> assertTrue(t.getRegionName().equals(region)));
  }

}
