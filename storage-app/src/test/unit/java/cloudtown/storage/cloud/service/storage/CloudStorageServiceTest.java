package cloudtown.storage.cloud.service.storage;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertFalse;
import static org.junit.Assert.assertNotNull;
import static org.junit.Assert.assertTrue;
import static org.mockito.Mockito.mock;
import static org.mockito.Mockito.when;

import cloudtown.storage.UnitTest;
import cloudtown.storage.cloud.Provider;
import cloudtown.storage.cloud.provider.ProviderIntegrationException;
import cloudtown.storage.model.account.AccountException;
import cloudtown.storage.model.account.AccountService;
import cloudtown.storage.model.bucket.Bucket;
import cloudtown.storage.model.bucket.BucketDetail;
import cloudtown.storage.model.bucket.BucketDetailRequest;
import cloudtown.storage.model.bucket.BucketListRequest;
import com.amazonaws.regions.Regions;
import com.amazonaws.services.s3.model.AccessControlList;
import java.util.ArrayList;
import java.util.List;
import org.junit.Before;
import org.junit.BeforeClass;
import org.junit.Test;
import org.mockito.Mockito;

public class CloudStorageServiceTest extends UnitTest {

  private AccountService accountService;
  private AwsStorageService awsStorageService;
  private CloudStorageService cloudStorageService;

  private static List<Bucket> buckets;

  /**
   * Pre setup.
   */
  @BeforeClass
  public static void preSetUp() {
    buckets = new ArrayList<>();
    for (int i = 0; i < 5; i++) {
      Bucket bucket = Bucket.builder().name("bucket" + i).build();
      buckets.add(bucket);
    }
  }

  /**
   * Setup mocks.
   */
  @Before
  public void setup() {
    accountService = mock(AccountService.class);
    awsStorageService = mock(AwsStorageService.class);
    cloudStorageService = new CloudStorageService(accountService, awsStorageService);
  }

  @Test
  public void listBucketsTest() throws ProviderIntegrationException, AccountException {
    BucketListRequest bucketListRequest = BucketListRequest.builder().provider(Provider.AMAZON_AWS)
        .region(Regions.US_EAST_1.getName()).build();
    when(awsStorageService.listBuckets(Mockito.any(), Mockito.any(), Mockito.anyBoolean()))
        .thenReturn(buckets);
    List<Bucket> list = cloudStorageService.listBuckets(bucketListRequest);
    assertNotNull(list);
    assertFalse(list.isEmpty());
    assertEquals(buckets, list);
  }

  @Test
  public void getAccessControlBucketTest() throws ProviderIntegrationException, AccountException {
    BucketDetailRequest buckeDetailRequest = BucketDetailRequest.builder().build();
    when(awsStorageService.getAccessControlBucket(Mockito.any(), Mockito.any(), Mockito.any()))
        .thenReturn(this.getBucketAccessControl());
    AccessControlList permission = cloudStorageService.getAccessControlBucket(buckeDetailRequest);
    assertNotNull(permission);
    when(awsStorageService.getRegionBucket(Mockito.any(), Mockito.any(), Mockito.any()))
        .thenReturn("us-west");
    String regionBuckets = cloudStorageService.getRegionBucket(buckeDetailRequest);
    assertNotNull(regionBuckets);
  }

  @Test
  public void getBucketDetailTest() throws ProviderIntegrationException, AccountException {
    BucketDetailRequest buckeDetailRequest = BucketDetailRequest.builder().build();
    when(awsStorageService.getBucketDetail(Mockito.any(), Mockito.any(), Mockito.any()))
        .thenReturn(BucketDetail.builder().build());
    BucketDetail bucketDetail = cloudStorageService.getBucketDetail(buckeDetailRequest);
    assertNotNull(bucketDetail);
  }

  private AccessControlList getBucketAccessControl() {
    AccessControlList permission = new AccessControlList();
    return permission;
  }

  @Test
  public void listBucketsNoProviderTest() throws ProviderIntegrationException, AccountException {
    BucketListRequest bucketListRequest =
        BucketListRequest.builder().provider(null).region(Regions.US_EAST_1.getName()).build();
    List<Bucket> list = cloudStorageService.listBuckets(bucketListRequest);
    assertNotNull(list);
    assertTrue(list.isEmpty());
  }
}
