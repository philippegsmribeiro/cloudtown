package cloudtown.storage.cloud.provider.aws;

import static org.assertj.core.api.Assertions.fail;
import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertFalse;
import static org.junit.Assert.assertNotNull;
import static org.junit.Assert.assertNull;

import cloudtown.storage.UnitTest;
import cloudtown.storage.cloud.provider.ProviderIntegrationException;
import cloudtown.storage.model.account.Credential;
import com.amazonaws.regions.Regions;
import com.amazonaws.services.s3.AmazonS3;
import com.amazonaws.services.s3.model.AccessControlList;
import com.amazonaws.services.s3.model.Bucket;
import java.util.List;
import org.junit.Before;
import org.junit.BeforeClass;
import org.junit.Test;

public class AwsS3Test extends UnitTest {

  private AwsS3 awsS3;

  private static Credential credential;

  private static String bucketName = "cloudos-builds-repository";

  /**
   * Pre setup.
   */
  @BeforeClass
  public static void preSetup() {
    credential = Credential.builder().awsCredential(TestUtils.getValidAwsCredential()).build();
  }

  @Before
  public void setup() {
    awsS3 = new AwsS3();
  }

  @Test
  public void testCache() throws ProviderIntegrationException {
    String region = Regions.US_EAST_1.getName();
    AmazonS3 client = awsS3.getClient(credential, region);
    assertNotNull(client);
    AmazonS3 client2 = awsS3.getClient(credential, region);
    assertNotNull(client2);
    assertEquals(client, client2);
  }

  @Test
  public void testCacheNull() throws ProviderIntegrationException {
    AmazonS3 client = awsS3.getClient(credential, null);
    assertNotNull(client);
    AmazonS3 client2 = awsS3.getClient(credential, null);
    assertNotNull(client2);
    assertEquals(client, client2);
  }

  @Test
  public void testClientCredentialNull() throws ProviderIntegrationException {
    try {
      AmazonS3 client = awsS3.getClient(null, "blah");
      assertNull(client);
      fail("Should've thrown an exception!");
    } catch (ProviderIntegrationException e) {
      System.out.println(e.getMessage());
    }
  }

  @Test
  public void testClientWrongData() throws ProviderIntegrationException {
    try {
      AmazonS3 client = awsS3.getClient(Credential.builder().build(), "blah");
      assertNull(client);
      fail("Should've thrown an exception!");
    } catch (ProviderIntegrationException e) {
      System.out.println(e.getMessage());
    }
  }

  @Test
  public void testBuckets() throws ProviderIntegrationException {
    List<Bucket> list = awsS3.listBuckets(credential, null);
    assertNotNull(list);
    assertFalse(list.isEmpty());

    String region = Regions.US_EAST_1.getName();
    List<Bucket> buckets = awsS3.listBuckets(credential, region);
    assertNotNull(buckets);
    assertFalse(buckets.isEmpty());
  }

  @Test
  public void getAccessControlBucketTest() throws ProviderIntegrationException {

    String region = Regions.US_EAST_1.getName();
    AccessControlList permission = awsS3.getAccessControlBucket(credential, region, bucketName);
    assertNotNull(permission);

    String buckets = awsS3.getRegionBucket(credential, region, bucketName);
    assertNotNull(buckets);
  }
}
