package cloudtown.storage.cloud.service.compute;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertFalse;
import static org.junit.Assert.assertNotNull;
import static org.mockito.Mockito.mock;
import static org.mockito.Mockito.when;

import cloudtown.storage.UnitTest;
import cloudtown.storage.cloud.provider.ProviderIntegrationException;
import cloudtown.storage.cloud.provider.aws.AwsEc2;
import cloudtown.storage.cloud.service.compute.AwsComputeService;
import cloudtown.storage.model.account.Credential;
import cloudtown.storage.model.region.Region;
import com.amazonaws.regions.Regions;
import com.amazonaws.services.ec2.model.AvailabilityZone;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import org.junit.Before;
import org.junit.BeforeClass;
import org.junit.Test;

public class AwsComputeServiceTest extends UnitTest {
  
  private AwsEc2 ec2;
  private AwsComputeService awsComputeService;

  private static List<com.amazonaws.services.ec2.model.Region> awsRegions;
  private static Map<String, List<AvailabilityZone>> awsZones;
  
  /**
   * Pre setup.
   */
  @BeforeClass
  public static void preSetUp() {
    awsRegions = new ArrayList<>();
    awsZones = new HashMap<>();
    
    Regions[] values = Regions.values();
    for (Regions reg : values) {

      // mock aws region
      com.amazonaws.services.ec2.model.Region awsRegion =
          new com.amazonaws.services.ec2.model.Region();
      awsRegion.setRegionName(reg.getName());
      awsRegions.add(awsRegion);
      
      // mock aws zone
      for (int i = 0; i < 3; i++) {
        AvailabilityZone zone = new AvailabilityZone();
        zone.withRegionName(reg.getName());
        zone.withZoneName(reg.getName() + "-" + i);
        zone.withState("state");
        if (!awsZones.containsKey(reg.getName())) {
          awsZones.put(reg.getName(), new ArrayList<>());
        }
        awsZones.get(reg.getName()).add(zone);
      }
    }
  }
  
  /**
   * Setup the mocks.
   */
  @Before
  public void setUp() {
    ec2 = mock(AwsEc2.class);
    awsComputeService = new AwsComputeService(ec2);
  }
  
  @Test
  public void listRegionsTest() throws ProviderIntegrationException {
    Credential credential = new Credential();
    when(ec2.listRegions(credential)).thenReturn(awsRegions);
    for (com.amazonaws.services.ec2.model.Region region : awsRegions) {
      when(ec2.listAvailabilityZones(credential, region.getRegionName()))
          .thenReturn(awsZones.get(region.getRegionName()));
    }
    List<Region> list = awsComputeService.listRegions(credential);
    assertNotNull(list);
    assertFalse(list.isEmpty());
    //
    assertEquals(awsRegions.size(), list.size());
    // assert zones
    list.forEach(t -> assertEquals(3, t.getZones().size()));
  }
}
