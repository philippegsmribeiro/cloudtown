package cloudtown.storage.cloud.service.compute;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertNotNull;
import static org.junit.Assert.assertTrue;
import static org.mockito.Mockito.mock;
import static org.mockito.Mockito.when;

import cloudtown.storage.UnitTest;
import cloudtown.storage.cloud.Provider;
import cloudtown.storage.model.account.AccountService;
import cloudtown.storage.model.region.Region;
import com.amazonaws.regions.Regions;
import java.util.ArrayList;
import java.util.List;
import java.util.stream.Collectors;
import java.util.stream.Stream;
import org.junit.Before;
import org.junit.BeforeClass;
import org.junit.Test;
import org.mockito.Mockito;

public class CloudComputeServiceTest extends UnitTest {

  private AccountService accountService;
  private AwsComputeService awsComputeService;
  private CloudComputeService cloudComputeService;

  private static Provider aws = Provider.AMAZON_AWS;
  private static String accountId = "123456789";
  private static List<Region> regions;
  
  /**
   * Pre setup.
   */
  @BeforeClass
  public static void preSetUp() {
    regions = new ArrayList<>();
    Regions[] values = Regions.values();
    for (Regions reg : values) {
      Region region = Region.builder()
                            .id(reg.getName())
                            .provider(aws)
                            .name(reg.getName())
                            .build();
      regions.add(region);
    }
  }
  
  /**
   * Setup the mocks.
   */
  @Before
  public void setUp() {
    accountService = mock(AccountService.class);
    awsComputeService = mock(AwsComputeService.class);
    cloudComputeService = new CloudComputeService(accountService, awsComputeService);
  }
  
  @Test
  public void listRegionsTest() throws Exception {
    when(awsComputeService.listRegions(Mockito.any())).thenReturn(regions);    
    List<Region> list = cloudComputeService.listRegions(accountId, Provider.AMAZON_AWS);
    assertNotNull(list);
    assertEquals(regions, list);
  }
  
  @Test
  public void listRegionsNotImplementedProviderTest() throws Exception {
    List<Provider> collect = Stream.of(Provider.values()).filter(t -> t != Provider.AMAZON_AWS)
        .collect(Collectors.toList());
    for (Provider provider : collect) {
      List<Region> list = cloudComputeService.listRegions(accountId, provider);
      assertNotNull(list);
      assertTrue(list.isEmpty());
    }
  }
  
  @Test
  public void listRegionsNullProviderTest() throws Exception {
    List<Region> list = cloudComputeService.listRegions(accountId, null);
    assertNotNull(list);
    assertTrue(list.isEmpty());
  }
}
