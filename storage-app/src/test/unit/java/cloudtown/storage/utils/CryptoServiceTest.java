package cloudtown.storage.utils;

import static org.junit.Assert.assertNotNull;

import cloudtown.storage.UnitTest;
import cloudtown.storage.api.commons.CryptoException;
import org.junit.Before;
import org.junit.Rule;
import org.junit.Test;
import org.junit.rules.ExpectedException;

public class CryptoServiceTest extends UnitTest {

  private CryptoService cryptoService;
  @Rule
  public ExpectedException thrown = ExpectedException.none();

  /**
   * Setup mocks.
   * 
   * @throws CryptoException  if Encryption or decryption goes wrong.
   */
  @Before
  public void setup() throws CryptoException {
    cryptoService = new CryptoService();
    this.configCrypto();

  }

  private void configCrypto() throws CryptoException {
    cryptoService.setChartsetname(UnitTest.getProperty("cloudtown.chartsetname.crypto"));
    cryptoService.setKeywordCrypto(UnitTest.getProperty("cloudtown.keyword.crypto"));
    cryptoService.setAlgorithmHash(UnitTest.getProperty("cloudtown.algorithm.hash"));
    cryptoService.setAlgorithmCrypto(UnitTest.getProperty("cloudtown.algorithm.crypto"));
    cryptoService.createKey();
  }

  @Test
  public void verifyFieldsCryptoTest() throws Exception {
    assertNotNull(cryptoService.getAlgorithmCrypto());
    assertNotNull(cryptoService.getAlgorithmHash());
    assertNotNull(cryptoService.getKeywordCrypto());
    assertNotNull(cryptoService.getChartsetname());
  }

  @Test
  public void createKeyExceptionTest() throws Exception {
    thrown.expect(Exception.class);
    cryptoService.setAlgorithmCrypto(null);
    cryptoService.createKey();
  }


  @Test
  public void encryptTest() throws Exception {
    String result = cryptoService.encrypt(new String(UnitTest.getProperty("user.password")));
    assertNotNull(result);
  }

  @Test
  public void encryptExceptionTest() throws Exception {
    thrown.expect(Exception.class);
    cryptoService.setAlgorithmCrypto(null);
    String result = cryptoService.encrypt(new String(UnitTest.getProperty("user.password")));
    assertNotNull(result);
  }

  @Test
  public void decryptTest() throws Exception {
    String result =
        cryptoService.decrypt(new String(UnitTest.getProperty("user.password.encrypt")));
    assertNotNull(result);
  }

  @Test
  public void decryptExceptionTest() throws Exception {
    thrown.expect(Exception.class);
    cryptoService.setAlgorithmCrypto(null);
    String result =
        cryptoService.decrypt(new String(UnitTest.getProperty("user.password.encrypt")));
    assertNotNull(result);
  }

}
