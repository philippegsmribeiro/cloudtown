package cloudtown.storage.utils;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertFalse;
import static org.junit.Assert.assertNotNull;
import static org.junit.Assert.assertNull;
import static org.junit.Assert.assertTrue;
import static org.mockito.Mockito.mock;
import static org.mockito.Mockito.when;

import cloudtown.storage.UnitTest;
import org.junit.Before;
import org.junit.Test;
import org.springframework.core.env.Environment;

public class ProfileUtilsTest extends UnitTest {

  private ProfileUtils profileUtils;
  private Environment environment;

  @Before
  public void setUp() throws Exception {
    environment = mock(Environment.class);
    profileUtils = new ProfileUtils(environment);
  }

  @Test
  public void isTestProfileTest() throws Exception {
    // not test
    assertFalse(profileUtils.isTest());
    // test
    when(environment.getActiveProfiles()).thenReturn(new String[] {ProfileUtils.TEST});
    assertTrue(profileUtils.isTest());
  }
  
  @Test
  public void isProductionProfileTest() throws Exception {
    // not test
    assertFalse(profileUtils.isProduction());
    // test
    when(environment.getActiveProfiles()).thenReturn(new String[] {ProfileUtils.PRODUCTION});
    assertTrue(profileUtils.isProduction());
  }
  
  @Test
  public void isDockerProfileTest() throws Exception {
    // not test
    assertFalse(profileUtils.isDocker());
    // test
    when(environment.getActiveProfiles()).thenReturn(new String[] {ProfileUtils.DOCKER});
    assertTrue(profileUtils.isDocker());
  }
  
  @Test
  public void getMainProfileTest() throws Exception {
    // not test
    assertNull(profileUtils.getMainProfile());
    // test
    when(environment.getActiveProfiles()).thenReturn(new String[] {ProfileUtils.DEVELOPMENT});
    assertNotNull(profileUtils.getMainProfile());
    assertEquals(ProfileUtils.DEVELOPMENT, profileUtils.getMainProfile());
  }
}
