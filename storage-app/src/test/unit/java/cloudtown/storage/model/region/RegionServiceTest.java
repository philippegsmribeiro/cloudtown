package cloudtown.storage.model.region;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertNotNull;
import static org.junit.Assert.assertTrue;
import static org.mockito.Mockito.mock;
import static org.mockito.Mockito.when;

import cloudtown.storage.UnitTest;
import cloudtown.storage.cloud.Provider;
import cloudtown.storage.cloud.provider.ProviderIntegrationException;
import cloudtown.storage.cloud.service.compute.CloudComputeService;
import cloudtown.storage.model.account.AccountException;
import com.amazonaws.regions.Regions;
import java.util.ArrayList;
import java.util.List;
import org.junit.Before;
import org.junit.BeforeClass;
import org.junit.Test;
import org.mockito.Mockito;

public class RegionServiceTest extends UnitTest {

  private RegionService regionService;
  private RegionRepository regionRepository;
  private CloudComputeService cloudComputeService;

  private static Provider aws = Provider.AMAZON_AWS;
  private static String accountId = "123456789";
  private static List<Region> regions;
  
  /**
   * Pre setup.
   */
  @BeforeClass
  public static void preSetUp() {
    regions = new ArrayList<>();
    Regions[] values = Regions.values();
    for (Regions reg : values) {
      Region region = Region.builder()
                            .id(reg.getName())
                            .provider(aws)
                            .name(reg.getName())
                            .build();
      regions.add(region);
    }
  }
  
  /**
   * Setup the mocks.
   */
  @Before
  public void setUp() {
    regionRepository = mock(RegionRepository.class);
    cloudComputeService = mock(CloudComputeService.class);
    regionService = new RegionService(regionRepository, cloudComputeService);
  }
  
  @Test
  public void listTest() throws Exception {
    List<Region> list = regionService.list(accountId, aws);
    assertNotNull(list);
    assertTrue(list.isEmpty());
  }
  
  @Test
  public void listLocalTest() throws Exception {
    when(regionRepository.findByProvider(aws)).thenReturn(regions);    
    List<Region> list = regionService.list(accountId, aws);
    assertNotNull(list);
    assertEquals(regions, list);
  }
  
  @Test
  public void listProviderTest() throws Exception {
    when(cloudComputeService.listRegions(accountId, aws)).thenReturn(regions);    
    when(regionRepository.saveAll(regions)).thenReturn(regions);    
    List<Region> list = regionService.list(accountId, aws);
    assertNotNull(list);
    assertEquals(regions, list);
  }
  
  @Test
  public void listProviderTestNullRequest() throws ProviderIntegrationException, AccountException {
    when(cloudComputeService.listRegions(Mockito.any(), Mockito.any()))
        .thenThrow(new NullPointerException());
    List<Region> list = regionService.list(null, aws);
    assertNotNull(list);
    assertTrue(list.isEmpty());
  }

}
