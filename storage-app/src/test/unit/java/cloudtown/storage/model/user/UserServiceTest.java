package cloudtown.storage.model.user;

import static org.junit.Assert.assertNotNull;
import static org.junit.Assert.assertNull;
import static org.junit.Assert.assertTrue;
import static org.mockito.Mockito.doNothing;
import static org.mockito.Mockito.mock;
import static org.mockito.Mockito.when;

import cloudtown.storage.UnitTest;
import cloudtown.storage.api.commons.CryptoException;
import cloudtown.storage.utils.CryptoService;
import com.querydsl.core.BooleanBuilder;
import com.querydsl.core.types.ExpressionUtils;
import com.querydsl.core.types.Path;
import com.querydsl.core.types.PathMetadata;
import com.querydsl.core.types.PathType;
import com.querydsl.core.types.dsl.BeanPath;
import com.querydsl.core.types.dsl.DslExpression;
import com.querydsl.core.types.dsl.PathInits;
import java.util.Arrays;
import java.util.List;
import java.util.Optional;
import org.apache.commons.lang3.RandomStringUtils;
import org.apache.commons.lang3.StringUtils;
import org.junit.Before;
import org.junit.Rule;
import org.junit.Test;
import org.junit.rules.ExpectedException;
import org.mockito.Mockito;
import org.springframework.security.core.userdetails.UsernameNotFoundException;


public class UserServiceTest extends UnitTest {

  private UserService userService;
  private CryptoService cryptoService;
  private UserRepository userRepository;
  @Rule
  public ExpectedException thrown = ExpectedException.none();

  /**
   * Initialization of the test class.
   * 
   * @throws Exception If any error is generated
   */
  @Before
  public void setUp() throws Exception {
    userRepository = mock(UserRepository.class);
    cryptoService = mock(CryptoService.class);
    userService = new UserService(userRepository, cryptoService);
  }

  @Test
  public void findByLoginTest() throws Exception {
    String username = RandomStringUtils.randomAlphabetic(6);
    User fakeUser = new User();
    when(userRepository.findByLogin(Mockito.any())).thenReturn(fakeUser);
    User userFound = userService.findByLoginName(username);
    assertNotNull(userFound);
    thrown.expect(UsernameNotFoundException.class);
    when(userRepository.findByLogin(Mockito.any())).thenReturn(null);
    userService.findByLoginName(RandomStringUtils.randomAlphabetic(6));
    User userNull = userService.findByLoginName(null);
    assertNull(userNull);
  }

  @Test
  public void findByIdTest() throws Exception {
    User user = createUser();
    when(userRepository.findById(UnitTest.getProperty("user.id"))).thenReturn(Optional.of(user));
    User userFound = userService.findById(UnitTest.getProperty("user.id"));
    assertNotNull(userFound);
  }

  @Test
  public void findByIdExceptionTest() throws Exception {
    when(userRepository.findById(UnitTest.getProperty("user.id"))).thenReturn(null);
    thrown.expect(UsernameNotFoundException.class);
    userService.findById(UnitTest.getProperty("user.id"));
  }

  @Test
  public void saveUserTest() throws Exception {
    User user = createUser();
    when(userRepository.save(user)).thenReturn(user);
    User userFound = userService.save(user);
    assertNotNull(userFound);
  }

  @Test
  public void saveUserExceptionNullTest() throws Exception {
    User user = null;
    thrown.expect(UserException.class);
    when(userRepository.save(user)).thenReturn(user);
    userService.save(user);
  }

  @Test
  public void saveUserExceptionLoginTest() throws Exception {
    User user = createUser();
    user.setLogin(null);
    when(userRepository.save(user)).thenReturn(user);
    thrown.expect(UserException.class);
    userService.save(user);
  }

  @Test
  public void saveUserExceptionEmailTest() throws Exception {
    User user = createUser();
    user.setEmail(null);
    when(userRepository.save(user)).thenReturn(user);
    thrown.expect(UserException.class);
    userService.save(user);
  }

  @Test
  public void saveUserExceptionPasswordTest() throws Exception {
    User user = createUser();
    user.setPassword(null);
    when(userRepository.save(user)).thenReturn(user);
    thrown.expect(UserException.class);
    userService.save(user);
  }

  @Test
  public void saveUserExceptionCryptoTest() throws Exception {
    User user = createUser();
    thrown.expect(UserException.class);
    when(userRepository.save(user)).thenReturn(user);
    when(userRepository.save(user)).thenReturn(user);
    when(cryptoService.encrypt(Mockito.any())).thenThrow(CryptoException.class);
    userService.save(user);
  }

  @Test
  public void saveUserPasswordEmptyTest() throws Exception {
    User userSave = createUser();
    when(userRepository.save(userSave)).thenReturn(userSave);
    User userFound = userService.save(userSave);
    assertTrue(StringUtils.isEmpty(userFound.getPassword()));
  }

  @Test
  public void updatePasswordTest() throws Exception {
    User user = createUser();
    User userEncrypt = userEncrypt();
    when(userRepository.save(user)).thenReturn(user);
    when(userRepository.findByLogin(Mockito.any())).thenReturn(userEncrypt);
    when(cryptoService.decrypt(Mockito.any())).thenReturn(UnitTest.getProperty("user.password"));
    when(cryptoService.encrypt(Mockito.any())).thenReturn("new12pass");
    UserChangePasswordRequest request =
        UserChangePasswordRequest.builder().login(UnitTest.getProperty("user.login"))
            .newPassword(UnitTest.getProperty("user.newpassword"))
            .oldPassword(UnitTest.getProperty("user.password")).build();
    userService.updatePassword(request);
  }

  @Test
  public void updatePasswordExceptionTest() throws Exception {
    User user = createUser();
    User userEncrypt = userEncrypt();
    when(userRepository.save(user)).thenReturn(user);
    when(userRepository.findByLogin(cryptoService.encrypt(UnitTest.getProperty("user.login"))))
        .thenReturn(userEncrypt);
    UserChangePasswordRequest request =
        UserChangePasswordRequest.builder().login(UnitTest.getProperty("user.login"))
            .newPassword(UnitTest.getProperty("user.newpassword"))
            .oldPassword(RandomStringUtils.randomAlphabetic(6)).build();
    thrown.expect(UserException.class);
    userService.updatePassword(request);
  }


  @Test
  public void deleteTest() throws Exception {
    User user = createUser();
    doNothing().when(userRepository).delete(user);
    userService.delete(UnitTest.getProperty("user.login"));
  }

  @Test
  public void findProfileByIdTest() throws Exception {
    UserProfile userProfile = createUserProfile();
    User user = createUser();
    user.setUserProfile(userProfile);
    when(userRepository.findById(UnitTest.getProperty("user.id"))).thenReturn(Optional.of(user));
    UserProfile userprofile = userService.findProfileByUserId(UnitTest.getProperty("user.id"));
    assertNotNull(userprofile);
  }

  @Test
  public void updateProfileByIdTest() throws Exception {
    UserProfile userProfile = createUserProfile();
    User user = createUser();
    user.setUserProfile(userProfile);

    UserProfile userProfile2 =
        UserProfile.builder().country(UnitTest.getProperty("user.profile.country.other"))
            .city(UnitTest.getProperty("user.profile.city.other"))
            .firstName(UnitTest.getProperty("user.profile.firstname.other")).build();
    User user2 = createUser();
    user2.setUserProfile(userProfile2);

    UserProfile userProfileChanged =
        UserProfile.builder().country(UnitTest.getProperty("user.profile.country.other"))
            .city(UnitTest.getProperty("user.profile.city.other"))
            .firstName(UnitTest.getProperty("user.profile.firstname.other")).build();

    when(userRepository.save(user)).thenReturn(user2);
    when(userRepository.findById(UnitTest.getProperty("user.id"))).thenReturn(Optional.of(user));
    UserProfile userprofile =
        userService.updateProfile(UnitTest.getProperty("user.id"), userProfileChanged);
    assertNotNull(userprofile);
  }

  @Test
  public void findUserFilterTest() throws Exception {
    BooleanBuilder builder = Mockito.any();
    User user = createUser();
    user.setUserProfile(createUserProfile());
    when(userRepository.findAll(builder)).thenReturn(Arrays.asList(user));
    List<User> users = userService.findByFilter(UserRequest.builder().build());
    assertNotNull(users);
  }

  @Test
  public void findUserFilterFieldsTest() throws Exception {
    BooleanBuilder builder = Mockito.any();
    User user = createUser();
    user.setUserProfile(createUserProfile());
    when(userRepository.findAll(builder)).thenReturn(Arrays.asList(user));

    List<User> users = userService.findByFilter(
        UserRequest.builder().firstName(UnitTest.getProperty("user.profile.firstname"))
            .city(UnitTest.getProperty("user.profile.city"))
            .lastName(UnitTest.getProperty("user.profile.lastname"))
            .country(UnitTest.getProperty("user.profile.country"))
            .state(UnitTest.getProperty("user.profile.state")).build());
    assertNotNull(users);
  }

  @Test
  public void findUserFilterExceptionTest() throws Exception {
    User user = createUser();
    user.setUserProfile(createUserProfile());
    thrown.expect(UserException.class);
    userService.findByFilter(null);

  }

  /**
   * Mock user profile data.
   * 
   * @return UserProfile
   */
  private UserProfile createUserProfile() {
    UserProfile userProfile =
        UserProfile.builder().country(UnitTest.getProperty("user.profile.country"))
            .city(UnitTest.getProperty("user.profile.city"))
            .firstName(UnitTest.getProperty("user.profile.firstname")).build();
    return userProfile;
  }

  /**
   * Mock user data.
   * 
   * @return User
   */
  private User createUser() {
    User user = User.builder().id(UnitTest.getProperty("user.id"))
        .login(UnitTest.getProperty("user.login")).email(UnitTest.getProperty("user.email"))
        .password(UnitTest.getProperty("user.password")).build();
    return user;
  }

  /**
   * Mock encrypted user data.
   * 
   * @return User
   * @throws CryptoException if Encryption or decryption goes wrong.
   */
  private User userEncrypt() throws CryptoException {
    User userEncrypt = User.builder().id(UnitTest.getProperty("user.id"))
        .login(UnitTest.getProperty("user.login.encrypt"))
        .email(UnitTest.getProperty("user.email.encrypt"))
        .password(UnitTest.getProperty("user.password.encrypt")).build();
    return userEncrypt;
  }

  @Test
  @SuppressWarnings({"unchecked", "rawtypes"})
  public void mountQUser() throws Exception {
    QUser user = new QUser("user");
    assertNotNull(user);
    PathMetadata metadata = new PathMetadata(null,
        User.builder().id(UnitTest.getProperty("user.id")).build(), PathType.VARIABLE);
    QUser quser = new QUser(metadata);
    assertNotNull(quser);
    DslExpression<?> expression = new BeanPath(User.class, "user");
    Path<User> path = (Path<User>) ExpressionUtils.path(expression.getType(), "user");
    QUser quserPath = new QUser(path);
    assertNotNull(quserPath);
    PathMetadata metadataUserProfile =
        new PathMetadata(null, new String("city"), PathType.VARIABLE);
    QUser quserAddProfile =
        new QUser(User.class, metadataUserProfile, new PathInits("userProfile"));
    assertNotNull(quserAddProfile);
  }

  @Test
  @SuppressWarnings({"unchecked", "rawtypes"})
  public void mountQUserProfile() throws Exception {
    QUserProfile user = new QUserProfile("userProfile");
    assertNotNull(user);
    PathMetadata metadata =
        new PathMetadata(null, UserProfile.builder().city("").build(), PathType.VARIABLE);
    QUserProfile quser = new QUserProfile(metadata);
    assertNotNull(quser);
    DslExpression<?> expression = new BeanPath(User.class, "userProfile");
    Path<UserProfile> path =
        (Path<UserProfile>) ExpressionUtils.path(expression.getType(), "userProfile");
    QUserProfile quserPath = new QUserProfile(path);
    assertNotNull(quserPath);


 
  }
}
