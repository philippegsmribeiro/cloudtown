package cloudtown.storage.model.user;

import static org.junit.Assert.assertNotNull;
import static org.junit.Assert.assertNull;
import static org.mockito.Mockito.mock;
import static org.mockito.Mockito.when;

import cloudtown.storage.UnitTest;
import java.util.Optional;
import org.junit.Before;
import org.junit.Test;

public class AppClientServiceTest extends UnitTest {

  private AppClientService appClientService;
  private AppClientRepository appClientRepository;

  @Before
  public void setUp() throws Exception {
    appClientRepository = mock(AppClientRepository.class);
    appClientService = new AppClientService(appClientRepository);
  }

  @Test
  public void retrieveByIdTest() throws Exception {
    String appClientname = "test";
    
    AppClient fakeAppClient = new AppClient();
    when(appClientRepository.findById(appClientname)).thenReturn(Optional.of(fakeAppClient));

    AppClient appClientFound = appClientService.findByClientId(appClientname);
    assertNotNull(appClientFound);

    AppClient appClientNotFound = appClientService.findByClientId("whatever");
    assertNull(appClientNotFound);

    AppClient appClientNull = appClientService.findByClientId(null);
    assertNull(appClientNull);
  }
  
  @Test
  public void saveTest() throws Exception {
    AppClient appClient = AppClient.builder().build();

    when(appClientRepository.save(appClient))
        .thenReturn(appClient);

    AppClient appClientFound = appClientService.save(appClient);
    assertNotNull(appClientFound);
  }
}
