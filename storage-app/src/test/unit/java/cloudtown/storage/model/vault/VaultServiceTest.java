package cloudtown.storage.model.vault;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertFalse;
import static org.junit.Assert.assertNotNull;
import static org.junit.Assert.assertTrue;
import static org.mockito.Mockito.mock;
import static org.mockito.Mockito.when;

import cloudtown.storage.UnitTest;
import cloudtown.storage.cloud.Provider;
import cloudtown.storage.cloud.provider.ProviderIntegrationException;
import cloudtown.storage.cloud.service.storage.CloudStorageService;
import cloudtown.storage.model.account.AccountException;
import com.amazonaws.regions.Regions;
import java.util.ArrayList;
import java.util.List;
import org.apache.commons.lang3.StringUtils;
import org.junit.Before;
import org.junit.BeforeClass;
import org.junit.Rule;
import org.junit.Test;
import org.junit.rules.ExpectedException;

public class VaultServiceTest extends UnitTest {

  private VaultService vaultService;
  private CloudStorageService cloudStorageService;
  @Rule
  public ExpectedException thrown = ExpectedException.none();
  private static List<Vault> vaults;
  private String accountId = UnitTest.getProperty("accountid");

  /**
   * Pre setup.
   */
  @BeforeClass
  public static void preSetUp() {
    vaults = new ArrayList<>();
    for (int i = 0; i < 5; i++) {
      Vault vault = Vault.builder().id("vault" + i).build();
      vaults.add(vault);
    }

  }

  /**
   * Setup mocks.
   */
  @Before
  public void setup() {
    cloudStorageService = mock(CloudStorageService.class);
    vaultService = new VaultService(cloudStorageService);
  }

  @Test
  public void testListVaults()
      throws ProviderIntegrationException, VaultException, AccountException {
    VaultListRequest vaultListRequest = VaultListRequest.builder().accountId(accountId)
        .provider(Provider.AMAZON_AWS).region(Regions.US_EAST_1.getName()).build();
    List<Vault> list = vaultService.listVaults(vaultListRequest);
    assertNotNull(list);
    assertTrue(list.isEmpty());
  }

  @Test
  public void testMockListVaults()
      throws ProviderIntegrationException, VaultException, AccountException {
    VaultListRequest vaultListRequest = VaultListRequest.builder().accountId(accountId)
        .provider(Provider.AMAZON_AWS).region(Regions.US_EAST_1.getName()).build();
    when(cloudStorageService.listVaults(vaultListRequest)).thenReturn(vaults);
    List<Vault> list = vaultService.listVaults(vaultListRequest);
    assertNotNull(list);
    assertFalse(list.isEmpty());
    assertEquals(vaults, list);
  }

  @Test
  public void testMockVaultsProviderIntegrationException()
      throws ProviderIntegrationException, VaultException, AccountException {
    VaultListRequest vaultListRequest = VaultListRequest.builder().accountId(accountId)
        .provider(Provider.AMAZON_AWS).region(Regions.US_EAST_1.getName()).build();
    thrown.expect(ProviderIntegrationException.class);
    when(cloudStorageService.listVaults(vaultListRequest))
        .thenThrow(ProviderIntegrationException.class);
    vaultService.listVaults(vaultListRequest);
  }

  @Test
  public void testMockVaultsAccountIdEmpty()
      throws ProviderIntegrationException, VaultException, AccountException {
    VaultListRequest vaultListRequest = VaultListRequest.builder().accountId(StringUtils.EMPTY)
        .provider(Provider.AMAZON_AWS).region(Regions.US_EAST_1.getName()).build();
    thrown.expect(VaultException.class);
    vaultService.listVaults(vaultListRequest);
  }
  
  @Test
  public void testMockVaultListRequestNull()
      throws ProviderIntegrationException, VaultException, AccountException {
    thrown.expect(VaultException.class);
    vaultService.listVaults(null);
  }

  @Test
  public void testMockVaultsProviderIsNull()
      throws ProviderIntegrationException, VaultException, AccountException {
    VaultListRequest vaultListRequest =
        VaultListRequest.builder().accountId(accountId).region(Regions.US_EAST_1.getName()).build();
    thrown.expect(VaultException.class);
    vaultService.listVaults(vaultListRequest);
  }

  @Test
  public void testMockVaultsRegionIsEmpty()
      throws ProviderIntegrationException, VaultException, AccountException {
    VaultListRequest vaultListRequest = VaultListRequest.builder().accountId(accountId)
        .provider(Provider.AMAZON_AWS).region(StringUtils.EMPTY).build();
    thrown.expect(VaultException.class);
    vaultService.listVaults(vaultListRequest);
  }
}

