package cloudtown.storage.model.bucket;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertFalse;
import static org.junit.Assert.assertNotNull;
import static org.junit.Assert.assertTrue;
import static org.mockito.Mockito.mock;
import static org.mockito.Mockito.when;

import cloudtown.storage.UnitTest;
import cloudtown.storage.cloud.Provider;
import cloudtown.storage.cloud.provider.ProviderIntegrationException;
import cloudtown.storage.cloud.service.storage.CloudStorageService;
import cloudtown.storage.model.account.AccountException;
import com.amazonaws.regions.Regions;
import com.amazonaws.services.s3.model.AccessControlList;
import java.util.ArrayList;
import java.util.List;
import org.apache.commons.lang3.StringUtils;
import org.junit.Before;
import org.junit.BeforeClass;
import org.junit.Rule;
import org.junit.Test;
import org.junit.rules.ExpectedException;
import org.mockito.Mockito;

public class BucketServiceTest extends UnitTest {

  private BucketService bucketService;
  private CloudStorageService cloudStorageService;
  @Rule
  public ExpectedException thrown = ExpectedException.none();
  private static List<Bucket> buckets;
  private String accountId = UnitTest.getProperty("accountid");
  private String bucketName = UnitTest.getProperty("bucket.name");
  private String bucketRegion = UnitTest.getProperty("bucket.region");

  /**
   * Pre setup.
   */
  @BeforeClass
  public static void preSetUp() {
    buckets = new ArrayList<>();
    for (int i = 0; i < 5; i++) {
      Bucket bucket = Bucket.builder().name("bucket" + i).build();
      buckets.add(bucket);
    }

  }

  /**
   * Setup mocks.
   */
  @Before
  public void setup() {
    cloudStorageService = mock(CloudStorageService.class);
    bucketService = new BucketService(cloudStorageService);
  }

  @Test
  public void testListBuckets()
      throws ProviderIntegrationException, BucketException, AccountException {
    BucketListRequest bucketListRequest = BucketListRequest.builder().accountId(accountId)
        .provider(Provider.AMAZON_AWS).region(Regions.US_EAST_1.getName()).build();
    List<Bucket> list = bucketService.listBuckets(bucketListRequest);
    assertNotNull(list);
    assertTrue(list.isEmpty());
  }

  @Test
  public void testMockListBuckets()
      throws ProviderIntegrationException, BucketException, AccountException {
    BucketListRequest bucketListRequest = BucketListRequest.builder().accountId(accountId)
        .provider(Provider.AMAZON_AWS).region(Regions.US_EAST_1.getName()).build();
    when(cloudStorageService.listBuckets(bucketListRequest)).thenReturn(buckets);
    List<Bucket> list = bucketService.listBuckets(bucketListRequest);
    assertNotNull(list);
    assertFalse(list.isEmpty());
    assertEquals(buckets, list);
  }

  @Test
  public void testMockListBucketsRequestException()
      throws ProviderIntegrationException, BucketException, AccountException {
    thrown.expect(Exception.class);
    BucketListRequest bucketListRequest = null;
    bucketService.listBuckets(bucketListRequest);
  }

  @Test
  public void testMockBucketsProviderIntegrationException()
      throws ProviderIntegrationException, BucketException, AccountException {
    BucketListRequest bucketListRequest = BucketListRequest.builder().accountId(accountId)
        .provider(Provider.AMAZON_AWS).region(Regions.US_EAST_1.getName()).build();
    thrown.expect(ProviderIntegrationException.class);
    when(cloudStorageService.listBuckets(bucketListRequest))
        .thenThrow(ProviderIntegrationException.class);
    bucketService.listBuckets(bucketListRequest);
  }

  @Test
  public void testMockBucketsAccountIdEmpty()
      throws ProviderIntegrationException, BucketException, AccountException {
    BucketListRequest bucketListRequest = BucketListRequest.builder().accountId(StringUtils.EMPTY)
        .provider(Provider.AMAZON_AWS).region(Regions.US_EAST_1.getName()).build();
    thrown.expect(BucketException.class);
    bucketService.listBuckets(bucketListRequest);
  }

  @Test
  public void testMockBucketsProviderIsNull()
      throws ProviderIntegrationException, BucketException, AccountException {
    BucketListRequest bucketListRequest = BucketListRequest.builder().accountId(accountId)
        .region(Regions.US_EAST_1.getName()).build();
    thrown.expect(BucketException.class);
    bucketService.listBuckets(bucketListRequest);
  }

  @Test
  public void testMockBucketsRegionIsEmpty()
      throws ProviderIntegrationException, BucketException, AccountException {
    BucketListRequest bucketListRequest = BucketListRequest.builder().accountId(accountId)
        .provider(Provider.AMAZON_AWS).region(StringUtils.EMPTY).build();
    thrown.expect(BucketException.class);
    bucketService.listBuckets(bucketListRequest);
  }

  @Test
  public void testValidateRequestDetailBucketRegion()
      throws ProviderIntegrationException, BucketException, AccountException {
    BucketDetailRequest bucketDetailRequest = BucketDetailRequest.builder()
        .region(StringUtils.EMPTY).name(bucketName).accountId(accountId).build();
    thrown.expect(BucketException.class);
    bucketService.getAccessControlBucket(bucketDetailRequest);
  }
  
  @Test
  public void testValidateRequestDetailBucketName()
      throws ProviderIntegrationException, BucketException, AccountException {
    BucketDetailRequest bucketDetailRequest1 = BucketDetailRequest.builder()
        .region(Regions.US_EAST_1.getName()).name(StringUtils.EMPTY).accountId(accountId).build();
    thrown.expect(BucketException.class);
    bucketService.getAccessControlBucket(bucketDetailRequest1);
  }
  
  @Test
  public void testValidateRequestDetailBucketAccountId()
      throws ProviderIntegrationException, BucketException, AccountException {
    BucketDetailRequest bucketDetailRequest2 = BucketDetailRequest.builder()
        .region(Regions.US_EAST_1.getName()).name(bucketName).accountId(StringUtils.EMPTY).build();
    thrown.expect(BucketException.class);
    bucketService.getAccessControlBucket(bucketDetailRequest2);
  }
  
  @Test
  public void testValidateRequestDetailBucketNull()
      throws ProviderIntegrationException, BucketException, AccountException {
    BucketDetailRequest bucketDetailRequest3 = null;
    thrown.expect(BucketException.class);
    bucketService.getAccessControlBucket(bucketDetailRequest3);
  }
  
 

  @Test
  public void testGetAccessControlBucket()
      throws ProviderIntegrationException, BucketException, AccountException {
    when(cloudStorageService.getAccessControlBucket(Mockito.any()))
        .thenReturn(this.getBucketAccessControl());
    BucketDetailRequest bucketDetailRequest = BucketDetailRequest.builder()
        .region(Regions.US_EAST_1.getName()).name(bucketName).accountId(accountId).build();
    AccessControlList list = bucketService.getAccessControlBucket(bucketDetailRequest);
    assertNotNull(list);
  }

  @Test
  public void testGetRegionBucket()
      throws ProviderIntegrationException, BucketException, AccountException {
    when(cloudStorageService.getRegionBucket(Mockito.any())).thenReturn(bucketRegion);
    BucketDetailRequest bucketDetailRequest = BucketDetailRequest.builder()
        .region(Regions.US_EAST_1.getName()).name(bucketName).accountId(accountId).build();
    String region = bucketService.getRegionBucket(bucketDetailRequest);
    assertNotNull(region);
  }

  @Test
  public void testBucketDetail()
      throws ProviderIntegrationException, BucketException, AccountException {
    when(cloudStorageService.getBucketDetail(Mockito.any()))
        .thenReturn(BucketDetail.builder().build());
    BucketDetailRequest bucketDetailRequest = BucketDetailRequest.builder()
        .region(Regions.US_EAST_1.getName()).name(bucketName).accountId(accountId).build();
    BucketDetail bucketDetail = bucketService.getBucketDetail(bucketDetailRequest);
    assertNotNull(bucketDetail);
  }

  private AccessControlList getBucketAccessControl() {
    AccessControlList permission = new AccessControlList();
    return permission;
  }
}

