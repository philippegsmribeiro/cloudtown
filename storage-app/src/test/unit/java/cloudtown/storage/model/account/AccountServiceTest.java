package cloudtown.storage.model.account;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertFalse;
import static org.junit.Assert.assertNotNull;
import static org.junit.Assert.assertNull;
import static org.junit.Assert.assertTrue;
import static org.mockito.Mockito.doThrow;
import static org.mockito.Mockito.mock;
import static org.mockito.Mockito.when;

import cloudtown.storage.UnitTest;
import cloudtown.storage.cloud.Provider;
import com.querydsl.core.BooleanBuilder;
import com.querydsl.core.types.ExpressionUtils;
import com.querydsl.core.types.Path;
import com.querydsl.core.types.PathMetadata;
import com.querydsl.core.types.PathType;
import com.querydsl.core.types.dsl.BeanPath;
import com.querydsl.core.types.dsl.DslExpression;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;
import java.util.Optional;
import org.apache.commons.lang3.StringUtils;
import org.junit.Before;
import org.junit.Rule;
import org.junit.Test;
import org.junit.rules.ExpectedException;
import org.mockito.Mockito;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageImpl;
import org.springframework.data.domain.PageRequest;


public class AccountServiceTest extends UnitTest {

  private String accountId = UnitTest.getProperty("account.accountid");
  private String secretKey = UnitTest.getProperty("account.secretkey");
  private String key = UnitTest.getProperty("account.key");
  private String secretKey2 = UnitTest.getProperty("account.secretkey2");
  private String key2 = UnitTest.getProperty("account.key2");

  private AccountService accountService;
  private AccountRepository accountRepository;
  @Rule
  public ExpectedException thrown = ExpectedException.none();

  @Before
  public void setUp() throws Exception {
    accountRepository = mock(AccountRepository.class);
    accountService = new AccountService(accountRepository);
  }

  @Test
  public void saveTest() throws Exception {
    Account account = mockAccount();
    when(accountRepository.save(account)).thenReturn(account);
    Account accountFound = accountService.save(account);
    assertNotNull(accountFound);
  }

  @Test
  public void saveTestSecretKeyEmpty() throws Exception {
    Account account = mockAccount();
    when(accountRepository.save(account)).thenReturn(account);
    Account accountFound = accountService.save(account);
    assertNotNull(accountFound);
    for (Credential credentialResponse : account.getCredentials()) {
      assertTrue(credentialResponse.getAwsCredential().getSecretKey().isEmpty());
    }
  }

  private Account mockAccount() {
    List<Credential> credentials = new ArrayList<>();
    Credential credential =
        Credential.builder().awsCredential(new AwsCredential(accountId, key, secretKey)).build();
    credentials.add(credential);
    Account account = Account.builder().id(accountId).provider(Provider.AMAZON_AWS)
        .credentials(credentials).build();
    return account;
  }

  @Test
  public void saveTestValidIdEmpty() throws Exception {
    Account account = Account.builder().id(StringUtils.EMPTY).provider(Provider.AMAZON_AWS).build();
    thrown.expect(AccountException.class);
    when(accountRepository.save(account)).thenReturn(account);
    accountService.save(account);
  }

  @Test
  public void saveAccountIsNull() throws Exception {
    Account account = null;
    thrown.expect(AccountException.class);
    accountService.save(account);
  }

  @Test
  public void saveTestValidProviderEmpty() throws Exception {
    Account account = Account.builder().id(accountId).build();
    thrown.expect(AccountException.class);
    when(accountRepository.save(account)).thenReturn(account);
    accountService.save(account);
  }


  @Test
  public void retrieveByIdTest() throws Exception {
    Account account = mockAccount();
    when(accountRepository.findById(accountId)).thenReturn(Optional.of(account));
    Account accountFound = accountService.retrieveById(accountId, true);
    assertNotNull(accountFound);
    Account accountNotFound = accountService.retrieveById("whatever", true);
    assertNull(accountNotFound);
  }

  @Test
  public void retrieveByIdEmpty() throws Exception {
    Account account = mockAccount();
    thrown.expect(AccountException.class);
    when(accountRepository.findById(accountId)).thenReturn(Optional.of(account));
    accountService.retrieveById(StringUtils.EMPTY, true);
  }

  @Test
  public void retrieveByIdAndReturnSecretKeyEmpty() throws Exception {
    Account account = mockAccount();
    when(accountRepository.findById(accountId)).thenReturn(Optional.of(account));
    Account accountFound = accountService.retrieveById(accountId, true);
    for (Credential credentialResponse : accountFound.getCredentials()) {
      assertTrue(credentialResponse.getAwsCredential().getSecretKey().isEmpty());
    }
  }

  @Test
  public void retrieveAllTest() throws Exception {
    PageRequest pageRequest = PageRequest.of(0, 100);
    when(accountRepository.findAll(pageRequest))
        .thenReturn(new PageImpl<>(Arrays.asList(mockAccount()), pageRequest, 1));
    Page<Account> accountFound = accountService.retrieveAll(pageRequest);
    assertNotNull(accountFound);
    Page<Account> accountNotFound = accountService.retrieveAll(PageRequest.of(0, 10));
    assertNull(accountNotFound);
  }

  @Test
  public void retrieveAllTestPageRequestNull() throws Exception {
    PageRequest pageRequest = null;
    thrown.expect(AccountException.class);
    accountService.retrieveAll(pageRequest);
  }

  @Test
  public void retrieveAllTestReturnSecretKeyEmpty() throws Exception {
    PageRequest pageRequest = PageRequest.of(0, 100);
    when(accountRepository.findAll(pageRequest))
        .thenReturn(new PageImpl<>(Arrays.asList(mockAccount()), pageRequest, 1));
    Page<Account> accountFound = accountService.retrieveAll(pageRequest);
    for (Account account : accountFound.getContent()) {
      for (Credential credentialResponse : account.getCredentials()) {
        assertTrue(credentialResponse.getAwsCredential().getSecretKey().isEmpty());
      }
    }
  }

  @Test
  public void deleteTest() throws Exception {
    String id = accountId;
    when(accountRepository.findById(id)).thenReturn(Optional.empty());
    Boolean accountFound = accountService.deleteById(id);
    assertNotNull(accountFound);
    assertTrue(accountFound);
  }

  @Test
  public void deleteNotExecutedTest() throws Exception {
    String id = accountId;
    when(accountRepository.findById(id)).thenReturn(Optional.of(new Account()));
    Boolean accountFound = accountService.deleteById(id);
    assertNotNull(accountFound);
    assertFalse(accountFound);
  }

  @Test
  public void deleteTestIdEmpty() throws Exception {
    String id = StringUtils.EMPTY;
    thrown.expect(AccountException.class);
    when(accountRepository.findById(id)).thenReturn(Optional.of(new Account()));
    accountService.deleteById(id);
  }

  @Test
  public void deleteTestException() throws Exception {
    String id = accountId;
    thrown.expect(Exception.class);
    when(accountRepository.findById(id)).thenReturn(Optional.of(new Account()));
    doThrow(new RuntimeException()).when(accountRepository).deleteById(Mockito.anyString());
    accountService.deleteById(id);
  }

  @Test
  public void getFirstCredentialTestReturnSecretKeyEmpty() throws Exception {
    String id = accountId;
    Credential credential1 =
        Credential.builder().awsCredential(new AwsCredential(accountId, key, secretKey)).build();
    Credential credential2 =
        Credential.builder().awsCredential(new AwsCredential(accountId, key2, secretKey2)).build();
    Account value = Account.builder().credentials(Arrays.asList(credential1, credential2)).build();
    when(accountRepository.findById(id)).thenReturn(Optional.of(value));
    Credential credentialFound = accountService.getFirstCredential(id, true);
    assertTrue(credentialFound.getAwsCredential().getSecretKey().isEmpty());
  }

  @Test
  public void getFirstCredentialTestAccountNull() throws Exception {
    String id = accountId;
    when(accountRepository.findById(id)).thenReturn(Optional.empty());
    Credential credentialNull = accountService.getFirstCredential(id, true);
    assertNull(credentialNull);
  }


  @Test
  public void getFirstCredentialTest() throws Exception {
    String id = accountId;
    Credential credential1 =
        Credential.builder().awsCredential(new AwsCredential(accountId, key, secretKey)).build();
    Credential credential2 =
        Credential.builder().awsCredential(new AwsCredential(accountId, key2, secretKey2)).build();
    Account value = Account.builder().credentials(Arrays.asList(credential1, credential2)).build();
    when(accountRepository.findById(id)).thenReturn(Optional.of(value));
    Credential credentialFound = accountService.getFirstCredential(id, true);
    assertNotNull(credentialFound);
    assertEquals(credential1, credentialFound);

    Credential credentialNull = accountService.getFirstCredential(null, false);
    assertNull(credentialNull);
  }

  @Test
  public void findAccountFilterTest() throws Exception {
    BooleanBuilder builder = Mockito.any();
    when(accountRepository.findAll(builder)).thenReturn(Arrays.asList(mockAccount()));
    List<Account> account = accountService.findByFilter(AccountRequest.builder().id(accountId)
        .provider(Provider.AMAZON_AWS).description("test").build());
    assertNotNull(account);
  }

  @Test
  public void findAccountFilterRequestIsNull() throws Exception {
    BooleanBuilder builder = Mockito.any();
    thrown.expect(AccountException.class);
    when(accountRepository.findAll(builder)).thenReturn(Arrays.asList(mockAccount()));
    accountService.findByFilter(null);
  }

  @Test
  public void findAccountFilterTestReturnSecretKeyEmpty() throws Exception {
    BooleanBuilder builder = Mockito.any();
    when(accountRepository.findAll(builder)).thenReturn(Arrays.asList(mockAccount()));
    List<Account> accountResult = accountService
        .findByFilter(AccountRequest.builder().id(accountId).provider(Provider.AMAZON_AWS).build());
    for (Account account : accountResult) {
      for (Credential credentialResponse : account.getCredentials()) {
        assertTrue(credentialResponse.getAwsCredential().getSecretKey().isEmpty());
      }
    }
  }

  @Test
  @SuppressWarnings({"unchecked", "rawtypes"})
  public void mountQAccountTest() throws Exception {
    PathMetadata metadata = new PathMetadata(null, mockAccount(), PathType.VARIABLE);
    QAccount qaccount = new QAccount(metadata);
    assertNotNull(qaccount);

    DslExpression<?> expression = new BeanPath(Account.class, "id");
    Path<Account> path = (Path<Account>) ExpressionUtils.path(expression.getType(), "id");
    QAccount qaccountPath = new QAccount(path);
    assertNotNull(qaccountPath);
  }

  @Test
  @SuppressWarnings({"unchecked", "rawtypes"})
  public void mountQCredentialTest() throws Exception {
    QCredential credential = new QCredential("accessKey");
    assertNotNull(credential);
    PathMetadata metadata = new PathMetadata(null,
        Credential.builder().awsCredential(new AwsCredential(accountId, key, secretKey)).build(),
        PathType.VARIABLE);
    QCredential qcredential = new QCredential(metadata);
    assertNotNull(qcredential);
    DslExpression<?> expression = new BeanPath(Account.class, "accessKey");
    Path<Credential> path =
        (Path<Credential>) ExpressionUtils.path(expression.getType(), "accessKey");
    QCredential qcredPath = new QCredential(path);
    assertNotNull(qcredPath);
  }
}
