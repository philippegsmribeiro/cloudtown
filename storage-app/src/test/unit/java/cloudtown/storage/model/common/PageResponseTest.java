package cloudtown.storage.model.common;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertNotEquals;
import static org.junit.Assert.assertTrue;

import cloudtown.storage.UnitTest;
import java.util.Arrays;
import java.util.List;
import org.junit.Test;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageImpl;
import org.springframework.data.domain.PageRequest;

public class PageResponseTest extends UnitTest {

  /**
   * Test if the object is correct.
   */
  @Test
  public void objectTest() {
    List<String> content = Arrays.asList("1", "2", "3");
    Integer pageNumber = 0;
    Integer pageSize = 10;

    Page<String> page =
        new PageImpl<>(content, PageRequest.of(pageNumber, pageSize), content.size());

    PageResponse<String> fromPage = PageResponse.fromPage(page);

    // actual page number
    assertEquals(pageNumber, fromPage.getPage());
    // page size
    assertEquals(pageSize, fromPage.getPageSize());
    // content
    assertEquals(content, fromPage.getContent());
    // elements size
    assertTrue(fromPage.getContent().size() <= pageSize);
    // total size
    assertEquals(content.size(), fromPage.getTotalElements().intValue());
  }

  /**
   * Test if pagination is correct.
   */
  @Test
  public void paginationTest() {
    List<String> content = Arrays.asList("1", "2", "3", "4", "5");
    Integer pageNumber = 1;
    Integer pageSize = 2;

    Page<String> page =
        new PageImpl<>(content.subList(2, 4), PageRequest.of(pageNumber, pageSize), content.size());

    PageResponse<String> fromPage = PageResponse.fromPage(page);

    // page number
    assertEquals(pageNumber, fromPage.getPage());
    // page size = elements size
    assertEquals(pageSize.intValue(), fromPage.getContent().size());
    // content is not equal
    assertNotEquals(content, fromPage.getContent());
    // pagination content
    assertEquals(fromPage.getContent(), content.subList(2, 4));
    // total elements
    assertEquals(content.size(), fromPage.getTotalElements().intValue());


  }

}
