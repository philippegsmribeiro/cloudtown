package cloudtown.storage.model.example;

import static org.junit.Assert.assertFalse;
import static org.junit.Assert.assertNotNull;
import static org.junit.Assert.assertNull;
import static org.junit.Assert.assertTrue;
import static org.mockito.Mockito.mock;
import static org.mockito.Mockito.when;

import cloudtown.storage.UnitTest;
import java.util.Arrays;
import java.util.Optional;
import org.junit.Before;
import org.junit.Test;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageImpl;
import org.springframework.data.domain.PageRequest;

public class ExampleServiceTest extends UnitTest {

  private ExampleService exampleService;
  private ExampleRepository exampleRepository;

  @Before
  public void setUp() throws Exception {
    exampleRepository = mock(ExampleRepository.class);
    exampleService = new ExampleService(exampleRepository);
  }

  @Test
  public void retrieveByIdTest() throws Exception {
    String id = "test";

    Example fakeExample = new Example();
    when(exampleRepository.findById(id)).thenReturn(Optional.of(fakeExample));

    Example exampleFound = exampleService.retrieveById(id);
    assertNotNull(exampleFound);

    Example exampleNotFound = exampleService.retrieveById("whatever");
    assertNull(exampleNotFound);

    Example exampleNull = exampleService.retrieveById(null);
    assertNull(exampleNull);
  }

  @Test
  public void retrieveAllTest() throws Exception {
    PageRequest pageRequest = PageRequest.of(0, 100);

    when(exampleRepository.findAll(pageRequest))
        .thenReturn(new PageImpl<>(Arrays.asList(new Example()), pageRequest, 1));

    Page<Example> exampleFound = exampleService.retrieveAll(pageRequest);
    assertNotNull(exampleFound);

    Page<Example> exampleNotFound = exampleService.retrieveAll(PageRequest.of(0, 10));
    assertNull(exampleNotFound);

    Page<Example> exampleNull = exampleService.retrieveAll(null);
    assertNull(exampleNull);
  }
  
  @Test
  public void saveTest() throws Exception {
    Example example = Example.builder().build();

    when(exampleRepository.save(example))
        .thenReturn(example);

    Example exampleFound = exampleService.save(example);
    assertNotNull(exampleFound);
  }
  
  @Test
  public void deleteTest() throws Exception {
    String id = "test";

    when(exampleRepository.findById(id))
        .thenReturn(Optional.empty());

    Boolean exampleFound = exampleService.deleteById(id);
    assertNotNull(exampleFound);
    assertTrue(exampleFound);
  }
  
  @Test
  public void deleteNotExecutedTest() throws Exception {
    String id = "test";

    when(exampleRepository.findById(id))
        .thenReturn(Optional.of(new Example()));

    Boolean exampleFound = exampleService.deleteById(id);
    assertNotNull(exampleFound);
    assertFalse(exampleFound);
  }

}
