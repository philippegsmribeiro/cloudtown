package cloudtown.storage.api;

import static org.junit.Assert.assertEquals;
import static org.mockito.Mockito.mock;
import static org.mockito.Mockito.when;

import cloudtown.storage.UnitTest;
import cloudtown.storage.cloud.Provider;
import cloudtown.storage.model.region.RegionService;
import org.junit.Before;
import org.junit.Test;
import org.mockito.Mockito;
import org.springframework.http.HttpStatus;
import org.springframework.mock.web.MockHttpServletResponse;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.setup.MockMvcBuilders;

/**
 * Provider Controller Unit Test.
 */
public class ProviderControllerTest extends UnitTest {

  private MockMvc mvc;
  private ProviderController providerController;
  private RegionService regionService;

  /**
   * Setup the objects with the mocks.
   * 
   * @throws Exception if something goes wrong
   */
  @Before
  public void setUp() throws Exception {
    regionService = mock(RegionService.class);
    providerController = new ProviderController(regionService);
    mvc = MockMvcBuilders.standaloneSetup(providerController).build();
  }

  @Test
  public void getTest() throws Exception {
    MockHttpServletResponse response = executeGet(mvc, ProviderController.API);
    assertEquals(HttpStatus.OK.value(), response.getStatus());
  }

  @Test
  public void listRegionsTest() throws Exception {
    MockHttpServletResponse response = executeGet(mvc,
        ProviderController.API + ProviderController.REGIONS, Provider.AMAZON_AWS, "123456789");
    assertEquals(HttpStatus.OK.value(), response.getStatus());

    when(regionService.list(Mockito.any(), Mockito.any())).thenThrow(new RuntimeException());
    response = executeGet(mvc, ProviderController.API + ProviderController.REGIONS,
        Provider.AMAZON_AWS, "123456789");
    assertEquals(HttpStatus.INTERNAL_SERVER_ERROR.value(), response.getStatus());
  }

}
