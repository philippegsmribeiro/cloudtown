package cloudtown.storage.api;

import static org.junit.Assert.assertEquals;
import static org.mockito.Mockito.mock;
import static org.mockito.Mockito.when;

import cloudtown.storage.UnitTest;
import cloudtown.storage.cloud.Provider;
import cloudtown.storage.cloud.provider.ProviderIntegrationException;
import cloudtown.storage.cloud.service.storage.CloudStorageService;
import cloudtown.storage.model.bucket.Bucket;
import cloudtown.storage.model.bucket.BucketDetail;
import cloudtown.storage.model.bucket.BucketDetailRequest;
import cloudtown.storage.model.bucket.BucketListRequest;
import cloudtown.storage.model.bucket.BucketService;
import com.amazonaws.regions.Regions;
import com.amazonaws.services.s3.model.AccessControlList;
import java.util.ArrayList;
import java.util.List;
import org.apache.commons.lang3.StringUtils;
import org.junit.Before;
import org.junit.Test;
import org.springframework.http.HttpStatus;
import org.springframework.mock.web.MockHttpServletResponse;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.setup.MockMvcBuilders;


/**
 * Bucket Controller Unit Test.
 */
public class BucketControllerTest extends UnitTest {

  private MockMvc mvc;
  private BucketController bucketController;
  private CloudStorageService cloudStorageService;
  private BucketService bucketService;

  private static List<Bucket> buckets;
  private static final String ACCOUNT_ID = UnitTest.getProperty("accountid");
  private String bucketName = UnitTest.getProperty("bucket.name");
  private String bucketRegion = UnitTest.getProperty("bucket.region");

  /**
   * Setup the objects with the mocks.
   * 
   * @throws Exception if something goes wrong
   */
  @Before
  public void setUp() throws Exception {
    cloudStorageService = mock(CloudStorageService.class);
    bucketService = new BucketService(cloudStorageService);
    bucketController = new BucketController(bucketService);
    mvc = MockMvcBuilders.standaloneSetup(bucketController).build();
    this.createBuckets();
  }

  /**
   * A method that mocks the buckets.
   */
  public void createBuckets() {
    buckets = new ArrayList<>();
    for (int i = 0; i < 5; i++) {
      Bucket bucket = Bucket.builder().name("bucket" + i).build();
      buckets.add(bucket);
    }
  }

  @Test
  public void getTest() throws Exception {
    MockHttpServletResponse response = executeGet(mvc, BucketController.API);
    assertEquals(HttpStatus.OK.value(), response.getStatus());
  }

  @Test
  public void testListBuckets() throws Exception {
    BucketListRequest bucketListRequest = BucketListRequest.builder().accountId(ACCOUNT_ID)
        .provider(Provider.AMAZON_AWS).region(Regions.US_EAST_1.getName()).build();
    when(cloudStorageService.listBuckets(bucketListRequest)).thenReturn(buckets);
    MockHttpServletResponse response =
        executePost(mvc, BucketController.API + BucketController.LIST, bucketListRequest);
    assertEquals(HttpStatus.OK.value(), response.getStatus());
  }

  @Test
  public void testBucketsProviderIntegrationException() throws Exception {
    BucketListRequest bucketListRequest = BucketListRequest.builder().accountId(ACCOUNT_ID)
        .provider(Provider.AMAZON_AWS).region(Regions.US_EAST_1.getName()).build();
    when(cloudStorageService.listBuckets(bucketListRequest))
        .thenThrow(ProviderIntegrationException.class);
    MockHttpServletResponse response =
        executePost(mvc, BucketController.API + BucketController.LIST, bucketListRequest);
    assertEquals(HttpStatus.INTERNAL_SERVER_ERROR.value(), response.getStatus());
  }

  @Test
  public void testListBucketsProviderException() throws Exception {
    BucketListRequest bucketListRequest = BucketListRequest.builder().accountId(ACCOUNT_ID)
        .region(Regions.US_EAST_1.getName()).build();
    MockHttpServletResponse response =
        executePost(mvc, BucketController.API + BucketController.LIST, bucketListRequest);
    assertEquals(HttpStatus.PRECONDITION_FAILED.value(), response.getStatus());
  }

  @Test
  public void testListBucketsAccountIdException() throws Exception {
    BucketListRequest bucketListRequest = BucketListRequest.builder().accountId(StringUtils.EMPTY)
        .provider(Provider.AMAZON_AWS).region(Regions.US_EAST_1.getName()).build();
    MockHttpServletResponse response =
        executePost(mvc, BucketController.API + BucketController.LIST, bucketListRequest);
    assertEquals(HttpStatus.PRECONDITION_FAILED.value(), response.getStatus());
  }


  @Test
  public void testListBucketsRegionException() throws Exception {
    BucketListRequest bucketListRequest = BucketListRequest.builder().accountId(ACCOUNT_ID)
        .provider(Provider.AMAZON_AWS).region(StringUtils.EMPTY).build();
    MockHttpServletResponse response =
        executePost(mvc, BucketController.API + BucketController.LIST, bucketListRequest);
    assertEquals(HttpStatus.PRECONDITION_FAILED.value(), response.getStatus());
  }

  @Test
  public void testListBucketsExceptionTest() throws Exception {
    BucketListRequest bucketListRequest = BucketListRequest.builder().accountId(ACCOUNT_ID)
        .provider(Provider.AMAZON_AWS).region(Regions.US_EAST_1.getName()).build();
    when(cloudStorageService.listBuckets(bucketListRequest))
        .thenThrow(new RuntimeException("Error"));
    MockHttpServletResponse response =
        executePost(mvc, BucketController.API + BucketController.LIST, bucketListRequest);
    assertEquals(HttpStatus.INTERNAL_SERVER_ERROR.value(), response.getStatus());
  }

  @Test
  public void testGetBucketDetail() throws Exception {
    BucketDetailRequest bucketDetailRequest = BucketDetailRequest.builder().name(bucketName)
        .accountId(ACCOUNT_ID).region(bucketRegion).build();
    when(cloudStorageService.getBucketDetail(bucketDetailRequest))
        .thenReturn(BucketDetail.builder().build());
    MockHttpServletResponse response =
        executePost(mvc, BucketController.API + BucketController.DETAIL, bucketDetailRequest);
    assertEquals(HttpStatus.OK.value(), response.getStatus());
  }

  @Test
  public void testGetBucketDetailException() throws Exception {
    BucketDetailRequest bucketDetailRequest =
        BucketDetailRequest.builder().accountId(ACCOUNT_ID).build();
    MockHttpServletResponse response =
        executePost(mvc, BucketController.API + BucketController.DETAIL, bucketDetailRequest);
    assertEquals(HttpStatus.PRECONDITION_FAILED.value(), response.getStatus());
  }
  
  @Test
  public void testGetBucketDetailRuntimeError() throws Exception {
    BucketDetailRequest bucketDetailRequest = BucketDetailRequest.builder().name(bucketName)
        .accountId(ACCOUNT_ID).region(bucketRegion).build();
    when(cloudStorageService.getBucketDetail(bucketDetailRequest))
        .thenThrow(new RuntimeException("Error"));
    MockHttpServletResponse response =
        executePost(mvc, BucketController.API + BucketController.DETAIL, bucketDetailRequest);
    assertEquals(HttpStatus.INTERNAL_SERVER_ERROR.value(), response.getStatus());
  }
  
  @Test
  public void testGetBucketDetailIntegrationException() throws Exception {
    BucketDetailRequest bucketDetailRequest = BucketDetailRequest.builder().name(bucketName)
        .accountId(ACCOUNT_ID).region(bucketRegion).build();
    when(cloudStorageService.getBucketDetail(bucketDetailRequest))
        .thenThrow(ProviderIntegrationException.class);
    MockHttpServletResponse response =
        executePost(mvc, BucketController.API + BucketController.DETAIL, bucketDetailRequest);
    assertEquals(HttpStatus.INTERNAL_SERVER_ERROR.value(), response.getStatus());
  }
  
  @Test
  public void testGetBucketRegion() throws Exception {
    BucketDetailRequest bucketDetailRequest = BucketDetailRequest.builder().name(bucketName)
        .accountId(ACCOUNT_ID).region(bucketRegion).build();
    when(cloudStorageService.getRegionBucket(bucketDetailRequest))
        .thenReturn(bucketRegion);
    MockHttpServletResponse response =
        executePost(mvc, BucketController.API + BucketController.REGION, bucketDetailRequest);
    assertEquals(HttpStatus.OK.value(), response.getStatus());
  }

  @Test
  public void testGetBucketRegionException() throws Exception {
    BucketDetailRequest bucketDetailRequest =
        BucketDetailRequest.builder().accountId(ACCOUNT_ID).build();
    MockHttpServletResponse response =
        executePost(mvc, BucketController.API + BucketController.REGION, bucketDetailRequest);
    assertEquals(HttpStatus.PRECONDITION_FAILED.value(), response.getStatus());
  }
  
  @Test
  public void testGetBucketRegionRuntimeError() throws Exception {
    BucketDetailRequest bucketDetailRequest = BucketDetailRequest.builder().name(bucketName)
        .accountId(ACCOUNT_ID).region(bucketRegion).build();
    when(cloudStorageService.getRegionBucket(bucketDetailRequest))
        .thenThrow(new RuntimeException("Error"));
    MockHttpServletResponse response =
        executePost(mvc, BucketController.API + BucketController.REGION, bucketDetailRequest);
    assertEquals(HttpStatus.INTERNAL_SERVER_ERROR.value(), response.getStatus());
  }
  
  @Test
  public void testGetBucketRegionIntegrationException() throws Exception {
    BucketDetailRequest bucketDetailRequest = BucketDetailRequest.builder().name(bucketName)
        .accountId(ACCOUNT_ID).region(bucketRegion).build();
    when(cloudStorageService.getRegionBucket(bucketDetailRequest))
        .thenThrow(ProviderIntegrationException.class);
    MockHttpServletResponse response =
        executePost(mvc, BucketController.API + BucketController.REGION, bucketDetailRequest);
    assertEquals(HttpStatus.INTERNAL_SERVER_ERROR.value(), response.getStatus());
  }
  
  @Test
  public void testBucketPermissionDetail() throws Exception {
    BucketDetailRequest bucketDetailRequest = BucketDetailRequest.builder().name(bucketName)
        .accountId(ACCOUNT_ID).region(bucketRegion).build();
    when(cloudStorageService.getAccessControlBucket(bucketDetailRequest))
        .thenReturn(this.getBucketAccessControl());
    MockHttpServletResponse response =
        executePost(mvc, BucketController.API + BucketController.PERMISSION, bucketDetailRequest);
    assertEquals(HttpStatus.OK.value(), response.getStatus());
  }

  @Test
  public void testBucketPermissionDetailException() throws Exception {
    BucketDetailRequest bucketDetailRequest =
        BucketDetailRequest.builder().accountId(ACCOUNT_ID).build();
    MockHttpServletResponse response =
        executePost(mvc, BucketController.API + BucketController.PERMISSION, bucketDetailRequest);
    assertEquals(HttpStatus.PRECONDITION_FAILED.value(), response.getStatus());
  }
  
  @Test
  public void testBucketPermissionDetailRuntimeError() throws Exception {
    BucketDetailRequest bucketDetailRequest = BucketDetailRequest.builder().name(bucketName)
        .accountId(ACCOUNT_ID).region(bucketRegion).build();
    when(cloudStorageService.getAccessControlBucket(bucketDetailRequest))
        .thenThrow(new RuntimeException("Error"));
    MockHttpServletResponse response =
        executePost(mvc, BucketController.API + BucketController.PERMISSION, bucketDetailRequest);
    assertEquals(HttpStatus.INTERNAL_SERVER_ERROR.value(), response.getStatus());
  }
  
  @Test
  public void testBucketPermissionDetailIntegrationException() throws Exception {
    BucketDetailRequest bucketDetailRequest = BucketDetailRequest.builder().name(bucketName)
        .accountId(ACCOUNT_ID).region(bucketRegion).build();
    when(cloudStorageService.getAccessControlBucket(bucketDetailRequest))
        .thenThrow(ProviderIntegrationException.class);
    MockHttpServletResponse response =
        executePost(mvc, BucketController.API + BucketController.PERMISSION, bucketDetailRequest);
    assertEquals(HttpStatus.INTERNAL_SERVER_ERROR.value(), response.getStatus());
  }
  
  private AccessControlList getBucketAccessControl() {
    AccessControlList permission = new AccessControlList();
    return permission;
  }
}
