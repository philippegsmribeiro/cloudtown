package cloudtown.storage.api;

import static org.junit.Assert.assertEquals;
import static org.mockito.Mockito.mock;
import static org.mockito.Mockito.when;

import cloudtown.storage.UnitTest;
import cloudtown.storage.model.example.Example;
import cloudtown.storage.model.example.ExampleService;
import org.junit.Before;
import org.junit.Test;
import org.mockito.Mockito;
import org.springframework.http.HttpStatus;
import org.springframework.mock.web.MockHttpServletResponse;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.setup.MockMvcBuilders;

/**
 * Unit Test the rest calls (url, paths, params, etcs).<br/>
 * All the other services inside the controllers has to be mocked.<br/>
 * Normal unit test - not loading spring context (@Autowired not valid).
 */
public class ExampleControllerTest extends UnitTest {

  private MockMvc mvc;
  private ExampleController exampleController;
  private ExampleService exampleService;

  /**
   * Setup the objects with the mocks.
   * 
   * @throws Exception if something goes wrong
   */
  @Before
  public void setUp() throws Exception {
    exampleService = mock(ExampleService.class);
    exampleController = new ExampleController(exampleService);
    mvc = MockMvcBuilders.standaloneSetup(exampleController).build();
  }

  @Test
  public void getTest() throws Exception {
    MockHttpServletResponse response = executeGet(mvc, ExampleController.API);
    assertEquals(HttpStatus.OK.value(),response.getStatus());
  }

  @Test
  public void getOneTest() throws Exception {
    MockHttpServletResponse response =
        executeGet(mvc, ExampleController.API + ExampleController.DESCRIBE, "id");
    assertEquals(HttpStatus.OK.value(),response.getStatus());

    when(exampleService.retrieveById(Mockito.any())).thenThrow(new RuntimeException());
    response = executeGet(mvc, ExampleController.API + ExampleController.DESCRIBE, "id");
    assertEquals(HttpStatus.INTERNAL_SERVER_ERROR.value(),response.getStatus());
  }

  @Test
  public void listTest() throws Exception {
    MockHttpServletResponse response =
        executeGet(mvc, ExampleController.API + ExampleController.LIST, 0, 10);
    assertEquals(HttpStatus.OK.value(),response.getStatus());

    when(exampleService.retrieveAll(Mockito.any())).thenThrow(new RuntimeException());
    response = executeGet(mvc, ExampleController.API + ExampleController.LIST, 0, 10);
    assertEquals(HttpStatus.INTERNAL_SERVER_ERROR.value(),response.getStatus());
  }

  @Test
  public void saveTest() throws Exception {
    Example example = new Example();
    MockHttpServletResponse response =
        executePost(mvc, ExampleController.API + ExampleController.SAVE, example);
    assertEquals(HttpStatus.OK.value(),response.getStatus());

    when(exampleService.save(Mockito.any())).thenThrow(new RuntimeException());
    response = executePost(mvc, ExampleController.API + ExampleController.SAVE, example);
    assertEquals(HttpStatus.INTERNAL_SERVER_ERROR.value(),response.getStatus());
  }

  @Test
  public void deleteTest() throws Exception {
    MockHttpServletResponse response =
        executeDelete(mvc, ExampleController.API + ExampleController.DELETE, null, "id");
    assertEquals(HttpStatus.INTERNAL_SERVER_ERROR.value(),response.getStatus());

    when(exampleService.deleteById(Mockito.any())).thenThrow(new RuntimeException());
    response = executeDelete(mvc, ExampleController.API + ExampleController.DELETE, null, "id");
    assertEquals(HttpStatus.INTERNAL_SERVER_ERROR.value(),response.getStatus());
  }
  
  @Test
  public void deleteSuccessTest() throws Exception {
    when(exampleService.deleteById(Mockito.any())).thenReturn(true);
    
    MockHttpServletResponse response =
        executeDelete(mvc, ExampleController.API + ExampleController.DELETE, null, "id");
    assertEquals(HttpStatus.OK.value(),response.getStatus());
  }
}
