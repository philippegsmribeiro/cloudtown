package cloudtown.storage.api;

import static org.junit.Assert.assertEquals;
import static org.mockito.Mockito.mock;
import static org.mockito.Mockito.when;

import cloudtown.storage.UnitTest;
import cloudtown.storage.api.commons.CryptoException;
import cloudtown.storage.model.user.User;
import cloudtown.storage.model.user.UserChangePasswordRequest;
import cloudtown.storage.model.user.UserException;
import cloudtown.storage.model.user.UserProfile;
import cloudtown.storage.model.user.UserRequest;
import cloudtown.storage.model.user.UserService;
import org.junit.Before;
import org.junit.Test;
import org.mockito.Mockito;
import org.springframework.http.HttpStatus;
import org.springframework.mock.web.MockHttpServletResponse;
import org.springframework.security.core.userdetails.UsernameNotFoundException;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.setup.MockMvcBuilders;


/**
 * Users Controller Unit Test.
 */
public class UserControllerTest extends UnitTest {

  private String userId = UnitTest.getProperty("user.id");
  private String login = UnitTest.getProperty("user.login");
  private MockMvc mvc;
  private UserController userController;
  private UserService userService;

  /**
   * Setup the objects with the mocks.
   * 
   * @throws Exception if something goes wrong
   */
  @Before
  public void setUp() throws Exception {
    userService = mock(UserService.class);
    userController = new UserController(userService);
    mvc = MockMvcBuilders.standaloneSetup(userController).build();
  }

  @Test
  public void getTest() throws Exception {
    MockHttpServletResponse response = executeGet(mvc, UserController.API);
    assertEquals(HttpStatus.OK.value(), response.getStatus());
  }

  @Test
  public void getOneUserNameExceptionTest() throws Exception {
    when(userService.findById(userId))
        .thenThrow(new UsernameNotFoundException(Mockito.anyString()));
    MockHttpServletResponse response =
        executeGet(mvc, UserController.API + UserController.DESCRIBE, userId);
    assertEquals(HttpStatus.PRECONDITION_FAILED.value(), response.getStatus());
  }

  @Test
  public void getOneTest() throws Exception {
    MockHttpServletResponse response =
        executeGet(mvc, UserController.API + UserController.DESCRIBE, userId);
    assertEquals(HttpStatus.OK.value(), response.getStatus());
    when(userService.findById(userId)).thenThrow(new RuntimeException());
    response = executeGet(mvc, UserController.API + UserController.DESCRIBE, userId);
    assertEquals(HttpStatus.INTERNAL_SERVER_ERROR.value(), response.getStatus());
  }

  @Test
  public void getOneProfileTest() throws Exception {
    MockHttpServletResponse response =
        executeGet(mvc, UserController.API + UserController.DESCRIBE_PROFILE, userId);
    assertEquals(HttpStatus.OK.value(), response.getStatus());
    when(userService.findProfileByUserId(userId)).thenThrow(new RuntimeException());
    response = executeGet(mvc, UserController.API + UserController.DESCRIBE_PROFILE, userId);
    assertEquals(HttpStatus.INTERNAL_SERVER_ERROR.value(), response.getStatus());
  }

  @Test
  public void getOneProfileExceptionTest() throws Exception {
    MockHttpServletResponse response =
        executeGet(mvc, UserController.API + UserController.DESCRIBE_PROFILE, userId);
    assertEquals(HttpStatus.OK.value(), response.getStatus());
    when(userService.findProfileByUserId(userId))
        .thenThrow(new UsernameNotFoundException(Mockito.anyString()));
    response = executeGet(mvc, UserController.API + UserController.DESCRIBE_PROFILE, userId);
    assertEquals(HttpStatus.PRECONDITION_FAILED.value(), response.getStatus());
  }

  @Test
  public void getUserByLoginTest() throws Exception {
    MockHttpServletResponse response =
        executeGet(mvc, UserController.API + UserController.DESCRIBE_LOGIN, login);
    assertEquals(HttpStatus.OK.value(), response.getStatus());

    when(userService.findByLoginName(Mockito.any())).thenThrow(new RuntimeException());
    response = executeGet(mvc, UserController.API + UserController.DESCRIBE_LOGIN, login);
    assertEquals(HttpStatus.INTERNAL_SERVER_ERROR.value(), response.getStatus());
  }

  @Test
  public void getUserByLoginExceptionTest() throws Exception {
    when(userService.findByLoginName(login)).thenThrow(new UsernameNotFoundException(login));
    MockHttpServletResponse response =
        executeGet(mvc, UserController.API + UserController.DESCRIBE_LOGIN, login);
    assertEquals(HttpStatus.PRECONDITION_FAILED.value(), response.getStatus());
  }


  @Test
  public void saveExceptionTest() throws Exception {
    User user = new User();
    when(userService.save(Mockito.any())).thenThrow(new UserException(Mockito.anyString()));
    MockHttpServletResponse response =
        executePost(mvc, UserController.API + UserController.SAVE, user);
    assertEquals(HttpStatus.PRECONDITION_FAILED.value(), response.getStatus());
  }

  @Test
  public void saveTest() throws Exception {
    User user = new User();
    MockHttpServletResponse response =
        executePost(mvc, UserController.API + UserController.SAVE, user);
    assertEquals(HttpStatus.OK.value(), response.getStatus());

    when(userService.save(Mockito.any())).thenThrow(new RuntimeException());
    response = executePost(mvc, UserController.API + UserController.SAVE, user);
    assertEquals(HttpStatus.INTERNAL_SERVER_ERROR.value(), response.getStatus());
  }

  @Test
  public void saveProfileTest() throws Exception {
    UserProfile userProfile = UserProfile.builder().build();
    MockHttpServletResponse response =
        executePost(mvc, UserController.API + UserController.SAVE_PROFILE, userProfile, userId);
    assertEquals(HttpStatus.OK.value(), response.getStatus());
    when(userService.updateProfile(Mockito.any(), Mockito.any())).thenThrow(new RuntimeException());
    response =
        executePost(mvc, UserController.API + UserController.SAVE_PROFILE, userProfile, userId);
    assertEquals(HttpStatus.INTERNAL_SERVER_ERROR.value(), response.getStatus());
  }

  @Test
  public void saveProfileExceptionTest() throws Exception {
    UserProfile userProfile = UserProfile.builder().build();
    when(userService.updateProfile(userId, userProfile)).thenThrow(new UserException(userId));
    MockHttpServletResponse response =
        executePost(mvc, UserController.API + UserController.SAVE_PROFILE, userProfile, userId);
    assertEquals(HttpStatus.PRECONDITION_FAILED.value(), response.getStatus());
  }

  @Test
  public void deleteTest() throws Exception {
    MockHttpServletResponse response =
        executeDelete(mvc, UserController.API + UserController.DELETE, null, userId);
    assertEquals(HttpStatus.OK.value(), response.getStatus());

    Mockito.doThrow(new UserException(userId)).when(userService).delete(userId);
    response = executeDelete(mvc, UserController.API + UserController.DELETE, null, userId);
    assertEquals(HttpStatus.PRECONDITION_FAILED.value(), response.getStatus());

    Mockito.doThrow(new RuntimeException()).when(userService).delete(userId);
    response = executeDelete(mvc, UserController.API + UserController.DELETE, null, userId);
    assertEquals(HttpStatus.INTERNAL_SERVER_ERROR.value(), response.getStatus());
  }


  @Test
  public void searchUsersTest() throws Exception {
    MockHttpServletResponse response =
        executePost(mvc, UserController.API + UserController.SEARCH, UserRequest.builder().build());
    assertEquals(HttpStatus.OK.value(), response.getStatus());
    when(userService.findByFilter(Mockito.any())).thenThrow(new RuntimeException());
    response =
        executePost(mvc, UserController.API + UserController.SEARCH, UserRequest.builder().build());
    assertEquals(HttpStatus.INTERNAL_SERVER_ERROR.value(), response.getStatus());
  }


  @Test
  public void searchUsersExceptionTest() throws Exception {
    Mockito.doThrow(new UserException(userId)).when(userService).findByFilter(Mockito.any());
    MockHttpServletResponse response =
        executePost(mvc, UserController.API + UserController.SEARCH, UserRequest.builder().build());
    assertEquals(HttpStatus.PRECONDITION_FAILED.value(), response.getStatus());
  }


  @Test
  public void updatePasswordTest() throws Exception {
    when(userService.findByLoginName(Mockito.any())).thenReturn(userEncrypt());
    when(userService.save(Mockito.any())).thenReturn(createUser());
    MockHttpServletResponse response = executePut(mvc,
        UserController.API + UserController.UPDATE_PASSWORD,
        UserChangePasswordRequest.builder().newPassword(UnitTest.getProperty("user.newpassword"))
            .oldPassword(UnitTest.getProperty("user.password")).build(),
        UnitTest.getProperty("user.login"));
    assertEquals(HttpStatus.OK.value(), response.getStatus());
  }

  @Test
  public void updatePasswordExceptionTest() throws Exception {
    UserChangePasswordRequest request =
        UserChangePasswordRequest.builder().newPassword(UnitTest.getProperty("user.newpassword"))
            .oldPassword(UnitTest.getProperty("user.password")).build();

    Mockito.doThrow(new UserException(userId)).when(userService).updatePassword(request);
    MockHttpServletResponse response =
        executePut(mvc, UserController.API + UserController.UPDATE_PASSWORD, request,
            UnitTest.getProperty("user.login"));
    assertEquals(HttpStatus.PRECONDITION_FAILED.value(), response.getStatus());

    Mockito.doThrow(new RuntimeException()).when(userService).updatePassword(request);
    response = executePut(mvc, UserController.API + UserController.UPDATE_PASSWORD, request,
        UnitTest.getProperty("user.login"));
    assertEquals(HttpStatus.INTERNAL_SERVER_ERROR.value(), response.getStatus());
  }

  /**
   * Mock encrypted user data.
   * 
   * @return User
   * @throws CryptoException  if Encryption or decryption goes wrong.
   */
  private User userEncrypt() throws CryptoException {
    User userEncrypt = User.builder().id(UnitTest.getProperty("user.id"))
        .login(UnitTest.getProperty("user.login.encrypt"))
        .email(UnitTest.getProperty("user.email.encrypt"))
        .password(UnitTest.getProperty("user.password.encrypt")).build();
    return userEncrypt;
  }

  /**
   * Mock user data.
   * 
   * @return User
   */
  private User createUser() {
    User user = User.builder().id(UnitTest.getProperty("user.id"))
        .login(UnitTest.getProperty("user.login")).email(UnitTest.getProperty("user.email"))
        .password(UnitTest.getProperty("user.password")).build();
    return user;
  }
}
