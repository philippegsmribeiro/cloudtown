package cloudtown.storage.api;

import static org.junit.Assert.assertEquals;
import static org.mockito.Mockito.mock;
import static org.mockito.Mockito.when;

import cloudtown.storage.UnitTest;
import cloudtown.storage.model.account.Account;
import cloudtown.storage.model.account.AccountException;
import cloudtown.storage.model.account.AccountRequest;
import cloudtown.storage.model.account.AccountService;
import org.junit.Before;
import org.junit.Test;
import org.mockito.Mockito;
import org.springframework.http.HttpStatus;
import org.springframework.mock.web.MockHttpServletResponse;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.setup.MockMvcBuilders;

/**
 * Account Controller Unit Test.
 */
public class AccountControllerTest extends UnitTest {

  private String accountId = UnitTest.getProperty("account.accountid");
  private MockMvc mvc;
  private AccountController accountController;
  private AccountService accountService;

  /**
   * Setup the objects with the mocks.
   * 
   * @throws Exception if something goes wrong
   */
  @Before
  public void setUp() throws Exception {
    accountService = mock(AccountService.class);
    accountController = new AccountController(accountService);
    mvc = MockMvcBuilders.standaloneSetup(accountController).build();
  }

  @Test
  public void getTest() throws Exception {
    MockHttpServletResponse response = executeGet(mvc, AccountController.API);
    assertEquals(HttpStatus.OK.value(), response.getStatus());
  }

  @Test
  public void getOneTest() throws Exception {
    MockHttpServletResponse response =
        executeGet(mvc, AccountController.API + AccountController.DESCRIBE, accountId);
    assertEquals(HttpStatus.OK.value(), response.getStatus());
    when(accountService.retrieveById(accountId, true)).thenThrow(new RuntimeException());
    response = executeGet(mvc, AccountController.API + AccountController.DESCRIBE, accountId);
    assertEquals(HttpStatus.INTERNAL_SERVER_ERROR.value(), response.getStatus());
  }

  @Test
  public void getOneException() throws Exception {
    when(accountService.retrieveById(accountId, true)).thenThrow(new AccountException("Error"));
    MockHttpServletResponse response =
        executeGet(mvc, AccountController.API + AccountController.DESCRIBE, accountId);
    assertEquals(HttpStatus.PRECONDITION_FAILED.value(), response.getStatus());
  }


  @Test
  public void listTest() throws Exception {
    MockHttpServletResponse response =
        executeGet(mvc, AccountController.API + AccountController.LIST, 0, 10);
    assertEquals(HttpStatus.OK.value(), response.getStatus());

    when(accountService.retrieveAll(Mockito.any())).thenThrow(new RuntimeException());
    response = executeGet(mvc, AccountController.API + AccountController.LIST, 0, 10);
    assertEquals(HttpStatus.INTERNAL_SERVER_ERROR.value(), response.getStatus());
  }

  @Test
  public void listExceptionTest() throws Exception {
    when(accountService.retrieveAll(Mockito.any())).thenThrow(new AccountException("Error"));
    MockHttpServletResponse response =
        executeGet(mvc, AccountController.API + AccountController.LIST, 0, 10);
    assertEquals(HttpStatus.PRECONDITION_FAILED.value(), response.getStatus());
  }

  @Test
  public void saveTest() throws Exception {
    Account account = new Account();
    MockHttpServletResponse response =
        executePost(mvc, AccountController.API + AccountController.SAVE, account);
    assertEquals(HttpStatus.OK.value(), response.getStatus());

    when(accountService.save(Mockito.any())).thenThrow(new RuntimeException());
    response = executePost(mvc, AccountController.API + AccountController.SAVE, account);
    assertEquals(HttpStatus.INTERNAL_SERVER_ERROR.value(), response.getStatus());
  }

  @Test
  public void saveExceptionTest() throws Exception {
    Account account = new Account();

    when(accountService.save(Mockito.any())).thenThrow(new AccountException("Error"));
    MockHttpServletResponse response =
        executePost(mvc, AccountController.API + AccountController.SAVE, account);
    assertEquals(HttpStatus.PRECONDITION_FAILED.value(), response.getStatus());
  }


  @Test
  public void deleteTest() throws Exception {
    MockHttpServletResponse response =
        executeDelete(mvc, AccountController.API + AccountController.DELETE, null, accountId);
    assertEquals(HttpStatus.INTERNAL_SERVER_ERROR.value(), response.getStatus());

    when(accountService.deleteById(Mockito.any())).thenThrow(new RuntimeException());
    response =
        executeDelete(mvc, AccountController.API + AccountController.DELETE, null, accountId);
    assertEquals(HttpStatus.INTERNAL_SERVER_ERROR.value(), response.getStatus());
  }

  @Test
  public void deleteExceptionTest() throws Exception {
    when(accountService.deleteById(Mockito.any())).thenThrow(new AccountException("Error"));
    MockHttpServletResponse response =
        executeDelete(mvc, AccountController.API + AccountController.DELETE, null, accountId);
    assertEquals(HttpStatus.PRECONDITION_FAILED.value(), response.getStatus());
  }

  @Test
  public void deleteSuccessTest() throws Exception {
    when(accountService.deleteById(Mockito.any())).thenReturn(true);
    MockHttpServletResponse response =
        executeDelete(mvc, AccountController.API + AccountController.DELETE, null, accountId);
    assertEquals(HttpStatus.OK.value(), response.getStatus());
  }

  @Test
  public void searchAccountTest() throws Exception {
    MockHttpServletResponse response = executePost(mvc,
        AccountController.API + AccountController.SEARCH, AccountRequest.builder().build());
    assertEquals(HttpStatus.OK.value(), response.getStatus());
    when(accountService.findByFilter(Mockito.any())).thenThrow(new RuntimeException());
    response = executePost(mvc, AccountController.API + AccountController.SEARCH,
        AccountRequest.builder().build());
    assertEquals(HttpStatus.INTERNAL_SERVER_ERROR.value(), response.getStatus());
  }

  @Test
  public void searchAccountExceptionTest() throws Exception {
    when(accountService.findByFilter(Mockito.any())).thenThrow(new AccountException("Error"));
    MockHttpServletResponse response = executePost(mvc,
        AccountController.API + AccountController.SEARCH, AccountRequest.builder().build());
    assertEquals(HttpStatus.PRECONDITION_FAILED.value(), response.getStatus());
  }
}
