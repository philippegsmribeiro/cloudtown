package cloudtown.storage;

import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.delete;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.get;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.post;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.put;

import cloudtown.storage.cloud.Provider;
import cloudtown.storage.model.account.Account;
import cloudtown.storage.model.account.AwsCredential;
import cloudtown.storage.model.account.Credential;
import com.fasterxml.jackson.databind.ObjectMapper;
import java.io.IOException;
import java.nio.charset.Charset;
import java.util.Arrays;
import java.util.Properties;
import org.junit.runner.RunWith;
import org.junit.runners.BlockJUnit4ClassRunner;
import org.springframework.http.MediaType;
import org.springframework.mock.web.MockHttpServletResponse;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.ResultActions;
import org.springframework.test.web.servlet.request.MockHttpServletRequestBuilder;
import org.springframework.web.bind.annotation.RequestMethod;

/**
 * Unit test.<br/>
 * ATTENTION: Should avoid to use a spring context on those tests.<br/>
 * Removed the spring annotations and added a custom reader for a property file.
 * Please use: UnitTest.getProperty(propertyName) to read a property from the file.
 */
@RunWith(BlockJUnit4ClassRunner.class)
public abstract class UnitTest {

  private static ObjectMapper mapper = new ObjectMapper();

  private static MediaType contentType = new MediaType(MediaType.APPLICATION_JSON.getType(),
      MediaType.APPLICATION_JSON.getSubtype(), Charset.forName("utf8"));

  private static Properties properties = new Properties();

  static {
    try {
      // loads the test properties
      properties.load(ClassLoader.getSystemResourceAsStream("mock-data.properties"));
    } catch (IOException e) {
      e.printStackTrace();
    }
  }

  /**
   * Read a property from the tests.properties
   * 
   * @param propertyName name
   * @return property value
   */
  protected static String getProperty(String propertyName) {
    return properties.getProperty(propertyName);
  }

  /**
   * Execute a GET request to a controller.
   * 
   * @param mvc mockedMvc
   * @param path url
   * @param params pathParams
   * @return MockHttpServletResponse
   * @throws Exception if something goes wrong
   */
  protected static MockHttpServletResponse executeGet(MockMvc mvc, String path, Object... params)
      throws Exception {
    return execute(RequestMethod.GET, mvc, path, null, params);
  }

  /**
   * Execute a POST request to a controller.
   * 
   * @param mvc mockedMvc
   * @param path url
   * @param json bodyObject
   * @param params pathParams
   * @return MockHttpServletResponse
   * @throws Exception if something goes wrong
   */
  protected static MockHttpServletResponse executePost(MockMvc mvc, String path, Object json,
      Object... params) throws Exception {
    return execute(RequestMethod.POST, mvc, path, json, params);
  }

  /**
   * Execute a PUT request to a controller.
   * 
   * @param mvc mockedMvc
   * @param path url
   * @param json bodyObject
   * @param params pathParams
   * @return MockHttpServletResponse
   * @throws Exception if something goes wrong
   */
  protected static MockHttpServletResponse executePut(MockMvc mvc, String path, Object json,
      Object... params) throws Exception {
    return execute(RequestMethod.PUT, mvc, path, json, params);
  }

  /**
   * Execute a DELETE request to a controller.
   * 
   * @param mvc mockedMvc
   * @param path url
   * @param json bodyObject
   * @param params pathParams
   * @return MockHttpServletResponse
   * @throws Exception if something goes wrong
   */
  protected static MockHttpServletResponse executeDelete(MockMvc mvc, String path, Object json,
      Object... params) throws Exception {
    return execute(RequestMethod.DELETE, mvc, path, json, params);
  }

  /**
   * Perform the execution.
   * 
   * @param method type
   * @param mvc mockedMvc
   * @param path url
   * @param json bodyObject
   * @param params pathParams
   * @return MockHttpServletResponse
   * @throws Exception if something goes wrong
   */
  protected static MockHttpServletResponse execute(RequestMethod method, MockMvc mvc, String path,
      Object json, Object... params) throws Exception {
    MockHttpServletRequestBuilder action = null;
    switch (method) {
      case GET:
        action = get(path, params);
        break;
      case POST:
        action = post(path, params);
        break;
      case DELETE:
        action = delete(path, params);
        break;
      case PUT:
        action = put(path, params);
        break;
      default:
        action = get(path, params);
        break;
    }
    ResultActions perform = null;
    if (json == null) {
      perform = mvc.perform(action);
    } else {
      byte[] writeValueAsBytes = mapper.writeValueAsBytes(json);
      perform = mvc.perform(action.content(writeValueAsBytes).contentType(contentType));
    }
    return perform.andReturn().getResponse();
  }

  protected static class TestUtils {

    /**
     * Valid Aws Credential.
     * 
     * @return
     */
    public static AwsCredential getValidAwsCredential() {
      return AwsCredential.builder()
                          .accountId(UnitTest.getProperty("aws.account.id"))
                          .accessKey(UnitTest.getProperty("aws.account.accesskey"))
                          .secretKey(UnitTest.getProperty("aws.account.secretkey"))
                          .build();
    }


    /**
     * Create an object Account for AWS.
     * 
     * @param accountId id
     * @param awsCredential credentials data
     * @return
     */
    public static Account createAwsAccount(String accountId, AwsCredential awsCredential) {
      Credential credential = Credential.builder()
                                        .awsCredential(awsCredential)
                                        .build();
      return Account.builder()
                    .provider(Provider.AMAZON_AWS)
                    .id(accountId)
                    .credentials(Arrays.asList(credential))
                    .build();
    }

  }
}
