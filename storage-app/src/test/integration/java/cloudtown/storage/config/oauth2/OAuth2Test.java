package cloudtown.storage.config.oauth2;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertTrue;

import cloudtown.storage.IntegrationTest;
import cloudtown.storage.test.TestRestTemplateWithAuth;
import java.util.Map;
import org.junit.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.security.oauth2.provider.OAuth2Authentication;
import org.springframework.security.oauth2.provider.token.store.JwtTokenStore;

public class OAuth2Test extends IntegrationTest {

  @Autowired
  private JwtTokenStore tokenStore;

  @Value("${auth.app.client.id}")
  private String appClientId;
  @Value("${auth.app.client.secret}")
  private String appClientSecret;

  @Value("${admin.user.loginname}")
  private String adminUserLoginName;
  @Value("${admin.user.password}")
  private String adminUserPassword;

  @SuppressWarnings("rawtypes")
  @Test
  public void getAuthenticatedTokenTest() {
    ResponseEntity<Map> response = TestRestTemplateWithAuth.getAccessToken(adminUserLoginName,
        adminUserPassword, "write", appClientId, appClientSecret, template);
    assertEquals(HttpStatus.OK, response.getStatusCode());
    final String tokenValue = (String) response.getBody().get("access_token");
    final OAuth2Authentication auth = tokenStore.readAuthentication(tokenValue);
    assertTrue(auth.isAuthenticated());
  }

  @SuppressWarnings("rawtypes")
  @Test
  public void getNotAuthenticatedTokenTest() {
    // wrong user credentials
    ResponseEntity<Map> response = TestRestTemplateWithAuth.getAccessToken("blah", "blah", "write",
        appClientId, appClientSecret, template);
    assertEquals(HttpStatus.BAD_REQUEST, response.getStatusCode());

    // wrong clientId
    response = TestRestTemplateWithAuth.getAccessToken(adminUserLoginName, adminUserPassword,
        "write", "appClientId", appClientSecret, template);
    assertEquals(HttpStatus.UNAUTHORIZED, response.getStatusCode());

    // wrong clientSecret
    response = TestRestTemplateWithAuth.getAccessToken(adminUserLoginName, adminUserPassword,
        "write", appClientId, "appClientSecret", template);
    assertEquals(HttpStatus.UNAUTHORIZED, response.getStatusCode());
  }
}
