package cloudtown.storage;

import cloudtown.storage.cloud.Provider;
import cloudtown.storage.model.account.Account;
import cloudtown.storage.model.account.AwsCredential;
import cloudtown.storage.model.account.Credential;
import cloudtown.storage.test.TestRestTemplateWithAuth;
import com.fasterxml.jackson.databind.ObjectMapper;
import java.util.Arrays;
import lombok.extern.log4j.Log4j2;
import org.junit.After;
import org.junit.Before;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.boot.web.client.RootUriTemplateHandler;
import org.springframework.boot.web.server.LocalServerPort;
import org.springframework.http.converter.json.MappingJackson2HttpMessageConverter;
import org.springframework.test.context.junit4.SpringRunner;

@RunWith(SpringRunner.class)
@Log4j2
@SpringBootTest(webEnvironment = SpringBootTest.WebEnvironment.RANDOM_PORT,
    properties = "spring.profiles.include=test")
public abstract class IntegrationTest {

  @LocalServerPort
  protected int port;

  private static final String URL_BASE = "http://localhost:%s/";

  @Autowired
  protected TestRestTemplateWithAuth template;

  @Autowired
  protected ObjectMapper mapper;

  @Autowired
  protected MappingJackson2HttpMessageConverter httpMessageConverter;

  /**
   * Setup the test rest template.<br/>
   * Set the root url.
   */
  @Before
  public void setUpForIntegration() {
    String url = String.format(URL_BASE, port);
    log.info("Base url for test: {}", url);
    template.setUriTemplateHandler(new RootUriTemplateHandler(url));
  }
  
  /**
   * Tear down the test rest template.<br/>
   * Logout the user, case it has been logged.
   */
  @After
  public void tearDownIntegration() {
    template.logout();
  }
  
  @Value("${admin.user.loginname}")
  private String adminUserLoginName;
  @Value("${admin.user.password}")
  private String adminUserPassword;
  
  /**
   * Aux method to login as admin user.
   */
  protected void loginAdminUser() {
    template.login(adminUserLoginName, adminUserPassword, "write");
  }

  protected static class TestUtils {

    /**
     * Valid Aws Credential.
     * 
     * @return
     */
    public static AwsCredential getValidAwsCredential() {
      return AwsCredential.builder().accessKey("AKIAJX4TSC4JHW5TBR7A")
          .secretKey("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").build();
    }


    /**
     * Create an object Account for AWS.
     * 
     * @param accountId id
     * @param awsCredential credentials data
     * @return
     */
    public static Account createAwsAccount(String accountId, AwsCredential awsCredential) {
      Credential credential = Credential.builder().awsCredential(awsCredential).build();
      return Account.builder().provider(Provider.AMAZON_AWS).id(accountId)
          .credentials(Arrays.asList(credential)).build();
    }
  }
}
