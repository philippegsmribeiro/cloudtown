package cloudtown.storage.test;

import static org.junit.Assert.fail;
import static org.springframework.http.HttpHeaders.AUTHORIZATION;

import java.net.URI;
import java.util.Collections;
import java.util.HashMap;
import java.util.Map;
import javax.annotation.PostConstruct;
import lombok.extern.log4j.Log4j2;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.boot.test.web.client.TestRestTemplate;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.security.oauth2.client.token.DefaultAccessTokenRequest;
import org.springframework.stereotype.Service;
import org.springframework.util.StringUtils;
import org.springframework.web.util.UriComponentsBuilder;

/**
 * Custom testRestTemplate with easy oath2 authentication for test.
 */
@Service
@Log4j2
public class TestRestTemplateWithAuth extends TestRestTemplate {

  private static final String OAUTH_TOKEN_URL = "/oauth/token";

  private static final String PASSWORD = "password";
  private static final String USERNAME = "username";
  private static final String SCOPE = "scope";
  private static final String GRANT_TYPE = "grant_type";
  private static final String GRANT_TYPE_VALUE = "password";

  private static final String BEARER_TOKEN = "Bearer %s";
  public static final String ACCESS_TOKEN = "access_token";

  /** App client id. */
  @Value("${auth.app.client.id}")
  private String appClientId;
  /** App client secret. */
  @Value("${auth.app.client.secret}")
  private String appClientSecret;

  /** auth token for the requests. */
  private Map<String, String> tokens = new HashMap<>();
  /** keep the last key. */
  private String lastTokenKey;

  /**
   * Set up the interceptor to add the token on the requests.
   */
  @PostConstruct
  protected void setInterceptor() {
    //
    getRestTemplate().setInterceptors(Collections.singletonList((request, body, execution) -> {
      if (lastTokenKey != null && tokens.get(lastTokenKey) != null) {
        String token = tokens.get(lastTokenKey);
        log.info("Request using token: {}", token);
        setHeaderToken(request.getHeaders(), token);
      }
      return execution.execute(request, body);
    }));
  }

  /**
   * Execute login authentication.
   * 
   * @param username user name
   * @param password user password
   * @param scope auth scope
   */
  @SuppressWarnings("rawtypes")
  public void login(String username, String password, String scope) {
    String tokenKey = String.format("%s@%s@%s", username, password, scope);
    String token = this.tokens.get(tokenKey);
    // if token already exists, reuse it
    if (StringUtils.isEmpty(token)) {
      ResponseEntity<Map> accessToken =
          getAccessToken(username, password, scope, appClientId, appClientSecret, this);
      if (accessToken.getStatusCode() != HttpStatus.OK) {
        fail("Authentication failed! Details: " + accessToken.toString());
      }
      token = (String) accessToken.getBody().get(ACCESS_TOKEN);
      this.tokens.put(tokenKey, token);
    }
    this.lastTokenKey = tokenKey;
  }

  /**
   * Execute logout.
   */
  public void logout() {
    this.lastTokenKey = null;
  }

  /**
   * Get an auth token with the credentials sent.
   * 
   * @param username user login
   * @param password user password
   * @param scope auth scope
   * @param clientId client id
   * @param clientSecret client secret
   * @param template rest template to use
   * @return ResponseEntity map
   */
  @SuppressWarnings("rawtypes")
  public static ResponseEntity<Map> getAccessToken(String username, String password, String scope,
      String clientId, String clientSecret, TestRestTemplate template) {
    // set the params
    DefaultAccessTokenRequest accessTokenRequest = new DefaultAccessTokenRequest();
    accessTokenRequest.add(GRANT_TYPE, GRANT_TYPE_VALUE);
    accessTokenRequest.add(SCOPE, scope);
    accessTokenRequest.add(USERNAME, username);
    accessTokenRequest.add(PASSWORD, password);
    // set the url
    String url = OAUTH_TOKEN_URL;
    // custom set the params on the url
    TestRestTemplate templateWithBasicAuth = template.withBasicAuth(clientId, clientSecret);
    URI uri = UriComponentsBuilder.fromHttpUrl(template.getRootUri()).path(url)
        .queryParams(accessTokenRequest).build().toUri();
    // get the result
    ResponseEntity<Map> tokenMap = templateWithBasicAuth.postForEntity(uri, null, Map.class);
    // get the token
    return tokenMap;
  }
  
  public static void setHeaderToken(HttpHeaders headers, String token) {
    headers.add(AUTHORIZATION, String.format(BEARER_TOKEN, token));
  }

}
