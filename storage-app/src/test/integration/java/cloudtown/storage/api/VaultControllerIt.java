package cloudtown.storage.api;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertNotNull;

import cloudtown.storage.IntegrationTest;
import cloudtown.storage.cloud.Provider;
import cloudtown.storage.model.account.Account;
import cloudtown.storage.model.vault.VaultListRequest;
import com.amazonaws.regions.Regions;
import java.util.List;
import org.apache.commons.lang3.StringUtils;
import org.junit.FixMethodOrder;
import org.junit.Test;
import org.junit.runners.MethodSorters;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;

/**
 * Integration Test Test the full function.
 */
@FixMethodOrder(MethodSorters.NAME_ASCENDING)
public class VaultControllerIt extends IntegrationTest {
  private static String id = StringUtils.EMPTY;
  private static final String ACCOUNT_ID = "287334376268";

  @Test
  public void t1CreateAccountTest() throws Exception {
    super.loginAdminUser();
    Account account = TestUtils.createAwsAccount(ACCOUNT_ID, TestUtils.getValidAwsCredential());
    ResponseEntity<Account> response = template
        .postForEntity(AccountController.API + AccountController.SAVE, account, Account.class);
    assertEquals(response.getStatusCode(), HttpStatus.OK);
    assertNotNull(response.getBody().getId());
    id = response.getBody().getId();
  }

  @SuppressWarnings("rawtypes")
  @Test
  public void t2ListVaultsByAccount() throws Exception {
    super.loginAdminUser();
    VaultListRequest vaultListRequest = VaultListRequest.builder().accountId(ACCOUNT_ID)
        .provider(Provider.AMAZON_AWS).region(Regions.US_WEST_1.getName()).build();
    ResponseEntity<List> response = template
        .postForEntity(VaultController.API + VaultController.LIST, vaultListRequest, List.class);
    assertEquals(response.getStatusCode(), HttpStatus.OK);
  }

  @Test
  public void t3DeleteAccountTest() throws Exception {
    super.loginAdminUser();
    template.delete(AccountController.API + AccountController.DELETE, id);
  }


}
