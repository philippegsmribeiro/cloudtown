package cloudtown.storage.api;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertNotNull;
import static org.junit.Assert.assertNull;
import static org.junit.Assert.assertTrue;

import cloudtown.storage.IntegrationTest;
import cloudtown.storage.cloud.Provider;
import cloudtown.storage.model.account.Account;
import cloudtown.storage.model.account.AccountRequest;
import cloudtown.storage.model.common.PageResponse;
import java.util.List;
import org.junit.FixMethodOrder;
import org.junit.Test;
import org.junit.runners.MethodSorters;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;

/**
 * Integration Test Test the full function.
 */
@FixMethodOrder(MethodSorters.NAME_ASCENDING)
public class AccountControllerIt extends IntegrationTest {

  @Test
  public void t0GetTest() throws Exception {
    super.loginAdminUser();
    ResponseEntity<String> response = template.getForEntity(AccountController.API, String.class);
    assertEquals(response.getStatusCode(), HttpStatus.OK);
    assertEquals(AccountController.ENDPOINT_DESCRIPTION, response.getBody());
  }

  private static String id = "1";

  @Test
  public void t1SaveTest() throws Exception {
    super.loginAdminUser();
    Account account = Account.builder().id(id).provider(Provider.AMAZON_AWS).build();
    ResponseEntity<Account> response = template
        .postForEntity(AccountController.API + AccountController.SAVE, account, Account.class);
    assertEquals(response.getStatusCode(), HttpStatus.OK);
    assertNotNull(response.getBody().getId());
    id = response.getBody().getId();
  }

  @Test
  public void t2GetOneTest() throws Exception {
    super.loginAdminUser();
    ResponseEntity<Account> response = template
        .getForEntity(AccountController.API + AccountController.DESCRIBE, Account.class, id);
    assertEquals(response.getStatusCode(), HttpStatus.OK);
    assertNotNull(response.getBody());
    assertEquals(id, response.getBody().getId());
  }

  @SuppressWarnings({"rawtypes", "unchecked"})
  @Test
  public void t3ListTest() throws Exception {
    super.loginAdminUser();
    Integer pageNumber = 0;
    Integer pageSize = 10;
    ResponseEntity<PageResponse> response = template.getForEntity(
        AccountController.API + AccountController.LIST, PageResponse.class, pageNumber, pageSize);
    assertEquals(response.getStatusCode(), HttpStatus.OK);
    assertNotNull(response.getBody());
    PageResponse<Account> page = response.getBody();
    assertEquals(pageNumber, page.getPage());
    assertTrue(page.getPageSize() <= pageSize);
  }

  @SuppressWarnings("rawtypes")
  @Test
  public void t4SearchTest() throws Exception {
    super.loginAdminUser();
    ResponseEntity<List> response =
        template.postForEntity(AccountController.API + AccountController.SEARCH,
            AccountRequest.builder().build(), List.class);
    assertEquals(response.getStatusCode(), HttpStatus.OK);
    assertNotNull(response.getBody());
  }

  @Test
  public void t5DeleteTest() throws Exception {
    super.loginAdminUser();
    template.delete(AccountController.API + AccountController.DELETE, id);
    ResponseEntity<Account> response = template
        .getForEntity(AccountController.API + AccountController.DESCRIBE, Account.class, id);
    assertEquals(response.getStatusCode(), HttpStatus.OK);
    assertNull(response.getBody());
  }
}
