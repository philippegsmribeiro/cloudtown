package cloudtown.storage.api;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertNotNull;

import cloudtown.storage.IntegrationTest;
import cloudtown.storage.model.user.User;
import cloudtown.storage.model.user.UserChangePasswordRequest;
import cloudtown.storage.model.user.UserProfile;
import cloudtown.storage.model.user.UserRequest;
import java.util.List;
import org.junit.FixMethodOrder;
import org.junit.Test;
import org.junit.runners.MethodSorters;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;

/**
 * Integration UserS Test the full function.
 */
@FixMethodOrder(MethodSorters.NAME_ASCENDING)
public class UserControllerIt extends IntegrationTest {

  private static String id = "1";

  @Test
  public void t1SaveTest() throws Exception {
    super.loginAdminUser();
    ResponseEntity<User> response =
        template.postForEntity(UserController.API + UserController.SAVE, createUser(), User.class);
    assertEquals(response.getStatusCode(), HttpStatus.OK);
    assertNotNull(response.getBody().getId());
    id = response.getBody().getId();
  }

  @Test
  public void t2GetOneTest() throws Exception {
    super.loginAdminUser();
    ResponseEntity<User> response =
        template.getForEntity(UserController.API + UserController.DESCRIBE, User.class, id);
    assertEquals(response.getStatusCode(), HttpStatus.OK);
    assertNotNull(response.getBody());
    assertEquals(id, response.getBody().getId());
  }

  @Test
  public void t3UserByLogin() throws Exception {
    super.loginAdminUser();
    ResponseEntity<User> response = template
        .getForEntity(UserController.API + UserController.DESCRIBE_LOGIN, User.class, "admin-test");
    assertEquals(response.getStatusCode(), HttpStatus.OK);
    assertNotNull(response.getBody());
    assertEquals(id, response.getBody().getId());
  }

  @Test
  public void t4ChangePasswordTest() throws Exception {
    super.loginAdminUser();
    UserChangePasswordRequest request = UserChangePasswordRequest.builder().login("admin-test")
        .oldPassword("password-test").newPassword("new-password").build();
    template.put(UserController.API + UserController.UPDATE_PASSWORD, request,"admin-test");

  }


  @Test
  @SuppressWarnings("rawtypes")
  public void t6getUserByFilter() throws Exception {
    super.loginAdminUser();
    UserRequest request = UserRequest.builder().firstName("Roberto").build();
    ResponseEntity<List> response =
        template.postForEntity(UserController.API + UserController.SEARCH, request, List.class);
    assertEquals(response.getStatusCode(), HttpStatus.OK);
    assertNotNull(response.getBody());
  }

  @Test
  public void t7DeleteTest() throws Exception {
    super.loginAdminUser();
    template.delete(UserController.API + UserController.DELETE, id);
  }


  /**
   * Mock user data.
   * 
   * @return User
   */
  private User createUser() {
    User user = User.builder().login("admin-test").email("admin-test@cloudtown.com")
        .password("password-test").userProfile(createUserProfile()).build();
    return user;
  }

  /**
   * Mock user profile data.
   * 
   * @return UserProfile
   */
  private UserProfile createUserProfile() {
    UserProfile userProfile =
        UserProfile.builder().country("USA").city("NEW YORK").firstName("Roberto").build();
    return userProfile;
  }
}
