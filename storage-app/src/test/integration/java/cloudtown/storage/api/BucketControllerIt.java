package cloudtown.storage.api;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertNotNull;

import cloudtown.storage.IntegrationTest;
import cloudtown.storage.cloud.Provider;
import cloudtown.storage.model.account.Account;
import cloudtown.storage.model.bucket.BucketDetail;
import cloudtown.storage.model.bucket.BucketDetailRequest;
import cloudtown.storage.model.bucket.BucketListRequest;
import com.amazonaws.regions.Regions;
import com.amazonaws.services.s3.model.AccessControlList;
import java.util.List;
import org.apache.commons.lang3.StringUtils;
import org.junit.FixMethodOrder;
import org.junit.Test;
import org.junit.runners.MethodSorters;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;

/**
 * Integration Test Test the full function.
 */
@FixMethodOrder(MethodSorters.NAME_ASCENDING)
public class BucketControllerIt extends IntegrationTest {
  private static String id = StringUtils.EMPTY;
  private static final String ACCOUNT_ID = "287334376268";
  private static final String BUCKETNAME = "cloudtown.repository";

  @Test
  public void t1CreateAccountTest() throws Exception {
    super.loginAdminUser();
    Account account = TestUtils.createAwsAccount(ACCOUNT_ID, TestUtils.getValidAwsCredential());
    ResponseEntity<Account> response = template
        .postForEntity(AccountController.API + AccountController.SAVE, account, Account.class);
    assertEquals(response.getStatusCode(), HttpStatus.OK);
    assertNotNull(response.getBody().getId());
    id = response.getBody().getId();
  }

  @SuppressWarnings("rawtypes")
  @Test
  public void t2ListBucketsByAccount() throws Exception {
    super.loginAdminUser();
    BucketListRequest bucketListRequest =
        BucketListRequest.builder().accountId(ACCOUNT_ID).provider(Provider.AMAZON_AWS)
            .region(Regions.US_EAST_2.getName()).fullDetail(false).build();
    ResponseEntity<List> response = template
        .postForEntity(BucketController.API + BucketController.LIST, bucketListRequest, List.class);
    assertEquals(response.getStatusCode(), HttpStatus.OK);
  }

  @Test
  public void t3GetBucketDetail() throws Exception {
    super.loginAdminUser();
    BucketDetailRequest bucketDetailRequest = BucketDetailRequest.builder().name(BUCKETNAME)
        .accountId(ACCOUNT_ID).region(Regions.US_EAST_2.getName()).build();
    ResponseEntity<BucketDetail> response = template.postForEntity(
        BucketController.API + BucketController.DETAIL, bucketDetailRequest, BucketDetail.class);
    assertEquals(response.getStatusCode(), HttpStatus.OK);
  }

  @Test
  public void t4GetBucketRegion() throws Exception {
    super.loginAdminUser();
    BucketDetailRequest bucketDetailRequest = BucketDetailRequest.builder().name(BUCKETNAME)
        .accountId(ACCOUNT_ID).region(Regions.US_EAST_2.getName()).build();

    ResponseEntity<String> response = template.postForEntity(
        BucketController.API + BucketController.REGION, bucketDetailRequest, String.class);
    assertEquals(response.getStatusCode(), HttpStatus.OK);
  }

  @Test
  public void t5BucketPermissionDetail() throws Exception {
    super.loginAdminUser();
    BucketDetailRequest bucketDetailRequest = BucketDetailRequest.builder().name(BUCKETNAME)
        .accountId(ACCOUNT_ID).region(Regions.US_WEST_2.getName()).build();
    ResponseEntity<AccessControlList> response =
        template.postForEntity(BucketController.API + BucketController.PERMISSION,
            bucketDetailRequest, AccessControlList.class);
    assertEquals(response.getStatusCode(), HttpStatus.OK);
  }

  @Test
  public void t6DeleteAccountTest() throws Exception {
    super.loginAdminUser();
    template.delete(AccountController.API + AccountController.DELETE, id);
  }


}
