package cloudtown.storage.api;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertNotNull;
import static org.junit.Assert.assertNull;
import static org.junit.Assert.assertTrue;

import cloudtown.storage.IntegrationTest;
import cloudtown.storage.model.common.PageResponse;
import cloudtown.storage.model.example.Example;
import org.junit.FixMethodOrder;
import org.junit.Test;
import org.junit.runners.MethodSorters;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;

/**
 * Integration Test Test the full function.
 */
@FixMethodOrder(MethodSorters.NAME_ASCENDING)
public class ExampleControllerIt extends IntegrationTest {

  @Test
  public void t0GetTest() throws Exception {
    super.loginAdminUser();
    ResponseEntity<String> response = template.getForEntity(ExampleController.API, String.class);
    assertEquals(HttpStatus.OK,response.getStatusCode());
    assertEquals(ExampleController.ENDPOINT_DESCRIPTION, response.getBody());
  }

  private static String id = "1";

  @Test
  public void t1SaveTest() throws Exception {
    super.loginAdminUser();
    Example example = Example.builder().string("test").build();
    ResponseEntity<Example> response = template
        .postForEntity(ExampleController.API + ExampleController.SAVE, example, Example.class);
    assertEquals(HttpStatus.OK,response.getStatusCode());
    assertNotNull(response.getBody().getId());
    id = response.getBody().getId();
  }

  @Test
  public void t2GetOneTest() throws Exception {
    super.loginAdminUser();
    ResponseEntity<Example> response = template
        .getForEntity(ExampleController.API + ExampleController.DESCRIBE, Example.class, id);
    assertEquals(HttpStatus.OK,response.getStatusCode());
    assertNotNull(response.getBody());
    assertEquals(id, response.getBody().getId());
  }

  @Test
  @SuppressWarnings({"rawtypes", "unchecked"})
  public void t3ListTest() throws Exception {
    super.loginAdminUser();
    Integer pageNumber = 0;
    Integer pageSize = 10;
    ResponseEntity<PageResponse> response = template
        .getForEntity(ExampleController.API + ExampleController.LIST, PageResponse.class,
            pageNumber, pageSize);
    assertEquals(HttpStatus.OK,response.getStatusCode());
    assertNotNull(response.getBody());
    PageResponse<Example> page = response.getBody();
    assertEquals(pageNumber, page.getPage());
    assertTrue(page.getPageSize() <= pageSize);
  }

  @Test
  public void t4DeleteTest() throws Exception {
    super.loginAdminUser();
    template.delete(ExampleController.API + ExampleController.DELETE, id);
    ResponseEntity<Example> response = template
        .getForEntity(ExampleController.API + ExampleController.DESCRIBE, Example.class, id);
    assertEquals(HttpStatus.OK,response.getStatusCode());
    assertNull(response.getBody());
  }
}
