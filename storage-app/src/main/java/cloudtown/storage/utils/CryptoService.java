package cloudtown.storage.utils;

import cloudtown.storage.api.commons.CryptoException;
import java.security.MessageDigest;
import java.util.Arrays;
import java.util.Base64;
import javax.annotation.PostConstruct;
import javax.crypto.Cipher;
import javax.crypto.SecretKey;
import javax.crypto.spec.SecretKeySpec;
import lombok.extern.log4j.Log4j2;
import org.apache.commons.lang3.StringUtils;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Service;

@Log4j2
@Service
public class CryptoService {

  @Value("${cloudtown.keyword.crypto}")
  private String keywordCrypto;

  @Value("${cloudtown.algorithm.hash}")
  private String algorithmHash;

  @Value("${cloudtown.algorithm.crypto}")
  private String algorithmCrypto;

  @Value("${cloudtown.chartsetname.crypto}")
  private String chartsetname;

  private SecretKey secretKey = null;


  /**
   * Creates a new {@link SecretKey} based on a password.
   *
   * @throws CryptoException  if Encryption or decryption goes wrong.
   */
  @PostConstruct
  public void createKey() throws CryptoException {
    try {
      byte[] key = keywordCrypto.getBytes(chartsetname);
      MessageDigest sha = MessageDigest.getInstance(algorithmHash);
      key = sha.digest(key);
      key = Arrays.copyOf(key, 16);
      secretKey = new SecretKeySpec(key, algorithmCrypto);
    } catch (Exception e) {
      log.error("Error create key ", e);
      throw new CryptoException("Error create key " + e);
    }
  }

  /**
   * The method that will encrypt data.
   * 
   * @param data The data to encrypt.
   * @return The encrypted data.
   * @throws CryptoException  if Encryption or decryption goes wrong.
   */
  public String encrypt(String data) throws CryptoException {
    String result = StringUtils.EMPTY;
    if (StringUtils.isNotEmpty(data)) {
      result = this.encrypt(secretKey, data.getBytes());
    }
    return result;
  }

  /**
   * The method that will encrypt data.
   * 
   * @param secretKey The key used to encrypt the data.
   * @param data The data to encrypt.
   * @return The encrypted data.
   * @throws CryptoException  if Encryption or decryption goes wrong.
   */
  public String encrypt(SecretKey secretKey, byte[] data) throws CryptoException {
    try {
      Cipher cipher = Cipher.getInstance(algorithmCrypto);
      cipher.init(Cipher.ENCRYPT_MODE, secretKey);
      byte[] encryptData = cipher.doFinal(data);
      return Base64.getEncoder().encodeToString(encryptData);
    } catch (Exception e) {
      log.error("Erro encrypt content ", e);
      throw new CryptoException("Error encrypt content " + e);
    }
  }

  /**
   * The method that will decrypt a piece of encrypted data.
   * 
   * @param encrypted text to be encrypted.
   * @return The decrypted data.
   * @throws CryptoException  if Encryption or decryption goes wrong.
   */
  public String decrypt(String encrypted) throws CryptoException {
    String result = StringUtils.EMPTY;
    if (StringUtils.isNotEmpty(encrypted)) {
      result = decrypt(secretKey, encrypted.getBytes());
    }
    return result;
  }

  /**
   * The method that will decrypt a piece of encrypted data.
   * 
   * @param secretKey The key used to decrypt encrypted data.
   * @param encrypted The encrypted data.
   * @return The decrypted data.
   * @throws CryptoException  if Encryption or decryption goes wrong.
   */
  public String decrypt(SecretKey secretKey, byte[] encrypted) throws CryptoException {
    try {
      Cipher cipher = Cipher.getInstance(algorithmCrypto);
      cipher.init(Cipher.DECRYPT_MODE, secretKey);
      byte[] original = cipher.doFinal(Base64.getDecoder().decode(encrypted));
      return new String(original);
    } catch (Exception e) {
      log.error("Erro decrypt content ", e);
      throw new CryptoException("Error decrypt content " + e);
    }
  }

  /**
   * Returns the keywordCrypto.
   * 
   * @return the keywordCrypto.
   */
  public String getKeywordCrypto() {
    return keywordCrypto;
  }

  /**
   * Sets the keywordCrypto.
   * 
   * @param keywordCrypto the keywordCrypto to set.
   */
  public void setKeywordCrypto(String keywordCrypto) {
    this.keywordCrypto = keywordCrypto;
  }

  /**
   * Returns the algorithmHash.
   * 
   * @return the algorithmHash.
   */
  public String getAlgorithmHash() {
    return algorithmHash;
  }

  /**
   * Sets the algorithmHash.
   * 
   * @param algorithmHash the algorithmHash to set.
   */
  public void setAlgorithmHash(String algorithmHash) {
    this.algorithmHash = algorithmHash;
  }

  /**
   * Returns the algorithmCrypto.
   * 
   * @return the algorithmCrypto.
   */
  public String getAlgorithmCrypto() {
    return algorithmCrypto;
  }

  /**
   * Sets the algorithmCrypto.
   * 
   * @param algorithmCrypto the algorithmCrypto to set.
   */
  public void setAlgorithmCrypto(String algorithmCrypto) {
    this.algorithmCrypto = algorithmCrypto;
  }

  /**
   * Returns the chartsetname.
   * 
   * @return the chartsetname.
   */
  public String getChartsetname() {
    return chartsetname;
  }

  /**
   * Sets the chartsetname.
   * 
   * @param chartsetname the chartsetname to set.
   */
  public void setChartsetname(String chartsetname) {
    this.chartsetname = chartsetname;
  }


}
