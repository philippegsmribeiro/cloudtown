package cloudtown.storage.utils;

import java.util.Arrays;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.core.env.Environment;
import org.springframework.stereotype.Service;

/**
 * Utils about profiles.
 *
 */
@Service
public class ProfileUtils {

  public static final String DEVELOPMENT = "development";
  public static final String TEST = "test";
  public static final String STAGING = "staging";
  public static final String PRODUCTION = "production";
  public static final String DOCKER = "docker";

  private Environment env;

  @Autowired
  public ProfileUtils(Environment env) {
    this.env = env;
  }


  /**
   * Verify if profile test is included.
   * 
   * @return true if yes, false otherwise
   */
  public boolean isTest() {
    if (env.getActiveProfiles() != null 
        && Arrays.asList(env.getActiveProfiles()).contains(TEST)) {
      return true;
    }
    return false;
  }


  /**
   * Verify if profile production is included.
   * 
   * @return true if yes, false otherwise
   */
  public boolean isProduction() {
    if (env.getActiveProfiles() != null
        && Arrays.asList(env.getActiveProfiles()).contains(PRODUCTION)) {
      return true;
    }
    return false;
  }

  /**
   * Returns if profile docker is included.
   * 
   * @return true if yes, false otherwise
   */
  public boolean isDocker() {
    if (env.getActiveProfiles() != null
        && Arrays.asList(env.getActiveProfiles()).contains(DOCKER)) {
      return true;
    }
    return false;
  }

  /**
   * Returns the first and main profile.
   * 
   * @return main profile name
   */
  public String getMainProfile() {
    return env.getActiveProfiles() != null ? env.getActiveProfiles()[0] : null;
  }
}
