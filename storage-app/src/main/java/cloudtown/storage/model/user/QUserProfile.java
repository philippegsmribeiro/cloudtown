package cloudtown.storage.model.user;

import static com.querydsl.core.types.PathMetadataFactory.forVariable;

import com.querydsl.core.types.Path;
import com.querydsl.core.types.PathMetadata;
import com.querydsl.core.types.dsl.BeanPath;
import com.querydsl.core.types.dsl.StringPath;
import javax.annotation.Generated;

/**
 * QUserProfile is a Querydsl query type for UserProfile.
 */
@Generated("com.querydsl.codegen.EmbeddableSerializer")
public class QUserProfile extends BeanPath<UserProfile> {

  private static final long serialVersionUID = -1047111910L;

  public static final QUserProfile userProfile = new QUserProfile("userProfile");

  public final StringPath city = createString("city");

  public final StringPath country = createString("country");

  public final StringPath firstName = createString("firstName");

  public final StringPath jobRole = createString("jobRole");

  public final StringPath lastName = createString("lastName");

  public final StringPath picture = createString("picture");

  public final StringPath state = createString("state");

  public final StringPath telephone = createString("telephone");

  public QUserProfile(String variable) {
    super(UserProfile.class, forVariable(variable));
  }

  public QUserProfile(Path<? extends UserProfile> path) {
    super(path.getType(), path.getMetadata());
  }

  public QUserProfile(PathMetadata metadata) {
    super(UserProfile.class, metadata);
  }

}

