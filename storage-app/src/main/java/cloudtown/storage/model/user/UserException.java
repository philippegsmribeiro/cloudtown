package cloudtown.storage.model.user;

/**
 * Exception when a action request on an instance fails.
 * 
 */
public class UserException extends Exception {

  private static final long serialVersionUID = 1L;

  public UserException(String message) {
    super(message);
  }
}
