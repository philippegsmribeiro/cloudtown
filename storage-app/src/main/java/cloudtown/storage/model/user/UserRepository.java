package cloudtown.storage.model.user;

import org.springframework.data.mongodb.repository.MongoRepository;
import org.springframework.data.querydsl.QuerydslPredicateExecutor;

/**
 * User repository.<br>
 */
interface UserRepository extends MongoRepository<User, String>, QuerydslPredicateExecutor<User> {

  User findByLogin(String login);

}
