package cloudtown.storage.model.user;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

/**
 * AppClient Service.
 *
 */
@Service
public class AppClientService {

  private AppClientRepository appClientRepository;
  
  @Autowired
  public AppClientService(AppClientRepository appClientRepository) {
    this.appClientRepository = appClientRepository;
  }

  /**
   * Find the appclient by clientId.
   * @param clientId identificator
   * @return AppClient
   */
  public AppClient findByClientId(String clientId) {
    return appClientRepository.findById(clientId).orElse(null);
  }

  /**
   * Save an appclient.
   * @param appclient to be saved
   * @return saved appclient
   */
  public AppClient save(AppClient appclient) {
    return appClientRepository.save(appclient);
  }
  
  
}
