package cloudtown.storage.model.user;

import java.util.List;
import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;
import org.springframework.data.annotation.Id;
import org.springframework.data.mongodb.core.mapping.Document;
import org.springframework.security.core.GrantedAuthority;

/**
 * App client for oauth2.<br/>
 * Represents one application client to get credentials.
 */
@Data
@Builder
@NoArgsConstructor
@AllArgsConstructor
@Document
public class AppClient {

  @Id
  private String clientId;
  private String clientSecret;
  private String secretEncoder;
  private List<String> authorityGrantTypes;
  private List<GrantedAuthority> authorities;
  private List<String> scopes;
  private List<String> resourceIds;
}
