package cloudtown.storage.model.user;

import cloudtown.storage.api.commons.CloudConstants;
import cloudtown.storage.api.commons.CryptoException;
import cloudtown.storage.utils.CryptoService;
import com.querydsl.core.BooleanBuilder;
import com.querydsl.core.types.Predicate;
import java.util.ArrayList;
import java.util.List;
import java.util.Optional;
import lombok.extern.log4j.Log4j2;
import org.apache.commons.lang3.StringUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.core.userdetails.UsernameNotFoundException;
import org.springframework.stereotype.Service;

/**
 * User service.
 */
@Log4j2
@Service
public class UserService {

  private final UserRepository userRepository;
  private final CryptoService cryptoService;

  @Autowired
  public UserService(UserRepository userRepository, CryptoService cryptoService) {
    this.userRepository = userRepository;
    this.cryptoService = cryptoService;
  }

  /**
   * Find user by login name.
   *
   * @param login name
   * @return User
   * @throws CryptoException if Encryption or decryption goes wrong
   */
  public User findByLoginName(String login) throws CryptoException {
    User result = userRepository.findByLogin(cryptoService.encrypt(login));
    if (result == null) {
      throw new UsernameNotFoundException(login);
    }
    return result;
  }

  /**
   * Find user by id.
   *
   * @param id user
   * @return User
   */
  public User findById(String id) {
    Optional<User> user = userRepository.findById(id);
    if (user == null) {
      throw new UsernameNotFoundException(id);
    }
    User result = user.get();
    this.clearUserFields(result);
    return result;
  }

  /**
   * Find user profile by user id.
   *
   * @param id user
   * @return UserProfile
   */
  public UserProfile findProfileByUserId(String id) {
    return this.findById(id).getUserProfile();
  }

  /**
   * Save an user.
   * 
   * @param user to be saved
   * @return saved user
   * @throws UserException if User goes wrong
   * @throws CryptoException if Encryption or decryption goes wrong
   */
  public User save(User user) throws UserException {
    this.validateUser(user);
    this.encryptUserField(user);
    User result = userRepository.save(user);
    this.clearUserFields(result);
    return result;
  }

  /**
   * Method that performs user password change.
   * 
   * @param request Request to change user password.
   * @throws UserException if User goes wrong
   * @throws CryptoException if Encryption or decryption goes wrong
   */
  public void updatePassword(UserChangePasswordRequest request)
      throws UserException, CryptoException {
    User user = this.findByLoginName(request.getLogin());
    if (user != null) {
      String oldPassword = cryptoService.decrypt(user.getPassword());
      if (StringUtils.equals(request.getOldPassword(), oldPassword)) {
        user.setPassword(cryptoService.encrypt(request.getNewPassword()));
        userRepository.save(user);
      } else {
        throw new UserException("Password not valid");
      }
    }
  }

  /**
   * Method that performs user password change.
   * 
   * @param id User ID.
   * @param userProfile User profile.
   * @throws UserException if User goes wrong
   * @throws CryptoException if Encryption or decryption goes wrong
   */
  public UserProfile updateProfile(String id, UserProfile userProfile)
      throws UserException, CryptoException {
    UserProfile result = null;
    User user = this.findById(id);
    if (user != null) {
      user.setUserProfile(userProfile);
      result = userRepository.save(user).getUserProfile();
    }
    return result;
  }

  /**
   * Delete an user.
   * 
   * @param id User to be deleted
   * @throws UserException if User goes wrong
   * @throws CryptoException if Encryption or decryption goes wrong
   */
  public void delete(String id) throws UserException {
    userRepository.deleteById(id);
  }

  /**
   * Method that cleans user fields.
   * 
   * @param user User that goes clean.
   */
  private void clearUserFields(User user) {
    user.setPassword(StringUtils.EMPTY);
  }

  /**
   * Method that method that encrypts user fields.
   * 
   * @param user User to be encrypted.
   * @throws UserException if User goes wrong
   */
  private void encryptUserField(User user) throws UserException {
    try {
      user.setLogin(cryptoService.encrypt(user.getLogin()));
      user.setPassword(cryptoService.encrypt(user.getPassword()));
      user.setEmail(cryptoService.encrypt(user.getEmail()));
    } catch (CryptoException e) {
      log.error("Erro encrypt user fields ", e);
      throw new UserException("Erro encrypt user fields " + e);
    }
  }

  /**
   * Method that validates the request.
   * 
   * @param userRequest Request to create a user.
   * @throws UserException if User goes wrong
   */
  private void validateUser(User userRequest) throws UserException {
    if (userRequest == null) {
      throw new UserException(String.format(CloudConstants.MESSAGE_ERROR_REQUEST, "User"));
    }
    if (StringUtils.isEmpty(userRequest.getLogin())) {
      throw new UserException(String.format(CloudConstants.MESSAGE_ERROR_REQUEST, "Login"));
    }
    if ((StringUtils.isEmpty(userRequest.getEmail()))) {
      throw new UserException(String.format(CloudConstants.MESSAGE_ERROR_REQUEST, "Email"));
    }
    if ((StringUtils.isEmpty(userRequest.getPassword()))) {
      throw new UserException(String.format(CloudConstants.MESSAGE_ERROR_REQUEST, "Password"));
    }
  }

  /**
   * Method that returns a list of User.
   * 
   * @param userRequest {@link UserRequest} that represents the request to search the User.
   * @return list the User.
   * @throws UserException if User goes wrong
   */
  public List<User> findByFilter(UserRequest userRequest) throws UserException {
    List<User> result = new ArrayList<User>();
    if (userRequest == null) {
      throw new UserException(String.format(CloudConstants.MESSAGE_ERROR_REQUEST, "User Request"));
    }
    result = (List<User>) userRepository.findAll(getFiltersQ(userRequest));
    return result;
  }

  /**
   * Method that mount a filter for User.
   * 
   * @param userRequest Request generic by search user.
   * @return Predicate Conditions the filter
   */
  private Predicate getFiltersQ(UserRequest userRequest) {
    BooleanBuilder builder = new BooleanBuilder();
    QUser user = new QUser("user");
    if (StringUtils.isNotBlank(userRequest.getFirstName())) {
      builder.and(user.userProfile.firstName.contains(userRequest.getFirstName()));
    }
    if (StringUtils.isNotBlank(userRequest.getCity())) {
      builder.and(user.userProfile.city.contains(userRequest.getCity()));
    }
    if (StringUtils.isNotBlank(userRequest.getLastName())) {
      builder.and(user.userProfile.lastName.contains(userRequest.getLastName()));
    }
    if (StringUtils.isNotBlank(userRequest.getCountry())) {
      builder.and(user.userProfile.country.contains(userRequest.getCountry()));
    }
    if (StringUtils.isNotBlank(userRequest.getState())) {
      builder.and(user.userProfile.state.contains(userRequest.getState()));
    }
    return builder;
  }
}
