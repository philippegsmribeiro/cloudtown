package cloudtown.storage.model.user;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;
import org.springframework.data.annotation.Id;
import org.springframework.data.mongodb.core.mapping.Document;

@Data
@Builder
@NoArgsConstructor
@AllArgsConstructor
@Document
public class User {

  @Id
  private String id;
  private String name;
  private String login;
  private String password;
  private String email;
  private UserProfile userProfile;
}
