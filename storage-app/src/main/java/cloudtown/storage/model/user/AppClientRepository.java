package cloudtown.storage.model.user;

import org.springframework.data.mongodb.repository.MongoRepository;

/**
 * AppClient repository.<br>
 */
interface AppClientRepository extends MongoRepository<AppClient, String> {
}
