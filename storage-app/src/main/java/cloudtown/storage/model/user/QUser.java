package cloudtown.storage.model.user;

import static com.querydsl.core.types.PathMetadataFactory.forVariable;

import com.querydsl.core.types.Path;
import com.querydsl.core.types.PathMetadata;
import com.querydsl.core.types.dsl.EntityPathBase;
import com.querydsl.core.types.dsl.PathInits;
import com.querydsl.core.types.dsl.StringPath;
import javax.annotation.Generated;

/**
 * QUser is a Querydsl query type for User.
 */
@Generated("com.querydsl.codegen.EntitySerializer")
public class QUser extends EntityPathBase<User> {

  private static final long serialVersionUID = -41387121L;

  private static final PathInits INITS = PathInits.DIRECT2;

  public static final QUser user = new QUser("user");

  public final StringPath email = createString("email");

  public final StringPath id = createString("id");

  public final StringPath login = createString("login");

  public final StringPath name = createString("name");

  public final StringPath password = createString("password");

  public final QUserProfile userProfile;

  public QUser(String variable) {
    this(User.class, forVariable(variable), INITS);
  }

  public QUser(Path<? extends User> path) {
    this(path.getType(), path.getMetadata(), PathInits.getFor(path.getMetadata(), INITS));
  }

  public QUser(PathMetadata metadata) {
    this(metadata, PathInits.getFor(metadata, INITS));
  }

  public QUser(PathMetadata metadata, PathInits inits) {
    this(User.class, metadata, inits);
  }

  /**
   * User class constructor.
   * 
   * @param type Type of classes user.
   * @param metadata User Metadata.
   * @param inits Defines path initializations that can be attached to properties via QueryInit
   *        annotations.
   */
  public QUser(Class<? extends User> type, PathMetadata metadata, PathInits inits) {
    super(type, metadata, inits);
    this.userProfile =
        inits.isInitialized("userProfile") ? new QUserProfile(forProperty("userProfile")) : null;
  }

}

