package cloudtown.storage.model.user;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@Builder
@NoArgsConstructor
@AllArgsConstructor
public class UserPreferences {
  private String dateFormat;
  private String timeFormat;
  private boolean alertNotifications;
  private boolean emailNotification;
  private boolean use24HourFormat;
  private boolean useSsl;
}
