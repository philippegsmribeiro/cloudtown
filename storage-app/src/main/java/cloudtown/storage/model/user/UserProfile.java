package cloudtown.storage.model.user;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@Builder
@NoArgsConstructor
@AllArgsConstructor
public class UserProfile {
  private String firstName;
  private String lastName;
  private String country;
  private String state;
  private String city;
  private String telephone;
  private String jobRole;
  private String picture;
  private UserPreferences userPreferences;
}
