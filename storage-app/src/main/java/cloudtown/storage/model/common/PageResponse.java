package cloudtown.storage.model.common;

import java.util.List;
import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;
import org.springframework.data.domain.Page;

@Data
@Builder
@AllArgsConstructor
@NoArgsConstructor
public class PageResponse<T> {

  private Long totalElements;
  private Integer pageSize;
  private Integer page;
  private List<T> content;

  /**
   * Create an instance based on the data Page object.
   * @param page data page object
   * @return PageResponse
   */
  public static <T> PageResponse<T> fromPage(Page<T> page) {
    if (page == null) {
      return null;
    }
    return new PageResponse<>(page.getTotalElements(), 
                              page.getSize(),
                              page.getNumber(),
                              page.getContent());
  }

}
