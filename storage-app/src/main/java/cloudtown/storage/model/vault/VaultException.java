package cloudtown.storage.model.vault;

/**
 * Exception when a action request on an instance fails.
 * 
 */
public class VaultException extends Exception {

  private static final long serialVersionUID = 1L;

  public VaultException(String message) {
    super(message);
  }
}
