package cloudtown.storage.model.vault;

import cloudtown.storage.cloud.Provider;
import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@Builder
@NoArgsConstructor
@AllArgsConstructor
public class VaultListRequest {

  private Provider provider;
  private String accountId;
  private String region;
}
