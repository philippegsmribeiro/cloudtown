package cloudtown.storage.model.vault;

import java.time.LocalDateTime;
import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;
import org.springframework.data.mongodb.core.mapping.Document;

@Data
@Builder
@NoArgsConstructor
@AllArgsConstructor
@Document
public class Vault {

  private String id;
  private String name;
  private String arn;
  private LocalDateTime creationDate;
  private LocalDateTime lastInventoryDate;
  private Long numberOfFiles;
  private Long sizeInBytes;
}
