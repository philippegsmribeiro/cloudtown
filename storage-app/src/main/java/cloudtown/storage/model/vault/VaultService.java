package cloudtown.storage.model.vault;

import cloudtown.storage.cloud.provider.ProviderIntegrationException;
import cloudtown.storage.cloud.service.storage.CloudStorageService;
import cloudtown.storage.model.account.AccountException;
import java.util.List;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.util.StringUtils;

@Service
public class VaultService {

  private final CloudStorageService cloudStorageService;
  private static final String MESSAGE_ERROR_REQUEST = "%s can not be null or empty";

  @Autowired
  public VaultService(CloudStorageService cloudStorageService) {
    this.cloudStorageService = cloudStorageService;
  }

  /**
   * List vaults.
   * 
   * @param vaultListRequest request
   * @return list the Vaults
   * @throws ProviderIntegrationException if provider integrations goes wrong
   * @throws VaultException if validation error
   * @throws AccountException if it fails to get the credentials for the account 
   */
  public List<Vault> listVaults(VaultListRequest vaultListRequest)
      throws ProviderIntegrationException, VaultException, AccountException {
    this.validateVaultListRequest(vaultListRequest);
    return cloudStorageService.listVaults(vaultListRequest);
  }

  /**
   * Method that validates the request.
   * 
   * @param vaultListRequest request
   * @throws VaultException if something goes wrong
   */
  private void validateVaultListRequest(VaultListRequest vaultListRequest) throws VaultException {
    if (vaultListRequest == null) {
      throw new VaultException(String.format(MESSAGE_ERROR_REQUEST, "Vault"));
    }
    if (vaultListRequest.getProvider() == null) {
      throw new VaultException(String.format(MESSAGE_ERROR_REQUEST, "Provider"));
    }
    if (StringUtils.isEmpty(vaultListRequest.getAccountId())) {
      throw new VaultException(String.format(MESSAGE_ERROR_REQUEST, "AccountId"));
    }
    if (StringUtils.isEmpty(vaultListRequest.getRegion())) {
      throw new VaultException(String.format(MESSAGE_ERROR_REQUEST, "Region"));
    }
  }

}
