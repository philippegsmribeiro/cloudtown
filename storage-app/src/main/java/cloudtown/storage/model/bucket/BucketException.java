package cloudtown.storage.model.bucket;

/**
 * Exception when a action request on an instance fails.
 * 
 */
public class BucketException extends Exception {

  private static final long serialVersionUID = 1L;

  public BucketException(String message) {
    super(message);
  }
}
