package cloudtown.storage.model.bucket;

import com.amazonaws.services.s3.model.AccessControlList;
import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;
import org.springframework.data.mongodb.core.mapping.Document;

@Data
@Builder
@NoArgsConstructor
@AllArgsConstructor
@Document
public class BucketDetail {
  private String region;
  private AccessControlList accessControl;
}
