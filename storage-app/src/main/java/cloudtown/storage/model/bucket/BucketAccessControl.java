package cloudtown.storage.model.bucket;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;
import org.springframework.data.mongodb.core.mapping.Document;

@Data
@Builder
@NoArgsConstructor
@AllArgsConstructor
@Document
public class BucketAccessControl {
  private String granteeType;
  private String grenteeIdentifier;
  private String permission;

}
