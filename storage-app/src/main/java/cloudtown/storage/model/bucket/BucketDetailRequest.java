package cloudtown.storage.model.bucket;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@Builder
@NoArgsConstructor
@AllArgsConstructor
public class BucketDetailRequest {

  private String accountId;
  private String region;
  private String name;
}
