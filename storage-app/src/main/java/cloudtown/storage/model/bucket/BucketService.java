package cloudtown.storage.model.bucket;

import cloudtown.storage.cloud.provider.ProviderIntegrationException;
import cloudtown.storage.cloud.service.storage.CloudStorageService;
import cloudtown.storage.model.account.AccountException;
import com.amazonaws.services.s3.model.AccessControlList;
import com.amazonaws.services.s3.model.Grant;
import java.util.List;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.util.StringUtils;

@Service
public class BucketService {

  private final CloudStorageService cloudStorageService;
  private static final String MESSAGE_ERROR_REQUEST = "%s can not be null or empty";

  @Autowired
  public BucketService(CloudStorageService cloudStorageService) {
    this.cloudStorageService = cloudStorageService;
  }

  /**
   * List buckets.
   * 
   * @param bucketListRequest request
   * @return list the Buckets
   * @throws ProviderIntegrationException if integration goes wrong
   * @throws BucketException if Bucket goes wrong
   * @throws AccountException if User Account goes wrong
   */
  public List<Bucket> listBuckets(BucketListRequest bucketListRequest)
      throws ProviderIntegrationException, BucketException, AccountException {
    this.validateBucketListRequest(bucketListRequest);
    return cloudStorageService.listBuckets(bucketListRequest);
  }

  /**
   * Get bucket permissions.
   * 
   * @param bucketDetailRequest of {@link cloudtown.storage.model.bucket.BucketDetailRequest}
   * @return AccessControlList of {@link com.amazonaws.services.s3.model.AccessControlList}
   * @throws ProviderIntegrationException if integration goes wrong.
   * @throws AccountException if User Account goes wrong
   * @throws BucketException if Bucket goes wrong
   */
  public AccessControlList getAccessControlBucket(BucketDetailRequest bucketDetailRequest)
      throws ProviderIntegrationException, AccountException, BucketException {
    this.validateBucketDetailRequest(bucketDetailRequest);
    AccessControlList accessControlList =
        cloudStorageService.getAccessControlBucket(bucketDetailRequest);
    if (accessControlList != null && accessControlList.getGrantsAsList() != null) {
      accessControlList.getGrantsAsList().clear();
      accessControlList.getGrantsAsList()
          .addAll(accessControlList.getGrantsAsList());
    }
    List<Grant> list = accessControlList.getGrantsAsList();
    accessControlList.getGrantsAsList().clear();
    accessControlList.getGrantsAsList().addAll(list);
    return accessControlList;
  }

  /**
   * Get bucket region.
   * 
   * @param bucketDetailRequest of {@link cloudtown.storage.model.bucket.BucketDetailRequest}
   * @return region of bucket
   * @throws ProviderIntegrationException if integration goes wrong.
   * @throws AccountException if User Account goes wrong
   * @throws BucketException if Bucket goes wrong
   */
  public String getRegionBucket(BucketDetailRequest bucketDetailRequest)
      throws ProviderIntegrationException, AccountException, BucketException {
    this.validateBucketDetailRequest(bucketDetailRequest);
    return cloudStorageService.getRegionBucket(bucketDetailRequest);
  }

  /**
   * Get bucket detail.
   * 
   * @param bucketDetailRequest of {@link cloudtown.storage.model.bucket.BucketDetailRequest}
   * @return bucketDetail of {@link cloudtown.storage.model.bucket.BucketDetail}
   * @throws ProviderIntegrationException if integration goes wrong.
   * @throws AccountException if User Account goes wrong
   * @throws BucketException if Bucket goes wrong
   */
  public BucketDetail getBucketDetail(BucketDetailRequest bucketDetailRequest)
      throws ProviderIntegrationException, AccountException, BucketException {
    this.validateBucketDetailRequest(bucketDetailRequest);
    BucketDetail detail = cloudStorageService.getBucketDetail(bucketDetailRequest);
    if (detail.getAccessControl() != null && detail.getAccessControl().getGrantsAsList() != null) {
      detail.getAccessControl().getGrantsAsList().clear();
      detail.getAccessControl().getGrantsAsList()
          .addAll(detail.getAccessControl().getGrantsAsList());
    }
    return detail;
  }

  /**
   * Method that validates the request.
   * 
   * @param bucketListRequest Information of the bucket to be searched.
   * @throws BucketException if Bucket goes wrong
   */
  private void validateBucketListRequest(BucketListRequest bucketListRequest)
      throws BucketException {
    if (bucketListRequest == null) {
      throw new BucketException(String.format(MESSAGE_ERROR_REQUEST, "Bucket"));
    }
    if (bucketListRequest.getProvider() == null) {
      throw new BucketException(String.format(MESSAGE_ERROR_REQUEST, "Provider"));
    }
    if (StringUtils.isEmpty(bucketListRequest.getAccountId())) {
      throw new BucketException(String.format(MESSAGE_ERROR_REQUEST, "AccountId"));
    }
    if (StringUtils.isEmpty(bucketListRequest.getRegion())) {
      throw new BucketException(String.format(MESSAGE_ERROR_REQUEST, "Region"));
    }
  }

  /**
   * Method that validates the request.
   * 
   * @param bucketDetailRequest Information of the bucket detail to be searched.
   * @throws BucketException if Bucket goes wrong
   */
  private void validateBucketDetailRequest(BucketDetailRequest bucketDetailRequest)
      throws BucketException {
    if (bucketDetailRequest == null) {
      throw new BucketException(String.format(MESSAGE_ERROR_REQUEST, "Bucket Detail"));
    }
    if (StringUtils.isEmpty(bucketDetailRequest.getAccountId())) {
      throw new BucketException(String.format(MESSAGE_ERROR_REQUEST, "AccountId"));
    }
    if (StringUtils.isEmpty(bucketDetailRequest.getName())) {
      throw new BucketException(String.format(MESSAGE_ERROR_REQUEST, "Bucket name"));
    }
    if (StringUtils.isEmpty(bucketDetailRequest.getRegion())) {
      throw new BucketException(String.format(MESSAGE_ERROR_REQUEST, "Region"));
    }
  }

}
