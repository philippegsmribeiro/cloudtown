package cloudtown.storage.model.bucket;

import cloudtown.storage.cloud.Provider;
import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@Builder
@NoArgsConstructor
@AllArgsConstructor
public class BucketListRequest {

  private Provider provider;
  private String accountId;
  private String region;
  private boolean fullDetail;
}
