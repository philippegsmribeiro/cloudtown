package cloudtown.storage.model.bucket;

import java.util.Date;
import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;
import org.springframework.data.mongodb.core.mapping.Document;

@Data
@Builder
@NoArgsConstructor
@AllArgsConstructor
@Document
public class Bucket {
  private String name;
  private Date creationDate;
  private String ownerName;
  private String ownerId;
  private BucketDetail detail;
}
