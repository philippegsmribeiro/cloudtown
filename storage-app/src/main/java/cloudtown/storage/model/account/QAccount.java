package cloudtown.storage.model.account;

import static com.querydsl.core.types.PathMetadataFactory.forVariable;

import com.querydsl.core.types.Path;
import com.querydsl.core.types.PathMetadata;
import com.querydsl.core.types.dsl.EntityPathBase;
import com.querydsl.core.types.dsl.EnumPath;
import com.querydsl.core.types.dsl.ListPath;
import com.querydsl.core.types.dsl.PathInits;
import com.querydsl.core.types.dsl.StringPath;
import javax.annotation.Generated;


/**
 * QAccount is a Querydsl query type for Account.
 */
@Generated("com.querydsl.codegen.EntitySerializer")
public class QAccount extends EntityPathBase<Account> {

  private static final long serialVersionUID = -1014851091L;

  public static final QAccount account = new QAccount("account");
  
  public final ListPath<Credential, QCredential> credentials =
      this.<Credential, QCredential>createList("credentials", Credential.class, QCredential.class,
          PathInits.DIRECT2);

  public final StringPath description = createString("description");

  public final StringPath id = createString("id");

  public final EnumPath<cloudtown.storage.cloud.Provider> provider =
      createEnum("provider", cloudtown.storage.cloud.Provider.class);

  public QAccount(String variable) {
    super(Account.class, forVariable(variable));
  }

  public QAccount(Path<? extends Account> path) {
    super(path.getType(), path.getMetadata());
  }

  public QAccount(PathMetadata metadata) {
    super(Account.class, metadata);
  }

}

