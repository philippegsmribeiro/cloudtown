package cloudtown.storage.model.account;

import cloudtown.storage.cloud.Provider;
import java.util.List;
import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;
import org.springframework.data.annotation.Id;
import org.springframework.data.mongodb.core.mapping.Document;

/**
 * Cloud account.
 * 
 * @author flaviolcastro
 *
 */
@Data
@Builder
@NoArgsConstructor
@AllArgsConstructor
@Document
public class Account {

  @Id
  private String id;
  private Provider provider;
  private String description;
  private List<Credential> credentials;

}
