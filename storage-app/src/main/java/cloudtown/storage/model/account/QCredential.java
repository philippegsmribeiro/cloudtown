package cloudtown.storage.model.account;

import static com.querydsl.core.types.PathMetadataFactory.forVariable;

import com.querydsl.core.types.Path;
import com.querydsl.core.types.PathMetadata;
import com.querydsl.core.types.dsl.BeanPath;
import com.querydsl.core.types.dsl.SimplePath;
import javax.annotation.Generated;

/**
 * QCredential is a Querydsl query type for Credential.
 */
@Generated("com.querydsl.codegen.EmbeddableSerializer")
public class QCredential extends BeanPath<Credential> {

  private static final long serialVersionUID = -1034043241L;

  public static final QCredential credential = new QCredential("credential");

  public final SimplePath<AwsCredential> awsCredential =
      createSimple("awsCredential", AwsCredential.class);

  public QCredential(String variable) {
    super(Credential.class, forVariable(variable));
  }

  public QCredential(Path<? extends Credential> path) {
    super(path.getType(), path.getMetadata());
  }

  public QCredential(PathMetadata metadata) {
    super(Credential.class, metadata);
  }

}

