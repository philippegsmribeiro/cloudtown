package cloudtown.storage.model.account;

/**
 * Exception when a action request on an instance fails.
 * 
 */
public class AccountException extends Exception {

  private static final long serialVersionUID = 1L;

  public AccountException(String message) {
    super(message);
  }
}
