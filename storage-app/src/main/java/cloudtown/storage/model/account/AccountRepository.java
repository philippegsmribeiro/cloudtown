package cloudtown.storage.model.account;

import cloudtown.storage.cloud.Provider;
import java.util.List;
import org.springframework.data.mongodb.repository.MongoRepository;
import org.springframework.data.querydsl.QuerydslPredicateExecutor;

/**
 * Account repository.<br>
 * 
 * @author flaviolcastro
 */
interface AccountRepository
    extends MongoRepository<Account, String>, QuerydslPredicateExecutor<Account> {

  List<Account> findByProvider(Provider provider);
}
