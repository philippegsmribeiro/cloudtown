package cloudtown.storage.model.account;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@Builder
@NoArgsConstructor
@AllArgsConstructor
public class AwsCredential {

  // Aws fields
  private String accountId;
  private String accessKey;
  private String secretKey;
}
