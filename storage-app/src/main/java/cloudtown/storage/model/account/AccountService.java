package cloudtown.storage.model.account;

import com.querydsl.core.BooleanBuilder;
import com.querydsl.core.types.Predicate;
import java.util.ArrayList;
import java.util.List;
import org.apache.commons.lang3.StringUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageRequest;
import org.springframework.stereotype.Service;
import org.springframework.util.CollectionUtils;

@Service
public class AccountService {

  private final AccountRepository accountRepository;
  private static final String MESSAGE_ERROR_REQUEST = "%s can not be null or empty";

  @Autowired
  public AccountService(AccountRepository accountRepository) {
    this.accountRepository = accountRepository;
  }


  /**
   * Method that returns a list of Account.
   * 
   * @param accountRequest {@link AccountRequest} that represents the request to search the Account.
   * @return list the Account.
   * @throws AccountException if User Account goes wrong
   */
  public List<Account> findByFilter(AccountRequest accountRequest) throws AccountException {
    List<Account> result = new ArrayList<Account>();
    if (accountRequest == null) {
      throw new AccountException(String.format(MESSAGE_ERROR_REQUEST, "Account Request"));
    }
    result = (List<Account>) accountRepository.findAll(getFiltersQ(accountRequest));
    this.clearSecretKeyByListAccount(result);
    return result;
  }

  /**
   * Retrieve by the id.
   * 
   * @param id of account.
   * @param secretKeyEmpty it's true to clean the secretkey.
   * @return Account if found, null otherwise.
   * @throws AccountException if User Account goes wrong
   */
  public Account retrieveById(String id, boolean secretKeyEmpty) throws AccountException {
    if (StringUtils.isEmpty(id)) {
      throw new AccountException(String.format(MESSAGE_ERROR_REQUEST, "Id"));
    }
    Account result = accountRepository.findById(id).orElse(null);
    if (result != null && secretKeyEmpty) {
      this.clearSecretKeyByAccount(result);
    }
    return result;
  }

  /**
   * Retrieve all account using pagination.
   * 
   * @param pageRequest pagination input
   * @return Page of account
   * @throws AccountException if User Account goes wrong
   */
  public Page<Account> retrieveAll(PageRequest pageRequest) throws AccountException {
    if (pageRequest == null) {
      throw new AccountException(String.format(MESSAGE_ERROR_REQUEST, "Request"));
    }
    Page<Account> result = accountRepository.findAll(pageRequest);
    if (result != null) {
      this.clearSecretKeyByListAccount(result.getContent());
    }
    return result;
  }

  /**
   * Remove an account by id.
   * 
   * @param id of account
   * @return true if removed, false otherwise
   * @throws AccountException if User Account goes wrong
   */
  public Boolean deleteById(String id) throws AccountException {
    if (StringUtils.isEmpty(id)) {
      throw new AccountException(String.format(MESSAGE_ERROR_REQUEST, "Id"));
    }
    try {
      accountRepository.deleteById(id);
    } catch (Exception e) {
      throw new AccountException("Error " + e.getMessage());
    }
    return retrieveById(id, false) == null;
  }

  /**
   * Save an example. Update if id is set, Insert otherwise.
   * 
   * @param account to be saved
   * @return saved Account
   * @throws AccountException if User Account goes wrong
   */
  public Account save(Account account) throws AccountException {
    if (account == null) {
      throw new AccountException(String.format(MESSAGE_ERROR_REQUEST, "Account"));
    }
    if (StringUtils.isEmpty(account.getId())) {
      throw new AccountException(String.format(MESSAGE_ERROR_REQUEST, "Id"));
    }
    if (account.getProvider() == null) {
      throw new AccountException(String.format(MESSAGE_ERROR_REQUEST, "Provider"));
    }
    Account result = accountRepository.save(account);
    this.clearSecretKeyByAccount(account);
    return result;
  }

  /**
   * Return the first credential for a cloud account.<br/>
   * TODO: we will need to change the app to not use this in the future.
   * 
   * @param accountId id of account
   * @param secretKeyEmpty it's true to clean the secretkey.
   * @return Credential if found, null otherwise
   * @throws AccountException if User Account goes wrong
   */
  public Credential getFirstCredential(String accountId, boolean secretKeyEmpty)
      throws AccountException {
    if (!StringUtils.isEmpty(accountId)) {
      Account account = retrieveById(accountId, false);
      if (account != null && !CollectionUtils.isEmpty(account.getCredentials())) {
        Credential credential = account.getCredentials().get(0);
        if (secretKeyEmpty) {
          this.clearSecretKeyByCredential(credential);
        }
        return credential;
      }
    }
    return null;
  }

  /**
   * Method that mount a filter for Account.
   * 
   * @param accountRequest Request generic by search accounts.
   * @return Predicate Conditions the filter
   */
  private Predicate getFiltersQ(AccountRequest accountRequest) {
    BooleanBuilder builder = new BooleanBuilder();
    QAccount account = new QAccount("account");
    if (StringUtils.isNotBlank(accountRequest.getId())) {
      builder.and(account.id.contains(accountRequest.getId()));
    }
    if (accountRequest.getProvider() != null) {
      builder.and(account.provider.eq(accountRequest.getProvider()));
    }
    if (StringUtils.isNotBlank(accountRequest.getDescription())) {
      builder.and(account.description.contains(accountRequest.getDescription()));
    }
    return builder;
  }


  /**
   * Method that cleans the key secret from account list.
   * 
   * @param accounts list of user accounts that will be cleared secret keys.
   */
  private void clearSecretKeyByListAccount(List<Account> accounts) {
    for (Account account : accounts) {
      this.clearSecretKeyByAccount(account);
    }
  }

  /**
   * Method that cleans the key secret from account.
   * 
   * @param account user accounts that will be cleared secret keys.
   */
  private void clearSecretKeyByAccount(Account account) {
    if (!CollectionUtils.isEmpty(account.getCredentials())) {
      account.getCredentials().forEach(c -> {
        this.clearSecretKeyByCredential(c);
      });
    }
  }

  /**
   * Method that cleans the key secret from credential.
   * 
   * @param credential credential that will clear the secret keys.
   */
  private void clearSecretKeyByCredential(Credential credential) {
    if (credential.getAwsCredential() != null) {
      credential.getAwsCredential().setSecretKey(StringUtils.EMPTY);
    }
  }

}
