package cloudtown.storage.model.account;

import cloudtown.storage.cloud.Provider;
import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

/**
 * Cloud account request.
 *
 */
@Data
@Builder
@NoArgsConstructor
@AllArgsConstructor
public class AccountRequest {

  private String id;
  private Provider provider;
  private String description;

}
