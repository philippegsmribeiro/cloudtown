package cloudtown.storage.model.example;

import lombok.Data;
import lombok.NoArgsConstructor;

/**
 * Example of a sub class which doesn't need the own collection,
 * but it will used by another one.
 * 
 * @author flaviolcastro
 *
 */
@Data
@NoArgsConstructor
public class SubClass {
  
  private String blah;
}