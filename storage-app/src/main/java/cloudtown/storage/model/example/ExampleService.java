package cloudtown.storage.model.example;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageRequest;
import org.springframework.stereotype.Service;
import org.springframework.util.StringUtils;

/**
 * Example service.<br>
 * Service class to manipulate the Example object.
 * 
 * @author flaviolcastro
 *
 */
@Service
public class ExampleService {

  private final ExampleRepository repository;

  @Autowired
  public ExampleService(ExampleRepository exampleRepository) {
    this.repository = exampleRepository;
  }

  /**
   * Retrieve by the id.
   * 
   * @param id of example.
   * @return Example if found, null otherwise.
   */
  public Example retrieveById(String id) {
    if (StringUtils.isEmpty(id)) {
      return null;
    }
    return repository.findById(id).orElse(null);
  }

  /**
   * Retrieve all example using pagination.
   * 
   * @param pageRequest pagination input
   * @return Page of examples
   */
  public Page<Example> retrieveAll(PageRequest pageRequest) {
    return repository.findAll(pageRequest);
  }

  /**
   * Remove an example by id.
   * 
   * @param id of example
   * @return true if removed, false otherwise
   */
  public Boolean deleteById(String id) {
    repository.deleteById(id);
    return retrieveById(id) == null;
  }

  /**
   * Save an example. Update if id is set, Insert otherwise.
   * 
   * @param example to be saved
   * @return saved Example
   */
  public Example save(Example example) {
    return repository.save(example);
  }
}
