package cloudtown.storage.model.example;

import org.springframework.data.mongodb.repository.MongoRepository;

/**
 * Example repository.<br>
 * Note: the repository is not public, it should be visible just in the package.<br>
 * All external access should be through the service class.
 * 
 * @author flaviolcastro
 */
interface ExampleRepository extends MongoRepository<Example, String> {

}
