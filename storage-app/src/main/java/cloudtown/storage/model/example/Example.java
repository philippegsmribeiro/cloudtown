package cloudtown.storage.model.example;

import java.math.BigDecimal;
import java.time.LocalDate;
import java.time.LocalDateTime;
import java.util.List;
import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;
import org.springframework.data.annotation.Id;
import org.springframework.data.annotation.Version;
import org.springframework.data.mongodb.core.mapping.Document;

/**
 * Example entity/document class.<br>
 * Use: @Data for the getter/setter/etc
 *      @Builder for builder
 */
@Data
@Builder
@NoArgsConstructor
@AllArgsConstructor
@Document
public class Example {

  @Id
  private String id;
  private String string;
  private BigDecimal bigDecimal;
  private Integer intNumber;
  private Boolean conditional;
  private LocalDate date;
  private LocalDateTime dateTime;
  private ExampleType type;
  private SubClass subClass;
  private List<String> list;
  private String[] array;
  @Version
  private Long version;
}
