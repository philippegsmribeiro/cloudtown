package cloudtown.storage.model.example;

/**
 * Enum example.
 * 
 * @author flaviolcastro
 *
 */
public enum ExampleType {
  TEST, NON_TEST
}
