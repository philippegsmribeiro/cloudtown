package cloudtown.storage.model.region;

import cloudtown.storage.cloud.Provider;
import cloudtown.storage.cloud.service.compute.CloudComputeService;
import java.util.Collections;
import java.util.List;
import lombok.extern.log4j.Log4j2;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.util.CollectionUtils;

/**
 * Region service.
 * 
 * @author flaviolcastro
 *
 */
@Log4j2
@Service
public class RegionService {

  private final RegionRepository regionRepository;
  private final CloudComputeService cloudComputeService;

  @Autowired
  public RegionService(RegionRepository regionRepository, CloudComputeService cloudComputeService) {
    this.regionRepository = regionRepository;
    this.cloudComputeService = cloudComputeService;
  }

  /**
   * List all regions.
   * 
   * @param accountId the provider account id
   * @param provider of regions
   * @return
   */
  public List<Region> list(String accountId, Provider provider) {
    // try to find locally
    List<Region> savedList = regionRepository.findByProvider(provider);
    if (!CollectionUtils.isEmpty(savedList)) {
      return savedList;
    }
    try {
      // retrieve from provider
      List<Region> providerList = cloudComputeService.listRegions(accountId, provider);
      if (!CollectionUtils.isEmpty(providerList)) {
        // save it
        return regionRepository.saveAll(providerList);
      }
    } catch (Exception e) {
      log.error("Error retriving regions from provider!", e);
    }
    return Collections.emptyList();
  }
}
