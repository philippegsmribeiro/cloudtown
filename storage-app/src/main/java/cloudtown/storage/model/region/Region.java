package cloudtown.storage.model.region;

import cloudtown.storage.cloud.Provider;
import java.util.List;
import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;
import org.springframework.data.annotation.Id;
import org.springframework.data.annotation.Version;
import org.springframework.data.mongodb.core.mapping.Document;

/**
 * Region object.
 * 
 * @author flaviolcastro
 *
 */
@Data
@Builder
@NoArgsConstructor
@AllArgsConstructor
@Document
public class Region {

  @Id
  private String id;
  private Provider provider;
  private String providerId;
  private String name;
  private List<Zone> zones;
  @Version
  private Long version;
}
