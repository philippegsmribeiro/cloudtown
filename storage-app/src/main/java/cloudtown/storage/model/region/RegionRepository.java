package cloudtown.storage.model.region;

import cloudtown.storage.cloud.Provider;
import java.util.List;
import org.springframework.data.mongodb.repository.MongoRepository;

/**
 * Region repository.<br>
 * 
 * @author flaviolcastro
 */
interface RegionRepository extends MongoRepository<Region, String> {

  List<Region> findByProvider(Provider provider);
}
