package cloudtown.storage.cloud.service.compute;

import cloudtown.storage.cloud.Provider;
import cloudtown.storage.cloud.provider.ProviderIntegrationException;
import cloudtown.storage.cloud.provider.aws.AwsEc2;
import cloudtown.storage.model.account.Credential;
import cloudtown.storage.model.region.Region;
import cloudtown.storage.model.region.Zone;
import com.amazonaws.services.ec2.model.AvailabilityZone;
import java.util.ArrayList;
import java.util.List;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

/**
 * Aws Compute Service integration service.
 * 
 * @author flaviolcastro
 *
 */
@Service
class AwsComputeService {

  private final AwsEc2 awsEc2;

  @Autowired
  public AwsComputeService(AwsEc2 awsEc2) {
    this.awsEc2 = awsEc2;
  }

  /**
   * List the regions.
   * 
   * @param credential credentials for access
   * @return List of Region
   * @throws ProviderIntegrationException if something goes wrong
   */
  public List<Region> listRegions(Credential credential) throws ProviderIntegrationException {
    List<com.amazonaws.services.ec2.model.Region> listRegions = awsEc2.listRegions(credential);
    List<Region> list = new ArrayList<>();
    for (com.amazonaws.services.ec2.model.Region region : listRegions) {
      //
      List<AvailabilityZone> describeAvailabilityZones =
          awsEc2.listAvailabilityZones(credential, region.getRegionName());
      
      Region region2 = Region.builder()
                             .providerId(region.getRegionName())
                             .provider(Provider.AMAZON_AWS)
                             .name(region.getRegionName())
                             .zones(getZonesFromAvailabilityZones(describeAvailabilityZones))
                             .build();
      list.add(region2);
    }
    return list;
  }

  /**
   * Convert AvailabilityZone to Zone.
   * 
   * @param availabilityZones list
   * @return List of Zone
   */
  private List<Zone> getZonesFromAvailabilityZones(List<AvailabilityZone> availabilityZones) {
    List<Zone> list = new ArrayList<>();
    for (AvailabilityZone availabilityZone : availabilityZones) {
      Zone zone = Zone.builder()
                      .id(availabilityZone.getZoneName())
                      .name(availabilityZone.getZoneName())
                      .description(availabilityZone.getState())
                      .build();
      list.add(zone);
    }
    return list;
  }

}
