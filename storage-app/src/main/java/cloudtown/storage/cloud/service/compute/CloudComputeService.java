package cloudtown.storage.cloud.service.compute;

import cloudtown.storage.cloud.Provider;
import cloudtown.storage.cloud.provider.ProviderIntegrationException;
import cloudtown.storage.model.account.AccountException;
import cloudtown.storage.model.account.AccountService;
import cloudtown.storage.model.region.Region;
import java.util.Collections;
import java.util.List;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

/**
 * Cloud compute service.<br/>
 * Used to manipulate compute related info on the providers.
 * 
 * @author flaviolcastro
 *
 */
@Service
public class CloudComputeService {

  private final AccountService accountService;
  private final AwsComputeService awsComputeService;

  @Autowired
  public CloudComputeService(AccountService accountService, AwsComputeService awsRegionService) {
    this.accountService = accountService;
    this.awsComputeService = awsRegionService;
  }

  /**
   * List all regions by provider.
   * 
   * @param provider region
   * @return List of Region
   * @throws ProviderIntegrationException if integration goes wrong
   * @throws AccountException if User Account goes wrong
   */

  public List<Region> listRegions(String accountId, Provider provider)
      throws ProviderIntegrationException, AccountException {
    if (Provider.AMAZON_AWS == provider) {
      return this.awsComputeService.listRegions(accountService.getFirstCredential(accountId,false));
    }
    return Collections.emptyList();
  }

}
