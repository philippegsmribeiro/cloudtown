package cloudtown.storage.cloud.service.storage;

import cloudtown.storage.cloud.provider.ProviderIntegrationException;
import cloudtown.storage.cloud.provider.aws.AwsGlacier;
import cloudtown.storage.cloud.provider.aws.AwsS3;
import cloudtown.storage.model.account.Credential;
import cloudtown.storage.model.bucket.Bucket;
import cloudtown.storage.model.bucket.BucketDetail;
import cloudtown.storage.model.vault.Vault;
import com.amazonaws.services.glacier.model.DescribeVaultOutput;
import com.amazonaws.services.s3.model.AccessControlList;
import com.amazonaws.services.s3.model.Region;
import java.time.LocalDateTime;
import java.time.format.DateTimeFormatter;
import java.util.ArrayList;
import java.util.List;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

@Service
class AwsStorageService {

  private final AwsS3 awsS3;
  private final AwsGlacier awsGlacier;

  @Autowired
  public AwsStorageService(AwsS3 awsS3, AwsGlacier awsGlacier) {
    this.awsS3 = awsS3;
    this.awsGlacier = awsGlacier;
  }

  /**
   * List buckets from aws.
   * 
   * @param credential credentials for access
   * @param region of buckets
   * @param fullDetail Return more detail the buckets
   * @return List of {@link cloudtown.storage.model.bucket.Bucket}
   * @throws ProviderIntegrationException if integration goes wrong
   */
  public List<Bucket> listBuckets(Credential credential, String region, boolean fullDetail)
      throws ProviderIntegrationException {
    List<Bucket> list = new ArrayList<>();
    List<com.amazonaws.services.s3.model.Bucket> listBuckets =
        awsS3.listBuckets(credential, region);
    for (com.amazonaws.services.s3.model.Bucket bucketS3 : listBuckets) {
      Bucket bucket = Bucket.builder().name(bucketS3.getName())
          .creationDate(bucketS3.getCreationDate()).ownerId(bucketS3.getOwner().getId())
          .ownerName(bucketS3.getOwner().getDisplayName()).build();
      if (fullDetail) {
        String regionS3 = this.getRegionBucket(credential, region, bucketS3.getName());
        BucketDetail detail = BucketDetail.builder().region(regionS3)
            .accessControl(this.getAccessControlBucket(credential, regionS3, bucketS3.getName()))
            .build();
        bucket.setDetail(detail);
      }
      list.add(bucket);
    }
    return list;
  }

  /**
   * Get bucket permissions.
   * 
   * @param credential credentials for access
   * @param region of buckets
   * @param name of the bucket that will be searched as permissions.
   * @return AccessControlList of {@link com.amazonaws.services.s3.model.AccessControlList}
   * @throws ProviderIntegrationException if integration goes wrong.
   */
  public AccessControlList getAccessControlBucket(Credential credential, String region,
      String name) throws ProviderIntegrationException {
    return awsS3.getAccessControlBucket(credential, region, name);
  }

  /**
   * Get bucket region.
   * 
   * @param credential credentials for access
   * @param region of buckets
   * @param name of bucket
   * @return region of buckets
   * @throws ProviderIntegrationException if integration goes wrong.
   */
  public String getRegionBucket(Credential credential, String region, String name)
      throws ProviderIntegrationException {
    Region regionBucketS3 = Region.fromValue(awsS3.getRegionBucket(credential, region, name));
    return regionBucketS3.toAWSRegion().getName();
  }

  /**
   * Get bucket detail.
   * 
   * @param credential credentials for access
   * @param region of buckets
   * @param bucket will be searched as permissions and region.
   * @return bucket of {@link cloudtown.storage.model.bucket.Bucket}
   * @throws ProviderIntegrationException if integration goes wrong.
   */
  public BucketDetail getBucketDetail(Credential credential, String region, String name)
      throws ProviderIntegrationException {
    String regionS3 = this.getRegionBucket(credential, region, name);
    BucketDetail detail = BucketDetail.builder().region(regionS3)
        .accessControl(this.getAccessControlBucket(credential, regionS3, name)).build();
    return detail;
  }
  
  /**
   * List vaults from aws.
   * 
   * @param credential credentials for access
   * @param region of vaults
   * @return List of {@link Vault}
   * @throws ProviderIntegrationException if integration goes wrong
   */
  public List<Vault> listVaults(Credential credential, String region)
      throws ProviderIntegrationException {
    List<Vault> list = new ArrayList<>();
    List<DescribeVaultOutput> listVaults = awsGlacier.listVaults(credential, region);
    for (DescribeVaultOutput vaultOutput : listVaults) {
      Vault vault = Vault.builder().id(vaultOutput.getVaultName()).arn(vaultOutput.getVaultARN())
          .creationDate(formatIsoDateTime(vaultOutput.getCreationDate()))
          .lastInventoryDate(formatIsoDateTime(vaultOutput.getCreationDate()))
          .numberOfFiles(vaultOutput.getNumberOfArchives())
          .sizeInBytes(vaultOutput.getSizeInBytes()).build();
      list.add(vault);
    }
    return list;
  }

  /**
   * Get a LocalDateTime from an Iso date string.
   * @param dateString iso date
   * @return LocalDateTime
   */
  private static LocalDateTime formatIsoDateTime(String dateString) {
    if (dateString == null) {
      return null;
    }
    return LocalDateTime.parse(dateString, DateTimeFormatter.ISO_DATE_TIME);
  }


}
