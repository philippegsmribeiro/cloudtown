package cloudtown.storage.cloud.service.storage;

import cloudtown.storage.cloud.Provider;
import cloudtown.storage.cloud.provider.ProviderIntegrationException;
import cloudtown.storage.model.account.AccountException;
import cloudtown.storage.model.account.AccountService;
import cloudtown.storage.model.bucket.Bucket;
import cloudtown.storage.model.bucket.BucketDetail;
import cloudtown.storage.model.bucket.BucketDetailRequest;
import cloudtown.storage.model.bucket.BucketListRequest;
import cloudtown.storage.model.vault.Vault;
import cloudtown.storage.model.vault.VaultListRequest;
import com.amazonaws.services.s3.model.AccessControlList;
import java.util.Collections;
import java.util.List;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

/**
 * Cloud storage service.<br/>
 * Used to get storage related info on the providers.
 * 
 * @author flaviolcastro
 *
 */
@Service
public class CloudStorageService {

  private final AccountService accountService;
  private final AwsStorageService awsStorageService;

  @Autowired
  public CloudStorageService(AccountService accountService, AwsStorageService awsStorageService) {
    this.accountService = accountService;
    this.awsStorageService = awsStorageService;
  }

  /**
   * List buckets.
   * 
   * @param bucketListRequest request filter
   * @return List of {@link Bucket}
   * @throws ProviderIntegrationException if integration goes wrong
   * @throws AccountException if User Account goes wrong
   */
  public List<Bucket> listBuckets(BucketListRequest bucketListRequest)
      throws ProviderIntegrationException, AccountException {
    if (Provider.AMAZON_AWS == bucketListRequest.getProvider()) {
      return this.awsStorageService.listBuckets(
          accountService.getFirstCredential(bucketListRequest.getAccountId(), false),
          bucketListRequest.getRegion(), bucketListRequest.isFullDetail());
    }
    return Collections.emptyList();
  }

  /**
   * Get bucket permissions.
   * 
   * @param buckeDetailRequest of {@link cloudtown.storage.model.bucket.BucketDetailRequest}
   * @return AccessControlList of {@link com.amazonaws.services.s3.model.AccessControlList}
   * @throws ProviderIntegrationException if integration goes wrong.
   * @throws AccountException if User Account goes wrong
   */
  public AccessControlList getAccessControlBucket(BucketDetailRequest buckeDetailRequest)
      throws ProviderIntegrationException, AccountException {
    return awsStorageService.getAccessControlBucket(
        accountService.getFirstCredential(buckeDetailRequest.getAccountId(), false),
        buckeDetailRequest.getRegion(), buckeDetailRequest.getName());
  }

  /**
   * Get bucket region.
   * 
   * @param buckeDetailRequest of {@link cloudtown.storage.model.bucket.BucketDetailRequest}
   * @return region of bucket
   * @throws ProviderIntegrationException if integration goes wrong.
   * @throws AccountException if User Account goes wrong
   */
  public String getRegionBucket(BucketDetailRequest buckeDetailRequest)
      throws ProviderIntegrationException, AccountException {
    return awsStorageService.getRegionBucket(
        accountService.getFirstCredential(buckeDetailRequest.getAccountId(), false),
        buckeDetailRequest.getRegion(), buckeDetailRequest.getName());
  }

  /**
   * Get bucket detail.
   * 
   * @param buckeDetailRequest of {@link cloudtown.storage.model.bucket.BucketDetailRequest}
   * @return bucketDetail of {@link cloudtown.storage.model.bucket.BucketDetail}
   * @throws ProviderIntegrationException if integration goes wrong.
   * @throws AccountException if User Account goes wrong
   */
  public BucketDetail getBucketDetail(BucketDetailRequest buckeDetailRequest)
      throws ProviderIntegrationException, AccountException {
    return awsStorageService.getBucketDetail(
        accountService.getFirstCredential(buckeDetailRequest.getAccountId(), false),
        buckeDetailRequest.getRegion(), buckeDetailRequest.getName());
  }

  /**
   * List vaults.
   * 
   * @param vaultListRequest request filter
   * @return List of {@link Vault}
   * @throws ProviderIntegrationException if integration goes wrong *
   * @throws AccountException if it fails to get the credentials for the account
   */
  public List<Vault> listVaults(VaultListRequest vaultListRequest)
      throws ProviderIntegrationException, AccountException {
    if (Provider.AMAZON_AWS == vaultListRequest.getProvider()) {
      return this.awsStorageService.listVaults(
          accountService.getFirstCredential(vaultListRequest.getAccountId(), false),
          vaultListRequest.getRegion());
    }
    return Collections.emptyList();
  }

}
