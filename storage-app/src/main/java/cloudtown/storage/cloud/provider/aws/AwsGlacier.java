package cloudtown.storage.cloud.provider.aws;

import cloudtown.storage.cloud.provider.ProviderIntegrationException;
import cloudtown.storage.model.account.Credential;
import com.amazonaws.auth.AWSStaticCredentialsProvider;
import com.amazonaws.regions.Regions;
import com.amazonaws.services.glacier.AmazonGlacier;
import com.amazonaws.services.glacier.AmazonGlacierClient;
import com.amazonaws.services.glacier.model.DescribeVaultOutput;
import com.amazonaws.services.glacier.model.ListVaultsRequest;
import com.amazonaws.services.glacier.model.ListVaultsResult;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import lombok.extern.log4j.Log4j2;
import org.springframework.stereotype.Service;
import org.springframework.util.CollectionUtils;
import org.springframework.util.StringUtils;

/**
 * Service to manipulate the Glacier client.
 * 
 * @author flaviolcastro
 *
 */
@Service
@Log4j2
public class AwsGlacier extends AbstractAwsService {

  /** Map cache of clients. */
  private Map<String, AmazonGlacier> clientMap;

  public AwsGlacier() {
    clientMap = new HashMap<>();
  }

  /**
   * Return a AmazonGlacier client.
   * 
   * @param credential credentials for access
   * @param region of client
   * @return AmazonGlacier client
   * @throws ProviderIntegrationException if integration goes wrong
   */
  protected AmazonGlacier getClient(Credential credential, String region)
      throws ProviderIntegrationException {
    if (credential == null) {
      throw new ProviderIntegrationException("Error creating client - credential is null");
    }
    // try to get on the map
    String key = credential.hashCode() + region;

    if (clientMap.containsKey(key)) {
      // if found
      return clientMap.get(key);
    }

    //
    if (StringUtils.isEmpty(region)) {
      // if region not defined - set defaul
      region = Regions.US_EAST_1.getName();
    }

    try {
      // get credentials provider
      AWSStaticCredentialsProvider credentialsProvider = getCredentialsProvider(credential);

      // create the client
      AmazonGlacier amazonClient = AmazonGlacierClient.builder().withRegion(region)
          .withCredentials(credentialsProvider).build();

      // add to the map
      clientMap.put(key, amazonClient);
      return amazonClient;
    } catch (Exception e) {
      log.error("Error creating client!", e);
      throw new ProviderIntegrationException("Error creating client!", e);
    }
  }

  /**
   * List vaults from a region.
   * 
   * @param credential credentials for access
   * @param region of vaults
   * @return List of {@link DescribeVaultOutput}
   * @throws ProviderIntegrationException if integration goes wrong
   */
  public List<DescribeVaultOutput> listVaults(Credential credential, String region)
      throws ProviderIntegrationException {
    ListVaultsRequest listVaultsRequest = new ListVaultsRequest();
    //listVaultsRequest.setAccountId(credential.getAwsCredential().getAccountId());
    ListVaultsResult list = getClient(credential, region).listVaults(listVaultsRequest);
    List<DescribeVaultOutput> result = list.getVaultList();
    while (!StringUtils.isEmpty(list.getMarker())) {
      listVaultsRequest.setMarker(list.getMarker());
      list = getClient(credential, region).listVaults(listVaultsRequest);
      if (!CollectionUtils.isEmpty(list.getVaultList())) {
        result.addAll(list.getVaultList());
      }
    }
    return list.getVaultList();
  }
}
