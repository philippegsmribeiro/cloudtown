package cloudtown.storage.cloud.provider.aws;

import cloudtown.storage.cloud.provider.ProviderIntegrationException;
import cloudtown.storage.model.account.Credential;
import com.amazonaws.auth.AWSStaticCredentialsProvider;
import com.amazonaws.regions.Regions;
import com.amazonaws.services.s3.AmazonS3;
import com.amazonaws.services.s3.AmazonS3ClientBuilder;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.stream.Collectors;
import lombok.extern.log4j.Log4j2;
import org.springframework.stereotype.Service;
import org.springframework.util.StringUtils;

/**
 * Service to manipulate the EC2 client.
 * 
 * @author flaviolcastro
 *
 */
@Service
@Log4j2
public class AwsS3 extends AbstractAwsService {

  /** Map cache of clients. */
  private Map<String, AmazonS3> clientMap;

  public AwsS3() {
    clientMap = new HashMap<>();
  }

  /**
   * Return a S3 client.
   * 
   * @param credential credentials for access
   * @param region of bucket for access
   * @return AmazonS3 client
   * @throws ProviderIntegrationException if integration goes wrong
   */
  protected AmazonS3 getClient(Credential credential, String region)
      throws ProviderIntegrationException {
    if (credential == null) {
      throw new ProviderIntegrationException("Error creating client - credential is null");
    }
    // try to get on the map
    String key = credential.hashCode() + region;

    if (clientMap.containsKey(key)) {
      // if found
      return clientMap.get(key);
    }

    //
    if (StringUtils.isEmpty(region)) {
      // if region not defined - set defaul
      region = Regions.US_EAST_1.getName();
    }

    try {
      // get credentials provider
      AWSStaticCredentialsProvider credentialsProvider = getCredentialsProvider(credential);

      // create the client
      AmazonS3 amazonClient = AmazonS3ClientBuilder.standard().withRegion(region)
          .withCredentials(credentialsProvider).build();
      // add to the map
      clientMap.put(key, amazonClient);
      return amazonClient;
    } catch (Exception e) {
      log.error("Error creating client!", e);
      throw new ProviderIntegrationException("Error creating client!", e);
    }
  }

  /**
   * List buckets from a region.
   * 
   * @param credential credentials for access
   * @param region of bucket for access
   * @return List of {@link com.amazonaws.services.s3.model.Bucket}
   * @throws ProviderIntegrationException if integration goes wrong
   */
  public List<com.amazonaws.services.s3.model.Bucket> listBuckets(Credential credential,
      String region) throws ProviderIntegrationException {
    AmazonS3 client = getClient(credential, region);
    List<com.amazonaws.services.s3.model.Bucket> listBuckets = client.listBuckets();
    return listBuckets.stream().collect(Collectors.toList());
  }

  /**
   * Get bucket permissions.
   * 
   * @param credential credentials for access
   * @param region of bucket for access
   * @param name of the bucket that will be searched as permissions.
   * @return accessControlList of {@link com.amazonaws.services.s3.model.AccessControlList}
   * @throws ProviderIntegrationException if integration goes wrong
   */
  public com.amazonaws.services.s3.model.AccessControlList getAccessControlBucket(
      Credential credential, String region, String name) throws ProviderIntegrationException {
    com.amazonaws.services.s3.model.AccessControlList accessControlList =
        getClient(credential, region).getBucketAcl(name);
    return accessControlList;
  }

  /**
   * Get bucket region.
   * 
   * @param credential credential credentials for access
   * @param region of buckets
   * @param name of bucket
   * @return List of {@link com.amazonaws.services.s3.model.Bucket}
   * @throws ProviderIntegrationException if integration goes wrong
   */
  public String getRegionBucket(Credential credential, String region, String name)
      throws ProviderIntegrationException {
    return getClient(credential, region).getBucketLocation(name);
  }

}
