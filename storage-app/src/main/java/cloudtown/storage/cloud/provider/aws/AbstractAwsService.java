package cloudtown.storage.cloud.provider.aws;

import cloudtown.storage.model.account.Credential;
import com.amazonaws.auth.AWSStaticCredentialsProvider;
import com.amazonaws.auth.BasicAWSCredentials;

/**
 * Abstract Aws Service class with common methods.
 * 
 * @author flaviolcastro
 *
 */
public abstract class AbstractAwsService {

  /**
   * Get Credentials Provider for credential.
   * 
   * @param credential credentials for access
   * @return CredentialProvider
   */
  protected static AWSStaticCredentialsProvider getCredentialsProvider(Credential credential) {
    String accessKey = credential.getAwsCredential().getAccessKey();
    String secretKey = credential.getAwsCredential().getSecretKey();
    BasicAWSCredentials credentials = new BasicAWSCredentials(accessKey, secretKey);
    AWSStaticCredentialsProvider provider = new AWSStaticCredentialsProvider(credentials);
    return provider;
  }
}
