package cloudtown.storage.cloud.provider;

/**
 * Exception when creating/executing provider client connection.
 * 
 * @author flaviolcastro
 *
 */
public class ProviderIntegrationException extends Exception {

  private static final long serialVersionUID = 3476603995757308447L;

  public ProviderIntegrationException(String message) {
    super(message);
  }
  
  public ProviderIntegrationException(String message, Throwable cause) {
    super(message, cause);
  }

}
