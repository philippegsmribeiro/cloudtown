package cloudtown.storage.cloud.provider.aws;

import cloudtown.storage.cloud.provider.ProviderIntegrationException;
import cloudtown.storage.model.account.Credential;
import com.amazonaws.auth.AWSStaticCredentialsProvider;
import com.amazonaws.regions.Regions;
import com.amazonaws.services.ec2.AmazonEC2;
import com.amazonaws.services.ec2.AmazonEC2ClientBuilder;
import com.amazonaws.services.ec2.model.AvailabilityZone;
import com.amazonaws.services.ec2.model.DescribeAvailabilityZonesResult;
import com.amazonaws.services.ec2.model.DescribeRegionsResult;
import com.amazonaws.services.ec2.model.Region;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import lombok.extern.log4j.Log4j2;
import org.springframework.stereotype.Service;
import org.springframework.util.StringUtils;

/**
 * Service to manipulate the EC2 client.
 * 
 * @author flaviolcastro
 *
 */
@Service
@Log4j2
public class AwsEc2 extends AbstractAwsService {

  /** Map cache of clients. */
  private final Map<String, AmazonEC2> clientMap;
  
  public AwsEc2() {
    clientMap = new HashMap<>();
  }
  
  /**
   * Return a EC2 client.
   * 
   * @param credential credentials for access
   * @param region of client
   * @return AmazonEC2 client
   * @throws ProviderIntegrationException if integration goes wrong
   */
  protected AmazonEC2 getClient(Credential credential, String region)
      throws ProviderIntegrationException {
    if (credential == null) {
      throw new ProviderIntegrationException("Error creating client - credential is null");
    }
    // try to get on the map
    String key = credential.hashCode() + region;
    
    if (clientMap.containsKey(key)) {
      // if found
      return clientMap.get(key);
    }
    
    //
    if (StringUtils.isEmpty(region)) {
      // if region not defined - set defaul
      region = Regions.US_EAST_1.getName();
    }

    try {
      // get credentials provider
      AWSStaticCredentialsProvider credentialsProvider = getCredentialsProvider(credential);
      
      // create the client
      AmazonEC2 amazonEc2Client = AmazonEC2ClientBuilder.standard()
          .withRegion(region)
          .withCredentials(credentialsProvider)
          .build();
      // add to the map
      clientMap.put(key, amazonEc2Client);
      return amazonEc2Client;
    } catch (Exception e) {
      log.error("Error creating client!", e);
      throw new ProviderIntegrationException("Error creating client!", e);
    }
  }
  
  /**
   * List all available regions.
   * @param credential credentials for access
   * @return List of {@link Region}
   * @throws ProviderIntegrationException if integration goes wrong
   */
  public List<Region> listRegions(Credential credential) throws ProviderIntegrationException {
    DescribeRegionsResult describeRegions = getClient(credential, null).describeRegions();
    return describeRegions.getRegions();
  }

  /**
   * List all available availability zones.
   * @param credential credentials for access
   * @param region region name
   * @return List of {@link AvailabilityZone}
   * @throws ProviderIntegrationException if integration goes wrong
   */
  public List<AvailabilityZone> listAvailabilityZones(Credential credential, String region)
      throws ProviderIntegrationException {
    DescribeAvailabilityZonesResult describeAvailabilityZones =
        getClient(credential, region).describeAvailabilityZones();
    return describeAvailabilityZones.getAvailabilityZones();
  }

}
