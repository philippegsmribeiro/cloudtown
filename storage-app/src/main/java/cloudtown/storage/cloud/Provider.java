package cloudtown.storage.cloud;

/**
 * Enum for the cloud providers.
 */
public enum Provider {

  AMAZON_AWS("Amazon AWS");

  private String shortName;

  Provider(String shortName) {
    this.shortName = shortName;
  }

  public String getShortName() {
    return shortName;
  }

  /**
   * Method that return the Provider by shortName.
   * 
   * @param name The name of provider.
   * @return Provider associated with the name.
   */
  public static final Provider getProviderByName(String name) {
    for (Provider provider : Provider.values()) {
      if (provider.getShortName().equals(name)) {
        return provider;
      }
    }
    return null;
  }
}
