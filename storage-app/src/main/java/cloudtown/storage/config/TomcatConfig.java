package cloudtown.storage.config;

import java.io.File;
import java.nio.file.Files;
import java.nio.file.StandardCopyOption;
import lombok.extern.log4j.Log4j2;
import org.apache.catalina.connector.Connector;
import org.apache.coyote.http11.AbstractHttp11Protocol;
import org.apache.coyote.http11.Http11NioProtocol;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.boot.web.embedded.tomcat.TomcatConnectorCustomizer;
import org.springframework.boot.web.embedded.tomcat.TomcatServletWebServerFactory;
import org.springframework.boot.web.servlet.server.ServletWebServerFactory;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.http.MediaType;

/**
 * Tomcat config.<br/>
 * Start the app using tomcat as container and enables the websocket support and ssl connector.
 */
@Configuration
@Log4j2
public class TomcatConfig {

  private static final int COMPRESSION_MIN_SIZE = 256;
  private static final String COMPRESSION_ON = "on";
  private static final String HTTPS = "https";

  @Value("${ssl.enabled}")
  boolean sslEnabled;

  @Value("${ssl.port}")
  Integer port;

  @Value("${ssl.keystore}")
  String keyStore;

  @Value("${ssl.keystore-password}")
  String keyStorePassword;

  @Value("${ssl.keyAlias}")
  String keyAlias;

  @Value("${ssl.keyStoreType}")
  String keyStoreType;

  @Value("${server.port}")
  Integer tomcatPort;

  /**
   * Tomcat Container.
   *
   * @return a new embedded container factory
   */
  @Bean
  public ServletWebServerFactory servletContainer() {
    TomcatServletWebServerFactory tomcat = new TomcatServletWebServerFactory(tomcatPort);
    tomcat.addConnectorCustomizers(gettomcatConnectorCustomizer());

    if (sslEnabled) {
      // adds the ssl connector
      tomcat.addAdditionalTomcatConnectors(createSslConnector());
    }
    return tomcat;
  }

  /**
   * Obtain a Tomcat connector customized.
   *
   * @return a new customized connector
   */
  private TomcatConnectorCustomizer gettomcatConnectorCustomizer() {
    return connector -> configCompression(
        (AbstractHttp11Protocol<?>) connector.getProtocolHandler());
  }

  /**
   * Enabling SSL.
   *
   * @return a new SSL connector
   */
  private Connector createSslConnector() {
    Connector connector = new Connector("org.apache.coyote.http11.Http11NioProtocol");
    connector.setScheme(HTTPS);
    connector.setSecure(true);
    connector.setPort(port);
    
    Http11NioProtocol protocol = (Http11NioProtocol) connector.getProtocolHandler();
    protocol.setSSLEnabled(true);
    protocol.setKeystoreFile(getKeystoreFile());
    protocol.setKeystorePass(keyStorePassword);
    protocol.setKeyAlias(keyAlias);
    // set compression
    configCompression(protocol);
    return connector;
  }

  /**
   * Config the compression over the http.
   *
   * @param httpProtocol a new HTTP protocol
   */
  private void configCompression(AbstractHttp11Protocol<?> httpProtocol) {
    httpProtocol.setCompression(COMPRESSION_ON);
    httpProtocol.setCompressionMinSize(COMPRESSION_MIN_SIZE);
    String mimeTypes = httpProtocol.getCompressibleMimeType();
    String mimeTypesWithJson = String.format("%s,%s", mimeTypes, MediaType.APPLICATION_JSON_VALUE);
    httpProtocol.setCompressibleMimeType(mimeTypesWithJson);
  }

  /**
   * Generates a reachable file to the container.
   *
   * @return the key store
   */
  private String getKeystoreFile() {
    try {
      File file = File.createTempFile("keystore", ".key");
      Files.copy(this.getClass().getClassLoader().getResourceAsStream(keyStore), file.toPath(),
          StandardCopyOption.REPLACE_EXISTING);
      return file.getAbsolutePath();
    } catch (Exception e) {
      log.error("Error getting keystore", e);
    }
    return null;
  }
}
