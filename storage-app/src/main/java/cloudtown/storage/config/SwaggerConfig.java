package cloudtown.storage.config;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;
import java.util.Map;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import springfox.documentation.builders.ApiInfoBuilder;
import springfox.documentation.builders.PathSelectors;
import springfox.documentation.builders.RequestHandlerSelectors;
import springfox.documentation.service.ApiInfo;
import springfox.documentation.service.AuthorizationScope;
import springfox.documentation.service.GrantType;
import springfox.documentation.service.OAuth;
import springfox.documentation.service.ResourceOwnerPasswordCredentialsGrant;
import springfox.documentation.service.SecurityReference;
import springfox.documentation.spi.DocumentationType;
import springfox.documentation.spi.service.contexts.SecurityContext;
import springfox.documentation.spring.web.plugins.Docket;
import springfox.documentation.swagger.web.SecurityConfiguration;
import springfox.documentation.swagger2.annotations.EnableSwagger2;

/**
 * Swagger Configuration class.
 * 
 * @author flaviolcastro
 *
 */
@Configuration
@EnableSwagger2
public class SwaggerConfig {

  private static final String ADMIN = "Admin";
  private static final String APP_PACKAGE = "cloudtown";

  /**
   * Enable the Swagger-UI.
   *
   * @return a Swagger docket
   */
  @Bean
  public Docket applicationApi() {
    return new Docket(DocumentationType.SWAGGER_2).select()
        .apis(RequestHandlerSelectors.basePackage(APP_PACKAGE))
        .paths(PathSelectors.any())
        .build()
        .apiInfo(apiInfo(""))
        .securitySchemes(Arrays.asList(securitySchema()))
        .securityContexts(Arrays.asList(securityContext()))
        .pathMapping("/")
        .groupName(APP_PACKAGE);
  }

  @Value("${management.context-path:admin}")
  String adminPath;
  @Value("${app.version:SNAPSHOT}")
  String version;

  /**
   * Swagger admin.
   * 
   * @return a Swagger docket
   */
  @Bean
  public Docket adminApi() {
    return new Docket(DocumentationType.SWAGGER_2).select()
        .apis(RequestHandlerSelectors.any())
        .paths(PathSelectors.ant(adminPath + "/*"))
        .build()
        .apiInfo(apiInfo(String.format(" -%s", ADMIN)))
        .securitySchemes(Arrays.asList(securitySchema()))
        .securityContexts(Arrays.asList(securityContext()))
        .pathMapping("/")
        .groupName(APP_PACKAGE + " " + ADMIN);
  }

  /**
   * Define the API info for the Swagger API.
   *
   * @return a new ApiInfo
   */
  private ApiInfo apiInfo(String description) {
    return new ApiInfoBuilder().title("Cloudtown Storage App")
        .description(String.format("Cloudtown Storage REST API %s", description))
        .license("private and proprietary")
        .version(version)
        .build();
  }

  /**
   * Define Oauth schema.
   * 
   * @return
   */
  private OAuth securitySchema() {
    List<AuthorizationScope> authorizationScopeList = new ArrayList<AuthorizationScope>();
    authorizationScopeList.add(new AuthorizationScope("read", "read all"));
    authorizationScopeList.add(new AuthorizationScope("write", "access all"));
    List<GrantType> grantTypes = new ArrayList<GrantType>();
    GrantType passwordCredentialsGrant = new ResourceOwnerPasswordCredentialsGrant("/oauth/token");
    grantTypes.add(passwordCredentialsGrant);
    return new OAuth("oauth2", authorizationScopeList, grantTypes);
  }

  /**
   * Obtain the security context for Swagger.
   *
   * @return the security context
   */
  private SecurityContext securityContext() {
    return SecurityContext.builder().securityReferences(defaultAuth()).build();
  }

  @Value("${auth.app.client.id}")
  private String appClientId;
  @Value("${auth.app.client.secret}")
  private String appClientSecret;
  @Value("${auth.resource.id}")
  private String resourceId;

  /**
   * Security config.
   * @return SecurityConfiguration
   */
  @Bean
  public SecurityConfiguration security() {
    String clientId = appClientId;
    String clientSecret = appClientSecret;
    String realm = "";
    String appName = "";
    String scopeSeparator = "";
    Map<String, Object> additionalQueryStringParams = null;
    Boolean useBasicAuthenticationWithAccessCodeGrant = false;
    return new SecurityConfiguration(clientId, clientSecret, realm, appName, scopeSeparator,
        additionalQueryStringParams, useBasicAuthenticationWithAccessCodeGrant);
  }

  /**
   * Obtain the default authentication for the swagger ui.
   *
   * @return a list of security references
   */
  private List<SecurityReference> defaultAuth() {
    AuthorizationScope authorizationScope = new AuthorizationScope("global", "accessEverything");
    AuthorizationScope[] authorizationScopes = new AuthorizationScope[1];
    authorizationScopes[0] = authorizationScope;
    return Arrays.asList(new SecurityReference("oauth2", authorizationScopes));
  }
}
