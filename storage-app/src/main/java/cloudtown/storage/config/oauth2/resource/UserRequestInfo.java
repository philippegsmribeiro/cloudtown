package cloudtown.storage.config.oauth2.resource;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.security.oauth2.provider.token.TokenStore;
import org.springframework.stereotype.Service;

/**
 * Auxiliary class to get information about the logged user.
 *
 */
@Service
public class UserRequestInfo {

  private TokenStore tokenStore;

  @Autowired
  public UserRequestInfo(TokenStore tokenStore) {
    this.setTokenStore(tokenStore);
  }

  /**
   * Get the username of the logged user.
   * 
   * @return user username
   */
  public String getUserName() {
    Authentication authentication = SecurityContextHolder.getContext().getAuthentication();
    if (authentication != null) {
      return (String) authentication.getPrincipal();
    }
    return null;
  }

  public TokenStore getTokenStore() {
    return tokenStore;
  }

  public void setTokenStore(TokenStore tokenStore) {
    this.tokenStore = tokenStore;
  }
}
