package cloudtown.storage.config.oauth2.resource;

import cloudtown.storage.utils.ProfileUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.context.annotation.Configuration;
import org.springframework.security.config.annotation.web.builders.HttpSecurity;
import org.springframework.security.config.http.SessionCreationPolicy;
import org.springframework.security.oauth2.config.annotation.web.configuration.EnableResourceServer;
import org.springframework.security.oauth2.config.annotation.web.configuration.ResourceServerConfigurerAdapter;
import org.springframework.security.oauth2.config.annotation.web.configurers.ResourceServerSecurityConfigurer;
import org.springframework.security.oauth2.provider.token.DefaultTokenServices;

/**
 * OAuth2 Resource server config.
 */
@Configuration
@EnableResourceServer
public class OAuth2ResourceServerConfig extends ResourceServerConfigurerAdapter {

  @Autowired
  DefaultTokenServices defaultTokenServices;

  @Value("${auth.resource.id}")
  private String resourceId;

  @Override
  public void configure(ResourceServerSecurityConfigurer config) {
    config.tokenServices(defaultTokenServices).resourceId(resourceId);
  }

  @Autowired
  private ProfileUtils profileUtils;

  @Override
  public void configure(HttpSecurity http) throws Exception {
    // disable csrf
    http = http.csrf().disable().exceptionHandling().and();

    // set session stateless
    http = http.sessionManagement().sessionCreationPolicy(SessionCreationPolicy.STATELESS).and();

    // free endpoints
    http = http.authorizeRequests().antMatchers("/oauth/*", "/test/*").permitAll().and();

    // allow access to swagger for NOT production
    if (!profileUtils.isProduction()) {
      http = http.authorizeRequests()
          .antMatchers("/v2/api-docs", "/configuration/ui", "/swagger-resources",
              "/configuration/security", "/swagger-ui.html", "/webjars/**",
              "/swagger-resources/configuration/ui", "/swagger-ui.html",
              "/swagger-resources/configuration/security")
          .permitAll().and();
    }

    // permit just authenticated access
    http.authorizeRequests().antMatchers("/**").authenticated();
    return;
  }
}
