package cloudtown.storage.config.oauth2.auth;

import cloudtown.storage.model.user.AppClient;
import cloudtown.storage.model.user.AppClientService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.context.annotation.Primary;
import org.springframework.security.authentication.AuthenticationManager;
import org.springframework.security.oauth2.config.annotation.configurers.ClientDetailsServiceConfigurer;
import org.springframework.security.oauth2.config.annotation.web.configuration.AuthorizationServerConfigurerAdapter;
import org.springframework.security.oauth2.config.annotation.web.configuration.EnableAuthorizationServer;
import org.springframework.security.oauth2.config.annotation.web.configurers.AuthorizationServerEndpointsConfigurer;
import org.springframework.security.oauth2.provider.ClientDetails;
import org.springframework.security.oauth2.provider.ClientDetailsService;
import org.springframework.security.oauth2.provider.ClientRegistrationException;
import org.springframework.security.oauth2.provider.client.BaseClientDetails;
import org.springframework.security.oauth2.provider.token.DefaultTokenServices;
import org.springframework.security.oauth2.provider.token.TokenStore;
import org.springframework.security.oauth2.provider.token.store.JwtAccessTokenConverter;
import org.springframework.security.oauth2.provider.token.store.JwtTokenStore;

/**
 * OAuth2 Authorization Server.<br/>
 * For this version, the auth server will be sitting in the same app. However, it will be moved to
 * its own microservice in the future.
 */
@Configuration
@EnableAuthorizationServer
public class OAuth2AuthorizationServerConfig extends AuthorizationServerConfigurerAdapter {

  static final String NOOP = "{noop}";

  @Autowired
  @Qualifier("authenticationManagerBean")
  private AuthenticationManager authenticationManager;

  @Autowired
  private AppClientService appClientService;

  @Value("${auth.jwt.signing.key}")
  private String signingKey;
  @Value("${auth.jwt.access.token.validity_seconds:0}")
  private Integer accessTokenValiditySeconds;
  @Value("${auth.jwt.refresh.token.validity_seconds:0}")
  private Integer refreshTokenValiditySeconds;

  @Override
  public void configure(ClientDetailsServiceConfigurer clients) throws Exception {
    clients.withClientDetails(new ClientDetailsService() {

      @Override
      public ClientDetails loadClientByClientId(String clientId)
          throws ClientRegistrationException {

        AppClient findByClientId = appClientService.findByClientId(clientId);
        if (findByClientId != null) {
          BaseClientDetails baseClientDetails = new BaseClientDetails();
          baseClientDetails.setClientId(clientId);
          baseClientDetails.setClientSecret(NOOP + findByClientId.getClientSecret());
          baseClientDetails.setAuthorizedGrantTypes(findByClientId.getAuthorityGrantTypes());
          baseClientDetails.setAuthorities(findByClientId.getAuthorities());
          baseClientDetails.setResourceIds(findByClientId.getResourceIds());
          return baseClientDetails;
        }
        return null;
      }
    });
  }

  @Override
  public void configure(AuthorizationServerEndpointsConfigurer endpoints) throws Exception {
    endpoints.tokenStore(tokenStore()).accessTokenConverter(accessTokenConverter())
        .authenticationManager((auth) -> authenticationManager.authenticate(auth));
  }

  @Bean
  public TokenStore tokenStore() {
    return new JwtTokenStore(accessTokenConverter());
  }

  /**
   * JwtAccessTokenConverter.<br/>
   * Using signingkey for now, as we have all on the same server. Must change for keypair when the
   * services get splitted.
   * 
   * @return JwtAccessTokenConverter
   */
  @Bean
  public JwtAccessTokenConverter accessTokenConverter() {
    JwtAccessTokenConverter converter = new JwtAccessTokenConverter();
    converter.setSigningKey(signingKey);
    return converter;
  }

  /**
   * Token services.
   * 
   * @return DefaultTokenServices
   */
  @Bean
  @Primary
  public DefaultTokenServices tokenServices() {
    DefaultTokenServices defaultTokenServices = new DefaultTokenServices();
    defaultTokenServices.setTokenStore(tokenStore());
    defaultTokenServices.setSupportRefreshToken(true);
    defaultTokenServices.setAccessTokenValiditySeconds(accessTokenValiditySeconds);
    defaultTokenServices.setRefreshTokenValiditySeconds(refreshTokenValiditySeconds);
    return defaultTokenServices;
  }
}
