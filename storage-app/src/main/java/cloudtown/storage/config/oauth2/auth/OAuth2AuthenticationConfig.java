package cloudtown.storage.config.oauth2.auth;

import cloudtown.storage.model.user.User;
import cloudtown.storage.model.user.UserService;
import cloudtown.storage.utils.CryptoService;
import java.util.Collection;
import java.util.Collections;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.security.config.annotation.authentication.builders.AuthenticationManagerBuilder;
import org.springframework.security.config.annotation.authentication.configuration.GlobalAuthenticationConfigurerAdapter;
import org.springframework.security.core.GrantedAuthority;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.security.core.userdetails.UserDetailsService;
import org.springframework.security.core.userdetails.UsernameNotFoundException;

@Configuration
public class OAuth2AuthenticationConfig extends GlobalAuthenticationConfigurerAdapter {

  @Autowired
  private UserService userService;

  @Autowired
  private CryptoService cryptoService;

  @Override
  public void init(AuthenticationManagerBuilder auth) throws Exception {
    auth.userDetailsService(userDetailsService());
  }

  @Bean
  protected UserDetailsService userDetailsService() {
    return new UserDetailsService() {

      @Override
      public UserDetails loadUserByUsername(String username) throws UsernameNotFoundException {
        // find the user
        org.springframework.security.core.userdetails.User user = null;
        try {
          User findByLoginName = userService.findByLoginName(username);
          // TODO: add kms
          String password = OAuth2AuthorizationServerConfig.NOOP
              + cryptoService.decrypt(findByLoginName.getPassword());
          // TODO: add those validations
          boolean enabled = true;
          boolean accountNonExpired = true;
          boolean credentialsNonExpired = true;
          boolean accountNonLocked = true;
          // authorities
          Collection<? extends GrantedAuthority> authorities = Collections.emptyList();
          user = new org.springframework.security.core.userdetails.User(username, password, enabled,
              accountNonExpired, credentialsNonExpired, accountNonLocked, authorities);
        } catch (Exception e) {
          throw new UsernameNotFoundException(username);
        }
        return user;
      }
    };
  }
}
