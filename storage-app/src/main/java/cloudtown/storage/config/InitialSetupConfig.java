package cloudtown.storage.config;

import cloudtown.storage.cloud.Provider;
import cloudtown.storage.model.account.Account;
import cloudtown.storage.model.account.AccountService;
import cloudtown.storage.model.account.AwsCredential;
import cloudtown.storage.model.account.Credential;
import cloudtown.storage.model.user.AppClient;
import cloudtown.storage.model.user.AppClientService;
import cloudtown.storage.model.user.User;
import cloudtown.storage.model.user.UserService;
import java.util.Arrays;
import java.util.List;
import lombok.extern.log4j.Log4j2;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.boot.CommandLineRunner;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.security.core.authority.AuthorityUtils;

/**
 * Initial setup for the application.
 * 
 * @author flaviolcastro
 *
 */
@Log4j2
@Configuration
public class InitialSetupConfig {

  @Value("${aws.account.id}")
  private String accountId;
  @Value("${aws.account.email}")
  private String accountEmail;
  @Value("${aws.account.accesskey}")
  private String accessKey;
  @Value("${aws.account.secretkey}")
  private String secretKey;

  @Autowired
  private AccountService accountService;

  @Value("${auth.app.client.id}")
  private String appClientId;
  @Value("${auth.app.client.secret}")
  private String appClientSecret;

  @Autowired
  private AppClientService appClientService;

  @Value("${admin.user.loginname}")
  private String adminUserLoginName;
  @Value("${admin.user.password}")
  private String adminUserPassword;
  @Value("${admin.user.email}")
  private String adminUserEmail;

  @Autowired
  private UserService userService;

  /**
   * Create the admin user.<br/>
   * Just add if is not set yet.
   * 
   * @return CommandLineRunner
   */
  @Bean
  public CommandLineRunner createAdminUser() {
    return args -> {
      log.info("Setting up admin user!");
      try {
        userService.findByLoginName(adminUserLoginName);
      } catch (Exception e) {
        User adminUser = User.builder().login(adminUserLoginName).password(adminUserPassword)
            .email(adminUserEmail).build();
        userService.save(adminUser);
      }
    };
  }

  /**
   * Create an AppClient for oauth2.<br/>
   * Just add if is not set yet.
   * 
   * @return CommandLineRunner
   */
  @Bean
  public CommandLineRunner createAppClient() {
    return args -> {

      log.info("Setting up app client!");

      if (appClientService.findByClientId(appClientId) == null) {

        List<String> types =
            Arrays.asList("implicit", "refresh_token", "password", "authorization_code");

        AppClient cashpieClient =
            AppClient.builder().clientId(appClientId).clientSecret(appClientSecret)
                .authorityGrantTypes(types).authorities(AuthorityUtils.createAuthorityList("ALL"))
                .scopes(Arrays.asList("write")).resourceIds(Arrays.asList(appClientId)).build();

        appClientService.save(cashpieClient);
      }
    };
  }


  /**
   * Create an account/credentials for testing.<br/>
   * Just add if is not set yet.
   * 
   * @return CommandLineRunner
   */
  @Bean
  public CommandLineRunner createTestAccount() {
    return args -> {

      log.info("Setting up test account!");
      Account account = accountService.retrieveById(accountId, true);
      if (account == null) {
        log.info("Account not found! Adding the test account!");
        // aws credential
        AwsCredential awsCredential =
            AwsCredential.builder().accessKey(accessKey).secretKey(secretKey).build();
        // credential
        Credential credential = Credential.builder().awsCredential(awsCredential).build();

        account = Account.builder().id(accountId).provider(Provider.AMAZON_AWS)
            .credentials(Arrays.asList(credential)).build();

        accountService.save(account);
      }
    };
  }
}
