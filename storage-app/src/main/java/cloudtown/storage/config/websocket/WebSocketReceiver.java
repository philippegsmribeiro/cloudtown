package cloudtown.storage.config.websocket;

import cloudtown.storage.model.example.Example;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.messaging.handler.annotation.MessageMapping;
import org.springframework.stereotype.Controller;

/**
 * Messenger for websockets.<br/>
 * That is just for tests, we shouldn't be using it, as we are using websockets just to send
 * messages.
 */
@Controller
public class WebSocketReceiver {

  public static final String TEST = "/test";

  private final WebSocketSender webSocketSender;

  @Autowired
  public WebSocketReceiver(WebSocketSender webSocketSender) {
    this.webSocketSender = webSocketSender;
  }

  /**
   * That is just for tests, we shouldn't be using it, as we are using websockets just to send
   * messages.
   * 
   * @param example example
   */
  @MessageMapping(TEST)
  public void test(Example example) {
    webSocketSender.sendMessageToTopic(WebSocketChannel.TEST, example);
  }

}
