package cloudtown.storage.config.websocket;

import cloudtown.storage.config.oauth2.resource.UserRequestInfo;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.messaging.simp.SimpMessagingTemplate;
import org.springframework.stereotype.Service;
import org.springframework.util.StringUtils;

/**
 * WebSocket sender service.
 *
 */
@Service
public class WebSocketSender {

  private SimpMessagingTemplate template;
  private UserRequestInfo userRequestInfo;

  @Autowired
  public WebSocketSender(SimpMessagingTemplate template, UserRequestInfo userRequestInfo) {
    this.template = template;
    this.userRequestInfo = userRequestInfo;
  }

  /**
   * Send a message/object to a topic.<br/>
   * All connected clients on that topic will receive it.
   * 
   * @param topic channel
   * @param message to be sent
   */
  public void sendMessageToTopic(WebSocketChannel topic, Object message) {
    if (StringUtils.isEmpty(topic)) {
      this.template.convertAndSend(message);
    } else {
      this.template.convertAndSend(topic.getOutbound(), message);
    }
  }

  /**
   * Send a message/object to a topic for an user.<br/>
   * Just the specific user will receive it.
   * 
   * @param user to receive the message
   * @param topic channel
   * @param message to be sent
   */
  public void sendMessageToUserAndTopic(String user, WebSocketChannel topic, Object message) {
    if (StringUtils.isEmpty(user)) {
      // send to the logged one
      user = userRequestInfo.getUserName();
    }
    this.template.convertAndSendToUser(user, topic.getOutbound(), message);
  }
}
