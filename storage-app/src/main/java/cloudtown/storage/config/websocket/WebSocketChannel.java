package cloudtown.storage.config.websocket;

import cloudtown.storage.config.WebSocketConfig;

/**
 * WebSocket Receiver Channel.
 * Topic to receive a message from the front end.
 */
public enum WebSocketChannel {
  
  TEST(WebSocketReceiver.TEST);
  
  private String topic;
  
  private WebSocketChannel(String topic) {
    this.topic = topic;
  }
  
  /**
   * Get inbound channel name.<br/>
   * Use this to send something to the backend.
   * @return
   */
  public String getInbound() {
    return WebSocketConfig.WS_APP + topic;
  }
  
  /**
   * Get outbound channel name.<br/>
   * Use this to receive something from the backend.
   * @return
   */
  public String getOutbound() {
    return WebSocketConfig.WS_TOPIC + topic;
  }
  
}
