package cloudtown.storage.api;

import cloudtown.storage.model.common.PageResponse;
import cloudtown.storage.model.example.Example;
import cloudtown.storage.model.example.ExampleService;
import lombok.extern.log4j.Log4j2;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageRequest;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

/**
 * Example Controller.<br>
 * 
 * @author flaviolcastro
 *
 */
@RestController
@Log4j2
@RequestMapping(value = ExampleController.API)
public class ExampleController {

  protected static final String ENDPOINT_DESCRIPTION = "Example endpoint";

  protected static final String API = "/example";
  protected static final String DESCRIBE = "/{id}";
  protected static final String LIST = "/list/{page}/{pageSize}";
  protected static final String SAVE = "/";
  protected static final String DELETE = "/{id}";

  private final ExampleService exampleService;


  /**
   * Autowired constructor.<br>
   * Allows to pass the mocked service for tests.
   * 
   * @param exampleService Service
   */
  @Autowired
  public ExampleController(ExampleService exampleService) {
    this.exampleService = exampleService;
  }

  /**
   * Get the / endpoint.
   * 
   * @return description of the endpoint
   */
  @GetMapping
  public ResponseEntity<String> get() {
    return ResponseEntity.ok(ENDPOINT_DESCRIPTION);
  }

  /**
   * Get example by Id.
   * 
   * @param id of example
   * @return Example if found, null otherwise
   */
  @GetMapping(DESCRIBE)
  public ResponseEntity<Example> getOne(@PathVariable String id) {
    try {
      Example example = exampleService.retrieveById(id);
      return ResponseEntity.ok(example);
    } catch (Exception e) {
      log.error(e);
      return ResponseEntity.status(HttpStatus.INTERNAL_SERVER_ERROR).build();
    }
  }

  /**
   * List examples with pagination.
   * 
   * @param page count
   * @param pageSize register per page
   * @return Page of Examples
   */
  @GetMapping(LIST)
  public ResponseEntity<PageResponse<Example>> list(@PathVariable Integer page,
      @PathVariable Integer pageSize) {
    try {
      Page<Example> pageResult = exampleService.retrieveAll(PageRequest.of(page, pageSize));
      return ResponseEntity.ok(PageResponse.fromPage(pageResult));
    } catch (Exception e) {
      log.error(e);
      return ResponseEntity.status(HttpStatus.INTERNAL_SERVER_ERROR).build();
    }
  }

  /**
   * Save a example.
   * 
   * @param example to be saved
   * @return saved example
   */
  @PostMapping(SAVE)
  public ResponseEntity<Example> save(@RequestBody Example example) {
    try {
      Example saved = exampleService.save(example);
      return ResponseEntity.ok(saved);
    } catch (Exception e) {
      log.error(e);
      return ResponseEntity.status(HttpStatus.INTERNAL_SERVER_ERROR).build();
    }
  }

  /**
   * Delete an example by id.
   * 
   * @param id of example
   * @return true if deleted, false otherwise
   */
  @DeleteMapping(DELETE)
  public ResponseEntity<Void> delete(@PathVariable String id) {
    try {
      Boolean deleted = exampleService.deleteById(id);
      if (deleted) {
        return ResponseEntity.ok().build();
      }
      return ResponseEntity.status(HttpStatus.INTERNAL_SERVER_ERROR).build();
    } catch (Exception e) {
      log.error(e);
      return ResponseEntity.status(HttpStatus.INTERNAL_SERVER_ERROR).build();
    }
  }

}
