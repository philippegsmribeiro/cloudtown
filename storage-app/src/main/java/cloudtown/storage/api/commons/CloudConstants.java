package cloudtown.storage.api.commons;

import lombok.Data;

@Data
public final class CloudConstants {
  public static final String MESSAGE_ERROR_REQUEST = "%s can not be null or empty";
  
}
