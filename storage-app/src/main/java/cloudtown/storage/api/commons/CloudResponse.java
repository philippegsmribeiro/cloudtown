package cloudtown.storage.api.commons;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;
import org.springframework.http.HttpStatus;

/**
 * Define the template for the Response pattern.
 */
@Data
@Builder
@NoArgsConstructor
@AllArgsConstructor
public class CloudResponse {

  protected HttpStatus status;

  protected String message;

}
