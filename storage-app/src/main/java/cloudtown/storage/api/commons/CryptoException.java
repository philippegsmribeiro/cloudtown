package cloudtown.storage.api.commons;

/**
 * Exception when a action request on an instance fails.
 * 
 */
public class CryptoException extends Exception {

  private static final long serialVersionUID = 1L;

  public CryptoException(String message) {
    super(message);
  }
}
