package cloudtown.storage.api;

import cloudtown.storage.cloud.provider.ProviderIntegrationException;
import cloudtown.storage.model.bucket.BucketDetailRequest;
import cloudtown.storage.model.bucket.BucketException;
import cloudtown.storage.model.bucket.BucketListRequest;
import cloudtown.storage.model.bucket.BucketService;
import lombok.extern.log4j.Log4j2;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

@RestController
@Log4j2
@RequestMapping(value = BucketController.API)
public class BucketController {

  protected static final String ENDPOINT_DESCRIPTION = "Bucket endpoint";
  protected static final String API = "/buckets";
  protected static final String LIST = "/list";
  protected static final String DETAIL = "/detail";
  protected static final String REGION = "/region";
  protected static final String PERMISSION = "/permissions";

  private final BucketService bucketService;

  @Autowired
  public BucketController(BucketService bucketService) {
    this.bucketService = bucketService;
  }

  /**
   * Get the / endpoint.
   * 
   * @return description of the endpoint
   */
  @GetMapping
  public ResponseEntity<String> get() {
    return ResponseEntity.ok(ENDPOINT_DESCRIPTION);
  }

  /**
   * List buckets with pagination.
   * 
   * @param bucketListRequest request
   * @return list the Buckets
   */
  @PostMapping(LIST)
  public ResponseEntity<?> list(@RequestBody BucketListRequest bucketListRequest) {
    try {
      return ResponseEntity.ok(bucketService.listBuckets(bucketListRequest));
    } catch (ProviderIntegrationException e) {
      log.error("Error", e);
      return ResponseEntity.status(HttpStatus.INTERNAL_SERVER_ERROR).body(e.getMessage());
    } catch (BucketException e) {
      log.error("Error", e);
      return ResponseEntity.status(HttpStatus.PRECONDITION_FAILED).body(e.getMessage());
    } catch (Exception e) {
      log.error("Error", e);
      return ResponseEntity.status(HttpStatus.INTERNAL_SERVER_ERROR).build();
    }
  }

  /**
   * Get bucket detail.
   * 
   * @param bucketDetailRequest request
   * @return Bucket detail
   */
  @PostMapping(DETAIL)
  public ResponseEntity<?> getBucketDetail(@RequestBody BucketDetailRequest bucketDetailRequest) {
    try {
      return ResponseEntity.ok(bucketService.getBucketDetail(bucketDetailRequest));
    } catch (ProviderIntegrationException e) {
      log.error("Error", e);
      return ResponseEntity.status(HttpStatus.INTERNAL_SERVER_ERROR).body(e.getMessage());
    } catch (BucketException e) {
      log.error("Error", e);
      return ResponseEntity.status(HttpStatus.PRECONDITION_FAILED).body(e.getMessage());
    } catch (Exception e) {
      log.error("Error", e);
      return ResponseEntity.status(HttpStatus.INTERNAL_SERVER_ERROR).build();
    }
  }

  /**
   * Get bucket region.
   * 
   * @param bucketDetailRequest request
   * @return Region
   */
  @PostMapping(REGION)
  public ResponseEntity<?> getBucketRegion(@RequestBody BucketDetailRequest bucketDetailRequest) {
    try {
      return ResponseEntity.ok(bucketService.getRegionBucket(bucketDetailRequest));
    } catch (ProviderIntegrationException e) {
      log.error("Error", e);
      return ResponseEntity.status(HttpStatus.INTERNAL_SERVER_ERROR).body(e.getMessage());
    } catch (BucketException e) {
      log.error("Error", e);
      return ResponseEntity.status(HttpStatus.PRECONDITION_FAILED).body(e.getMessage());
    } catch (Exception e) {
      log.error("Error", e);
      return ResponseEntity.status(HttpStatus.INTERNAL_SERVER_ERROR).build();
    }
  }

  /**
   * Get bucket permission.
   * 
   * @param bucketDetailRequest request
   * @return Bucket permission
   */
  @PostMapping(PERMISSION)
  public ResponseEntity<?> getBucketPermissionDetail(
      @RequestBody BucketDetailRequest bucketDetailRequest) {
    try {
      return ResponseEntity.ok(bucketService.getAccessControlBucket(bucketDetailRequest));
    } catch (ProviderIntegrationException e) {
      log.error("Error", e);
      return ResponseEntity.status(HttpStatus.INTERNAL_SERVER_ERROR).body(e.getMessage());
    } catch (BucketException e) {
      log.error("Error", e);
      return ResponseEntity.status(HttpStatus.PRECONDITION_FAILED).body(e.getMessage());
    } catch (Exception e) {
      log.error("Error", e);
      return ResponseEntity.status(HttpStatus.INTERNAL_SERVER_ERROR).build();
    }
  }
}
