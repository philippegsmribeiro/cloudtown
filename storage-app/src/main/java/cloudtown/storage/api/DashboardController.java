package cloudtown.storage.api;

import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

@RestController
@RequestMapping(value = DashboardController.API)
public class DashboardController {

  protected static final String API = "/dashboard";
  
}
