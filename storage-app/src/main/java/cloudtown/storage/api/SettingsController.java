package cloudtown.storage.api;

import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

@RestController
@RequestMapping(value = SettingsController.API)
public class SettingsController {

  protected static final String API = "/settings";
  
}
