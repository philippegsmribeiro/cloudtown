package cloudtown.storage.api;

import cloudtown.storage.model.user.User;
import cloudtown.storage.model.user.UserChangePasswordRequest;
import cloudtown.storage.model.user.UserException;
import cloudtown.storage.model.user.UserProfile;
import cloudtown.storage.model.user.UserRequest;
import cloudtown.storage.model.user.UserService;
import java.util.List;
import lombok.extern.log4j.Log4j2;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.security.core.userdetails.UsernameNotFoundException;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

@RestController
@Log4j2
@RequestMapping(value = UserController.API)
public class UserController {

  protected static final String ENDPOINT_DESCRIPTION = "User endpoint";
  protected static final String API = "/users";
  protected static final String SAVE = "/";
  protected static final String DELETE = "/{id}";
  protected static final String DESCRIBE = "/{id}";
  protected static final String SEARCH = "/search";
  protected static final String DESCRIBE_LOGIN = "/username/{login}";
  protected static final String UPDATE_PASSWORD = "/update_password/{login}";
  protected static final String SAVE_PROFILE = "/profiles/{id}/";
  protected static final String DESCRIBE_PROFILE = "/profiles/{id}";

  private final UserService userService;

  @Autowired
  public UserController(UserService userService) {
    this.userService = userService;
  }

  /**
   * Get the / endpoint.
   * 
   * @return description of the endpoint
   */
  @GetMapping
  public ResponseEntity<String> get() {
    return ResponseEntity.ok(ENDPOINT_DESCRIPTION);
  }

  /**
   * Get user by Id.
   * 
   * @param id of user
   * @return User if found, null otherwise
   */
  @GetMapping(DESCRIBE)
  public ResponseEntity<?> getOne(@PathVariable String id) {
    try {
      User user = userService.findById(id);
      return ResponseEntity.ok(user);
    } catch (UsernameNotFoundException e) {
      log.error("Error", e);
      return ResponseEntity.status(HttpStatus.PRECONDITION_FAILED).body(e.getMessage());
    } catch (Exception e) {
      log.error("Error", e);
      return ResponseEntity.status(HttpStatus.INTERNAL_SERVER_ERROR).build();
    }
  }
  
  /**
   * Save a user.*
   * 
   * @param user to be saved
   * @return saved user
   */
  @PostMapping(SAVE)
  public ResponseEntity<?> save(@RequestBody User user) {
    try {
      User saved = userService.save(user);
      return ResponseEntity.ok(saved);
    } catch (UserException e) {
      log.error("Error", e);
      return ResponseEntity.status(HttpStatus.PRECONDITION_FAILED).body(e.getMessage());
    } catch (Exception e) {
      log.error("Error", e);
      return ResponseEntity.status(HttpStatus.INTERNAL_SERVER_ERROR).build();
    }
  }
  
  /**
   * Delete an user by id.
   * 
   * @param id of user
   * @return true if deleted, false otherwise
   */
  @DeleteMapping(DELETE)
  public ResponseEntity<?> delete(@PathVariable String id) {
    try {
      userService.delete(id);
      return ResponseEntity.ok().build();
    } catch (UserException e) {
      log.error("Error", e);
      return ResponseEntity.status(HttpStatus.PRECONDITION_FAILED).body(e.getMessage());
    } catch (Exception e) {
      log.error(e);
      return ResponseEntity.status(HttpStatus.INTERNAL_SERVER_ERROR).build();
    }
  }

  /**
   * Get user by login.
   * 
   * @param login of user
   * @return User if found, null otherwise
   */
  @GetMapping(DESCRIBE_LOGIN)
  public ResponseEntity<?> getUserByLogin(@PathVariable String login) {
    try {
      User user = userService.findByLoginName(login);
      return ResponseEntity.ok(user);
    } catch (UsernameNotFoundException e) {
      log.error("Error", e);
      return ResponseEntity.status(HttpStatus.PRECONDITION_FAILED).body(e.getMessage());
    } catch (Exception e) {
      log.error("Error", e);
      return ResponseEntity.status(HttpStatus.INTERNAL_SERVER_ERROR).build();
    }
  }

  /**
   * Get user profile by Id.
   * 
   * @param id of user
   * @return User if found, null otherwise
   */
  @GetMapping(DESCRIBE_PROFILE)
  public ResponseEntity<?> getOneProfile(@PathVariable String id) {
    try {
      UserProfile userProfile = userService.findProfileByUserId(id);
      return ResponseEntity.ok(userProfile);
    } catch (UsernameNotFoundException e) {
      log.error("Error", e);
      return ResponseEntity.status(HttpStatus.PRECONDITION_FAILED).body(e.getMessage());
    } catch (Exception e) {
      log.error("Error", e);
      return ResponseEntity.status(HttpStatus.INTERNAL_SERVER_ERROR).build();
    }
  }

 

  /**
   * Save a user profile.
   * 
   * @param id the User.
   * @param userProfile to be saved
   * @return saved user profile
   */
  @PostMapping(SAVE_PROFILE)
  public ResponseEntity<?> saveProfile(@PathVariable String id,
      @RequestBody UserProfile userProfile) {
    try {
      UserProfile saved = userService.updateProfile(id, userProfile);
      return ResponseEntity.ok(saved);
    } catch (UserException e) {
      log.error("Error", e);
      return ResponseEntity.status(HttpStatus.PRECONDITION_FAILED).body(e.getMessage());
    } catch (Exception e) {
      log.error("Error", e);
      return ResponseEntity.status(HttpStatus.INTERNAL_SERVER_ERROR).build();
    }
  }




  /**
   * Update password.
   * 
   * @param request password to be changed
   */
  @PutMapping(UPDATE_PASSWORD)
  public ResponseEntity<?> updatePassword(@RequestBody UserChangePasswordRequest request) {
    try {
      userService.updatePassword(request);
      return ResponseEntity.ok().build();
    } catch (UserException e) {
      log.error("Error", e);
      return ResponseEntity.status(HttpStatus.PRECONDITION_FAILED).body(e.getMessage());
    } catch (Exception e) {
      log.error("Error", e);
      return ResponseEntity.status(HttpStatus.INTERNAL_SERVER_ERROR).build();
    }
  }

  /**
   * Search users by filter request.
   * 
   * @param userRequest Information of the user to be searched.
   * @return List of Users
   */
  @PostMapping(SEARCH)
  public ResponseEntity<?> list(@RequestBody UserRequest userRequest) {
    try {
      List<User> users = userService.findByFilter(userRequest);
      return ResponseEntity.ok(users);
    } catch (UserException e) {
      log.error("Error", e);
      return ResponseEntity.status(HttpStatus.PRECONDITION_FAILED).body(e.getMessage());
    } catch (Exception e) {
      log.error("Error", e);
      return ResponseEntity.status(HttpStatus.INTERNAL_SERVER_ERROR).build();
    }
  }
}
