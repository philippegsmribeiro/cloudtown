package cloudtown.storage.api;

import cloudtown.storage.cloud.Provider;
import cloudtown.storage.model.region.Region;
import cloudtown.storage.model.region.RegionService;
import java.util.List;
import lombok.extern.log4j.Log4j2;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

@RestController
@Log4j2
@RequestMapping(value = ProviderController.API)
public class ProviderController {
  
  protected static final String ENDPOINT_DESCRIPTION = "Provider endpoint";

  protected static final String API = "/provider";
  protected static final String REGIONS = "/{provider}/{accountId}/regions/";

  private final RegionService regionService;

  @Autowired
  public ProviderController(RegionService bucketService) {
    this.regionService = bucketService;
  }
  
  /**
   * Get the / endpoint.
   * 
   * @return description of the endpoint
   */
  @GetMapping
  public ResponseEntity<String> get() {
    return ResponseEntity.ok(ENDPOINT_DESCRIPTION);
  }

  /**
   * List regions from a provider.
   * 
   */
  @GetMapping(REGIONS)
  public ResponseEntity<List<Region>> list(@PathVariable Provider provider,
      @PathVariable String accountId) {
    try {
      List<Region> result = regionService.list(accountId, provider);
      return ResponseEntity.ok(result);
    } catch (Exception e) {
      log.error(e);
      return ResponseEntity.status(HttpStatus.INTERNAL_SERVER_ERROR).build();
    }
  }
}
