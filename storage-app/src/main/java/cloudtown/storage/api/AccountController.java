package cloudtown.storage.api;

import cloudtown.storage.model.account.Account;
import cloudtown.storage.model.account.AccountException;
import cloudtown.storage.model.account.AccountRequest;
import cloudtown.storage.model.account.AccountService;
import cloudtown.storage.model.common.PageResponse;
import java.util.List;
import lombok.extern.log4j.Log4j2;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageRequest;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

/**
 * Account Controller.
 * 
 * @author flaviolcastro
 *
 */
@RestController
@Log4j2
@RequestMapping(value = AccountController.API)
public class AccountController {

  protected static final String ENDPOINT_DESCRIPTION = "Account endpoint";

  protected static final String API = "/accounts";
  protected static final String DESCRIBE = "/{id}";
  protected static final String LIST = "/list/{page}/{pageSize}";
  protected static final String SEARCH = "/list/search";
  protected static final String SAVE = "/";
  protected static final String DELETE = "/{id}";

  private final AccountService accountService;

  @Autowired
  public AccountController(AccountService accountService) {
    this.accountService = accountService;
  }

  /**
   * Get the / endpoint.
   * 
   * @return description of the endpoint
   */
  @GetMapping
  public ResponseEntity<String> get() {
    return ResponseEntity.ok(ENDPOINT_DESCRIPTION);
  }

  /**
   * Get account by Id.
   * 
   * @param id of account
   * @return Account if found, null otherwise
   */
  @GetMapping(DESCRIBE)
  public ResponseEntity<?> getOne(@PathVariable String id) {
    try {
      Account account = accountService.retrieveById(id, true);
      return ResponseEntity.ok(account);
    } catch (AccountException e) {
      log.error("Error", e);
      return ResponseEntity.status(HttpStatus.PRECONDITION_FAILED).body(e.getMessage());
    } catch (Exception e) {
      log.error("Error", e);
      return ResponseEntity.status(HttpStatus.INTERNAL_SERVER_ERROR).build();
    }
  }

  /**
   * List accounts with pagination.
   * 
   * @param page count
   * @param pageSize register per page
   * @return Page of Accounts
   */
  @GetMapping(LIST)
  public ResponseEntity<?> list(@PathVariable Integer page, @PathVariable Integer pageSize) {
    try {
      Page<Account> pageResult = accountService.retrieveAll(PageRequest.of(page, pageSize));
      return ResponseEntity.ok(PageResponse.fromPage(pageResult));
    } catch (AccountException e) {
      log.error("Error", e);
      return ResponseEntity.status(HttpStatus.PRECONDITION_FAILED).body(e.getMessage());
    } catch (Exception e) {
      log.error("Error", e);
      return ResponseEntity.status(HttpStatus.INTERNAL_SERVER_ERROR).build();
    }
  }

  /**
   * Search accounts by filter request.
   * 
   * @param accountRequest request
   * @return List of Accounts
   */
  @PostMapping(SEARCH)
  public ResponseEntity<?> list(@RequestBody AccountRequest accountRequest) {
    try {
      List<Account> accounts = accountService.findByFilter(accountRequest);
      return ResponseEntity.ok(accounts);
    } catch (AccountException e) {
      log.error("Error", e);
      return ResponseEntity.status(HttpStatus.PRECONDITION_FAILED).body(e.getMessage());
    } catch (Exception e) {
      log.error("Error", e);
      return ResponseEntity.status(HttpStatus.INTERNAL_SERVER_ERROR).build();
    }
  }

  /**
   * Save a account.
   * 
   * @param account to be saved
   * @return saved account
   */
  @PostMapping(SAVE)
  public ResponseEntity<?> save(@RequestBody Account account) {
    try {
      Account saved = accountService.save(account);
      return ResponseEntity.ok(saved);
    } catch (AccountException e) {
      log.error("Error", e);
      return ResponseEntity.status(HttpStatus.PRECONDITION_FAILED).body(e.getMessage());
    } catch (Exception e) {
      log.error("Error", e);
      return ResponseEntity.status(HttpStatus.INTERNAL_SERVER_ERROR).build();
    }
  }

  /**
   * Delete an account by id.
   * 
   * @param id of account
   * @return true if deleted, false otherwise
   */
  @DeleteMapping(DELETE)
  public ResponseEntity<?> delete(@PathVariable String id) {
    try {
      Boolean deleted = accountService.deleteById(id);
      if (deleted) {
        return ResponseEntity.ok().build();
      }
      return ResponseEntity.status(HttpStatus.INTERNAL_SERVER_ERROR).build();
    } catch (AccountException e) {
      log.error("Error", e);
      return ResponseEntity.status(HttpStatus.PRECONDITION_FAILED).body(e.getMessage());
    } catch (Exception e) {
      log.error(e);
      return ResponseEntity.status(HttpStatus.INTERNAL_SERVER_ERROR).build();
    }
  }
}
