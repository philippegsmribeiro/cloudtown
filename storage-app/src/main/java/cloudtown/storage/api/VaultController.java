package cloudtown.storage.api;

import cloudtown.storage.cloud.provider.ProviderIntegrationException;
import cloudtown.storage.model.vault.VaultException;
import cloudtown.storage.model.vault.VaultListRequest;
import cloudtown.storage.model.vault.VaultService;
import lombok.extern.log4j.Log4j2;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

@RestController
@Log4j2
@RequestMapping(value = VaultController.API)
public class VaultController {

  protected static final String ENDPOINT_DESCRIPTION = "Vault endpoint";
  protected static final String API = "/vaults";
  protected static final String LIST = "/list";

  private final VaultService vaultService;

  @Autowired
  public VaultController(VaultService vaultService) {
    this.vaultService = vaultService;
  }

  /**
   * Get the / endpoint.
   * 
   * @return description of the endpoint
   */
  @GetMapping
  public ResponseEntity<String> get() {
    return ResponseEntity.ok(ENDPOINT_DESCRIPTION);
  }

  /**
   * List vaults with pagination.
   * 
   * @param vaultListRequest request
   * @return list the Vaults
   */
  @PostMapping(LIST)
  public ResponseEntity<?> list(@RequestBody VaultListRequest vaultListRequest) {
    try {
      return ResponseEntity.ok(vaultService.listVaults(vaultListRequest));
    } catch (ProviderIntegrationException e) {
      log.error("Error", e);
      return ResponseEntity.status(HttpStatus.INTERNAL_SERVER_ERROR).body(e.getMessage());
    } catch (VaultException e) {
      log.error("Error", e);
      return ResponseEntity.status(HttpStatus.PRECONDITION_FAILED).body(e.getMessage());
    } catch (Exception e) {
      log.error("Error", e);
      return ResponseEntity.status(HttpStatus.INTERNAL_SERVER_ERROR).build();
    }
  }
}
