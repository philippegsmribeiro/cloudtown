package cloudtown.storage;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

/**
 * Spring boot application.
 * 
 * @author flaviolcastro
 *
 */
@SpringBootApplication
public class StorageApplication {

  public static void main(String[] args) {
    SpringApplication.run(StorageApplication.class, args);
  }
}
