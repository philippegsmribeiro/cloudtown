#!/bin/bash
set -e

# run tests
gradle clean build --stacktrace --info --scan -g /cache/

# upload the codecov
echo "Running code coverage"
curl -s https://codecov.io/bash >> codecov
bash codecov

# copy jar to volume
cp storage-app/build/libs/*.jar /data
ls /data